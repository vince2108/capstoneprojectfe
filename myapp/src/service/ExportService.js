import axios from "axios";

const EXPORT_CREATE_API="http://localhost:8081/api/v1/create-export"
const EXPORT_LIST_API="http://localhost:8081/api/v1/get-export-by-branch"
const EXPORT_LIST_SEARCH_API="http://localhost:8081/api/v1/get-export-by-status"
const EXPORT_DETAIL_API="http://localhost:8081/api/v1/short-export-detail"
const EXPORT_DETAIL_FULL_API="http://localhost:8081/api/v1/full-export-detail"

const ExportService = {
    create(body) {
      return axios.post(EXPORT_CREATE_API, body);
    },
    getListExport(params) {
      return axios.get(EXPORT_LIST_API, { params })
    },
    searchListExport(params) {
      return axios.get(EXPORT_LIST_SEARCH_API, { params })
    },
    getDetail(idExport) {
      return axios.get(EXPORT_DETAIL_API, { params: { idExport } })
    },
    getDetailFull(idExport) {
      return axios.get(EXPORT_DETAIL_FULL_API, { params: { idExport } })
    },
    exportProduct(body){
      return axios.post("http://localhost:8081/api/v1/export-products", body);
    }
  }

export default ExportService;