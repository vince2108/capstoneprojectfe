import axios from "axios";

const DOCTOR_API_GETMEDICALRECORDBYID_URL = 'http://localhost:8081/api/v1/find-patient?patientId='
const DOCTOR_API_GETPATIENTBYCONSULTINGROOM_URL = 'http://localhost:8081/api/v1/patient-consulting-room?consultingId='
const DOCTOR_API_STARTEXAMINATIONCREATE_URL = 'http://localhost:8081/api/v1/start-examination-create?'
const DOCTOR_API_STARTEXAMINATIONUPDATE_ROOM = 'http://localhost:8081/api/v1/start-examination-update?'
const DOCTOR_API_GETALLPATIENTINFORBYID_URL = 'http://localhost:8081/api/v1/find-patient'
const DOCTOR_API_ADDSERVICE_URL = 'http://localhost:8081/api/v1/add-service'
const DOCTOR_API_FINISHSERVICE_URL = 'http://localhost:8081/api/v1/finish-examination'
const DOCTOR_API_UPDATESTATUSEXAMINATION_URL = 'http://localhost:8081/api/v1/update-status-examination'
const DOCTOR_API_UPDATEINDEXPATIENT_URL = 'http://localhost:8081/api/v1/update-index-patient'
const DOCTOR_API_FINISHSERVICEOFSERVICEROOM_URL = 'http://localhost:8081/api/v1/finish-examination-service'
class DoctorService {
    getPatientMedicalRecordById(patientId) {
        try {
            return axios.get(DOCTOR_API_GETMEDICALRECORDBYID_URL + patientId)
        } catch (error) {
            throw (console.log(error))
        }
    }

    getPatientByConsultingRoom(consultingRoomId,idBranch) {
        try {
            return axios.get(DOCTOR_API_GETPATIENTBYCONSULTINGROOM_URL + consultingRoomId +'&idBranch='+idBranch)
        } catch (error) {
            throw (console.log(error))
        }
    }

    startExaminationCreate(allergies, pastMedicalHistory, idService, idConsultingRoom, idStaff, idBranch,
        fullName, dob, address, village, district, province, sex, identityCard, phoneNumber, ethnicity, job, email) {
        try {
            console.log(DOCTOR_API_STARTEXAMINATIONCREATE_URL
                + 'allergies=' + allergies + '&pastMedicalHistory=' + pastMedicalHistory + '&idService=' + idService
                + '&idConsultingRoom=' + idConsultingRoom + '&idStaff=' + idStaff + '&idBranch=' + idBranch)
            console.log(fullName, dob, address, village, district, province, sex, identityCard, phoneNumber, ethnicity, job, email)
            return axios.post(DOCTOR_API_STARTEXAMINATIONCREATE_URL
                + 'allergies=' + allergies + '&pastMedicalHistory=' + pastMedicalHistory + '&idService=' + idService
                + '&idConsultingRoom=' + idConsultingRoom + '&idStaff=' + idStaff + '&idBranch=' + idBranch,
                { fullName, dob, address, village, district, province, sex, identityCard, phoneNumber, ethnicity, job, email })
        } catch (error) {
            throw (console.log(error))
        }
    }

    updateExaminationUpdate(accountId, allergies, pastMedicalHistory, idService, idConsultingRoom, idStaff, idBranch,
        fullName, dob, address, village, district, province, sex, identityCard, phoneNumber, ethnicity, job, email) {
             console.log(DOCTOR_API_STARTEXAMINATIONUPDATE_ROOM +
                'idPatient='+ accountId + '&allergies=' + allergies + '&pastMedicalHistory=' + pastMedicalHistory + '&idService=' + idService
                + '&idConsultingRoom=' + idConsultingRoom + '&idStaff=' + idStaff + '&idBranch=' + idBranch)
            console.log(fullName, dob, address, village, district, province, sex, identityCard, phoneNumber, ethnicity, job,email+"1")
        try {
            return axios.post(DOCTOR_API_STARTEXAMINATIONUPDATE_ROOM +
                'idPatient='+ accountId + '&allergies=' + allergies + '&pastMedicalHistory=' + pastMedicalHistory + '&idService=' + idService
                + '&idConsultingRoom=' + idConsultingRoom + '&idStaff=' + idStaff + '&idBranch=' + idBranch,
                { fullName, dob, address, village, district, province, sex, identityCard, phoneNumber, ethnicity, job, email })
        } catch (error) {
            throw (console.log(error))
        }
    }

    getAllPatientInforByPatientId(patientId){
        try {
            console.log(DOCTOR_API_GETALLPATIENTINFORBYID_URL+'?patientId='+patientId)
            return axios.get(DOCTOR_API_GETALLPATIENTINFORBYID_URL+'?patientId='+patientId)
        } catch (error) {
            throw (console.log(error))
        }
    }

    addService(idPatient,idThisConsulting, note, sttThis,idThatConsulting , idThatService){
        try {
            
            return axios.put(DOCTOR_API_ADDSERVICE_URL+'?idPatient='+idPatient+'&idThisConsulting='+idThisConsulting
            +'&note='+note+'&sttThis='+sttThis+'&idThatConsulting='+idThatConsulting+'&idThatService='+idThatService)
        } catch (error) {
            throw (console.log(error))
        }
    }

    finishService(idPatient,idConsulting, note){
        try {
            return axios.put(DOCTOR_API_FINISHSERVICE_URL+'?idPatient='+idPatient+'&idConsulting='+idConsulting+'&note='+note)
        } catch (error) {
            throw (console.log(error))
        }
    }

    finishServiceOfServiceRoom(idPatient,idConsulting ){
        try {
            return axios.put(DOCTOR_API_FINISHSERVICEOFSERVICEROOM_URL+"?idPatient="+idPatient+"&idConsulting="+idConsulting)
        } catch (error) {
            throw (console.log(error))
        }
    }


    updateStatusExamnination(idPatient, idConsulting ) {
        try {
            return axios.put(DOCTOR_API_UPDATESTATUSEXAMINATION_URL+"?idPatient="+idPatient+"&idConsulting="+idConsulting)
        } catch (error) {
            throw (console.log(error))
        }
    }

    updateIndexPatient(idPatient ,idConsulting ,stt) {
        try {
            return axios.put(DOCTOR_API_UPDATEINDEXPATIENT_URL+"?idPatient="+idPatient+"&idConsulting="+idConsulting+"&stt="+stt)
        } catch (error) {
            throw (console.log(error))
        }
    }

    getPatientWithStatus(consultingId,idBranch){
        try {
            return axios.get("http://localhost:8081/api/v1/patient-consulting-room-status"+"?consultingId="+consultingId+"&idBranch="+idBranch)
        } catch (error) {
            throw (console.log(error))
        }
    }

    changeRoomService(abc){
        try {
            return axios.put("http://localhost:8081/api/v1/change-room-service",abc);
        } catch (error) {
            throw (console.log(error))
        }
    }

}
export default new DoctorService();