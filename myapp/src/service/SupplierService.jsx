import axios from "axios";
const SUPPLIER_API_GETALL_URL="http://localhost:8081/api/v1/get-all-supplier"
class SupplierService {
    getAllSupplier(){
        try {
            console.log(SUPPLIER_API_GETALL_URL)
            return axios.get(SUPPLIER_API_GETALL_URL)
        } catch (error) {
            throw console.log(error);
        }
    }

}

export default new SupplierService();