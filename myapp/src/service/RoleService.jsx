import axios from "axios";
const ROLE_API_GETALLROLE = "http://localhost:8081/api/v1/get-all-role";
class RoleService {
  getAllRole() {
    return axios.get(ROLE_API_GETALLROLE);
  }
}
export default new RoleService();
