import axios from "axios";
const EQUITMENT_API_GETCREATEQUITMENT_URL = "http://localhost:8081/api/v1/create-equipment";
const EQUIPMENT_API_GETALLEQUIPMENT_URL = "http://localhost:8081/api/v1/get-all-equipment";
class EquitmentService {
  createEquitment(name, note, unitId, equipmentTypeIds) {
    try {
        return axios.post(EQUITMENT_API_GETCREATEQUITMENT_URL,{name,note,unitId,equipmentTypeIds});
    } catch (error) {
        throw (console.log(error))
    }
  }

  getAllEquipment(){
    try {
      return axios.get(EQUIPMENT_API_GETALLEQUIPMENT_URL)
    } catch (error) {
      throw (console.log(error))
    }
  }
}
export default new EquitmentService();
