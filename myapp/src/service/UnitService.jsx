import axios from "axios";
const UNIT_API_GETALLUNITGROUP_URL = 'http://localhost:8081/api/v1/get-unit-group'
const UNIT_API_CREATEUNIGROUP_URL = 'http://localhost:8081/api/v1/create-unit-group'
const UNIT_API_DELETEUNITGROUP_URL = 'http://localhost:8081/api/v1/delete-unit-group'
const UNIT_API_GETALLUNIT_URL = 'http://localhost:8081/api/v1/get-unit'
const UNIT_API_DELETEUNIT_URL = 'http://localhost:8081/api/v1/delete-unit'
const UNIT_API_CREATEUNIT_URL = 'http://localhost:8081/api/v1/create-unit'
const UNIT_API_GETUNITBYUNITGROUPID = 'http://localhost:8081/api/v1/get-unit-by-id-group'
class UnitService {
    getAllUniGroup(){
        try {
            console.log(UNIT_API_GETALLUNITGROUP_URL)
            return axios.get(UNIT_API_GETALLUNITGROUP_URL)
        } catch (error) {
            throw console.log(error);
        }
    }

    createUnitGroup(name){
        try {
            console.log(UNIT_API_CREATEUNIGROUP_URL, {name})
            return axios.post(UNIT_API_CREATEUNIGROUP_URL, {name})
        } catch (error) {
            throw console.log(error);
        }
    }

    deleteUnitGroup(unitGroupsId){
        try {
            console.log(UNIT_API_DELETEUNITGROUP_URL,{unitGroupsId})
            return axios.delete(UNIT_API_DELETEUNITGROUP_URL, {data : {unitGroupsId:unitGroupsId}} )
        } catch (error) {
            throw console.log(error);
        }
    }

    getAllUnit(){
        try {
            return axios.get('http://localhost:8081/api/v1/get-unit')
        } catch (error) {
            throw console.log(error);
        }
    }

    deleteUnit(idUnits){
        try {
            return axios.delete(UNIT_API_DELETEUNIT_URL, {data : {idUnits:idUnits}})
        } catch (error) {
            throw console.log(error);
        }
    }

    createUnit(unitName, unitGroupId){
        try {
            return axios.post(UNIT_API_CREATEUNIT_URL,{unitName,unitGroupId})
        } catch (error) {
            throw console.log(error);
        }
    }

    getAllUnitByUnitGroupId(idUnitGroup){
        try {
            console.log(UNIT_API_GETUNITBYUNITGROUPID+'?idUnitGroup='+idUnitGroup)
            return axios.get(UNIT_API_GETUNITBYUNITGROUPID+'?idUnitGroup='+idUnitGroup)
        } catch (error) {
            throw console.log(error);
        }
    }
}

export default new UnitService();