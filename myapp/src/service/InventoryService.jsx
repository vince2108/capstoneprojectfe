import axios from "axios";
const INVENTORY_API_CREATEINVENTORY_URL="http://localhost:8081/api/v1/create-inventory"
const FINISH_INVENTORY ="http://localhost:8081/api/v1/finish-inventory"
const INVENTORY_BY_ID_URL="http://localhost:8081/api/v1/inventory-by-id"
const INVENTORY_PRODUCT_BY_ID_URL="http://localhost:8081/api/v1/inventory-product-by-id"
class InventoryService {
    createInventory(storeHouseId,petitionerId,date){
        try {
            return axios.post(INVENTORY_API_CREATEINVENTORY_URL,{storeHouseId,petitionerId,date})
        } catch (error) {
            throw console.log(error);
        }
    }

    getAllInventoryByBranch(idBranch ){
        try {
            return axios.get("http://localhost:8081/api/v1/get-inventory-by-branch"+"?idBranch="+idBranch)
        } catch (error) {
            throw console.log(error);
        }
    }

    searchInventoryByBranchAndStatus(idBranch ,idStatus ){
        try {
            return axios.get("http://localhost:8081/api/v1/inventory-by-branch-status"+"?idBranch="+idBranch+"&idStatus="+idStatus)
        } catch (error) {
            throw console.log(error);
        }
    }

    finishInventory(body) {
        return axios.post(FINISH_INVENTORY, body)
    }

    getDetail(idInventory) {
        return axios.get(INVENTORY_BY_ID_URL, { params: { idInventory }})
    }

    getDetailDone(idInventory) {
        return axios.get(INVENTORY_PRODUCT_BY_ID_URL, { params: { idInventory }})
    }

    getExportByBranchAndStatus(idBranch ,idStatus ){
        try {
            return axios.get("http://localhost:8081/api/v1/get-export-by-status"+"?idBranch="+idBranch+"&idStatus="+idStatus);
        } catch (error) {
            throw console.log(error);
        }
    }
}

export default new InventoryService();