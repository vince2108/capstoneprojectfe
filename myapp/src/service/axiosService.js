import axios from "axios";

// Set up default config for http requests here
const axiosService = axios.create({
  baseURL: 'http://localhost:8081/api/v1',
  headers: {
    "content-type": "application/raw",
  },
});

axiosService.interceptors.response.use(
  (response) => {
    if (response && response.data) {
      return response.data;
    }
    return response;
  },
  (error) => {
    switch (error.response.status) {
      default:
        return Promise.reject(error);
    }
  }
);

export { axiosService };
