import axios from "axios";

const PAYMENT_API_PATIENTPAID_URL = "http://localhost:8081/api/v1/patient-paid";
const PAYMENT_API_PATIENTSERVICEPAID_URL =
  "http://localhost:8081/api/v1/patient-used-service";
const PAYMENT_API_PATIENTPAIEDBYNAME_URL =
  "http://localhost:8081/api/v1/patient-paid-by-name";
const PAYMENT_API_PATIENTPAYINGSERVICE_URL =
  "http://localhost:8081/api/v1/patient-paying-service";
const PAYMENT_API_PAYTIENTFINISHSERVICE_URL =
  "http://localhost:8081/api/v1/patient-finish-pay"
const PAYMENT_API_REPORTSERVICEBYID_URL = "http://localhost:8081/api/v1/report-service-by-id"
const PAYMENT_API_PATIENTUSESERVICECONSULTINGROOM = "http://localhost:8081/api/v1/patient-used-service-consulting"

class PaymentService {
  getAllPatientPaid(idBranch) {
    try {
      console.log(PAYMENT_API_PATIENTPAID_URL);
      return axios.get(PAYMENT_API_PATIENTPAID_URL+'?idBranch='+idBranch);
    } catch (error) {
      throw console.log(error);
    }
  }

  getPatientUseService(idPatient) {
    try {
      return axios.get(
        PAYMENT_API_PATIENTSERVICEPAID_URL +
          "?idPatient=" +
          idPatient 
      );
    } catch (error) {
      throw console.log(error);
    }
  }

  getPatientPaidByNameAndCurrentDate(name,idBranch ) {
    try {
      return axios.get(
        PAYMENT_API_PATIENTPAIEDBYNAME_URL + "?name=" + name +"&idBranch="+idBranch
      );
    } catch (error) {
      throw console.log(error);
    }
  }

  patientPayingService(idServices) {
    try {
      return axios.put(PAYMENT_API_PATIENTPAYINGSERVICE_URL, { idServices });
    } catch (error) {
      throw console.log(error);
    }
  }

  getReportServiceById(reportServices){
    try {
      return axios.post(PAYMENT_API_REPORTSERVICEBYID_URL,{reportServices})
    } catch (error) {
      throw console.log(error);
    }
  }

  getPatientUseServiceConsultingRoom(idPatient ,idConsulting ){
    try {
      return axios.get(PAYMENT_API_PATIENTUSESERVICECONSULTINGROOM+"?idPatient="+idPatient+"&idConsulting="+idConsulting)
    } catch (error) {
      throw console.log(error);
    }
  }

  patientFinishService(idPatient) {
    try {
      return axios.put(PAYMENT_API_PAYTIENTFINISHSERVICE_URL+"?idPatient="+idPatient);
    } catch (error) {
      throw console.log(error);
    }
  }
}
export default new PaymentService();
