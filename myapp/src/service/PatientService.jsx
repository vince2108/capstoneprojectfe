import axios from "axios";

const PATIENT_API_BASE_URL = "http://localhost:8081/api/v1/patient";
const PATIENT_API_GETPATIENTBYNAMEANDSTATUS =
  "http://localhost:8081/api/v1/patient-by-name-and-status";
// const PATIENT_API_UPDATEPATIENTINFORANDMEDICALRECORD =
//   "http://localhost:8081/api/v1/patient-examination-info";
class PatientService {
  getAllPatientStatusTrue(idBranch) {
    try {
      return axios.get(PATIENT_API_BASE_URL + "T?idBranch=" + idBranch);
    } catch (error) {
      throw console.log(error);
    }
  }

  getAllPatientStatusFalse() {
    try {
      return axios.get(PATIENT_API_BASE_URL + "F");
    } catch (error) {
      throw console.log(error);
    }
  }

  getPatientById(patientId) {
    try {
      return axios.get(PATIENT_API_BASE_URL + "/" + patientId);
    } catch (error) {
      throw console.log(error);
    }
  }

  getPatientByName(name){
    try {
      return axios.get("http://localhost:8081/api/v1/patient-by-name"+"?name=" +name)
    } catch (error) {
      throw console.log(error);
    }
  }

  getPatientByNameAndStatus(patientName, status) {
    console.log(
      PATIENT_API_GETPATIENTBYNAMEANDSTATUS +
        "?name=" +
        patientName +
        "&status=" +
        status
    );
    try {
      return axios.get(
        PATIENT_API_GETPATIENTBYNAMEANDSTATUS +
          "?name=" +
          patientName +
          "&status=" +
          status
      );
    } catch (error) {
      throw console.log(error);
    }
  }
  updatePatient(patient, patientId) {
    return axios.put(PATIENT_API_BASE_URL + "/" + patientId, patient);
  }

  inserPatient(
    accountId,
    fullName,
    dob,
    address,
    village,
    district,
    province,
    sex,
    identityCard,
    phoneNumber,
    ethnicity,
    job,
    status,
    roleId
  ) {
    try {
      return axios.post(PATIENT_API_BASE_URL, {
        accountId,
        fullName,
        dob,
        address,
        village,
        district,
        province,
        sex,
        identityCard,
        phoneNumber,
        ethnicity,
        job,
        status,
        roleId,
      });
    } catch (error) {
      throw console.log(error);
    }
  }

  updatePatientInforAndMedicalRecord(
    accountId,
    fullName,
    dob,
    address,
    village,
    district,
    province,
    sex,
    identityCard,
    phoneNumber,
    ethnicity,
    job,
    email,
    medicalRecordId,
    circuit,
    bloodPressure,
    pastMedicalHistory,
    allergies,
    note,
    icd10,
    icd,
    advice,
    date,
    weight,
    bmi,
    height,
    temperature
  ) {
    try {
      return axios.put('http://localhost:8081/api/v1/patient-examination-info', {
        accountId,
        fullName,
        dob,
        address,
        village,
        district,
        province,
        sex,
        identityCard,
        phoneNumber,
        ethnicity,
        job,
        email,
        medicalRecordId,
        circuit,
        bloodPressure,
        pastMedicalHistory,
        allergies,
        note,
        icd10,
        icd,
        advice,
        date,
        weight,
        bmi,
        height,
        temperature,
      });
    } catch (error) {
      throw console.log(error);
    }
  }
}

export default new PatientService();
