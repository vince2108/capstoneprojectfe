import axios from "axios";
const CONSULTINGROOM_API_GETALL_URL =
  "http://localhost:8081/api/v1/get-all-consulting";
const CONSULTINGROOM_API_GETBYSERVICE_URL =
  "http://localhost:8081/api/v1/get-consulting-by-service?idService=";
const CONSULTINGROOM_API_GETBYID_URL =
  "http://localhost:8081/api/v1/get-consulting-by-id";
const CONSULTINGROOM_API_GETALLANDSERVICE_URL =
  "http://localhost:8081/api/v1/get-all-consulting-service";
const CONSULTINGROOM_API_CREATENEW_URL =
  "http://localhost:8081/api/v1/create-consulting";
const CONSULTINGROOM_API_UPDATE_URL =
  "http://localhost:8081/api/v1/update-consulting";
class ConsultingRoomService {
  getAllConsultingRoom(idBranch) {
    try {
      return axios.get(CONSULTINGROOM_API_GETALL_URL+'?idBranch='+idBranch);
    } catch (error) {
      throw console.log(error);
    }
  }

  getConsultingRoomByService(medicalServiceId) {
    try {
      return axios.get(CONSULTINGROOM_API_GETBYSERVICE_URL + medicalServiceId);
    } catch (error) {
      throw console.log(error);
    }
  }

  getConsultingRoomById(consultingId) {
    try {
      return axios.get(
        CONSULTINGROOM_API_GETBYID_URL + "?roomId=" + consultingId
      );
    } catch (error) {
      throw console.log(error);
    }
  }

  getAllConsultingRoomAndService(idPatient, idBranch) {
    try {
      console.log(
        CONSULTINGROOM_API_GETALLANDSERVICE_URL + "?idPatient=" + idPatient+ "&idBranch=" + idBranch
      );
      return axios.get(
        CONSULTINGROOM_API_GETALLANDSERVICE_URL + "?idPatient=" + idPatient + "&idBranch=" + idBranch
      );
    } catch (error) {
      throw console.log(error);
    }
  }

  createConsultingRoom(roomName, abbreviationName, branchId, action = true) {
    console.log(
      CONSULTINGROOM_API_CREATENEW_URL + ": " + roomName,
      "brief: " + abbreviationName,
      "id: " + branchId
    );
    try {
      return axios.post(CONSULTINGROOM_API_CREATENEW_URL, {
        roomName,
        abbreviationName,
        branchId,
        action,
      });
    } catch (error) {
      throw console.log(error);
    }
  }
  updateConsultingRoom(consultingRoomId, roomName, abbreviationName, branchId) {
    console.log(
      CONSULTINGROOM_API_UPDATE_URL + ": " + consultingRoomId,
      "RoomName: " + roomName,
      "brief: " + abbreviationName,
      "id: " + branchId
    );
    try {
      return axios.put(CONSULTINGROOM_API_UPDATE_URL, {
        consultingRoomId,
        roomName,
        abbreviationName,
        branchId,
      });
    } catch (error) {}
  }
}
export default new ConsultingRoomService();
