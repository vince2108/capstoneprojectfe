import axios from "axios";
const STAFF_API_BASE_URL = "http://localhost:8081/api/v1/staff";
// const STAFF_API_PUT_URL = "http://localhost:8081/api/v1/staff/id?";
const STAFF_API_GETSTAFFBYNAME = "http://localhost:8081/api/v1/staffByName";
const STAFF_API_UPDATEPASSWORD = "http://localhost:8081/api/v1/changePassword"

class StaffService {
  getAllStaff() {
    return axios.get(STAFF_API_BASE_URL);
  }

  getAllStaffStatusFalse() {
    try {
      return axios.get(STAFF_API_GETSTAFFBYNAME + "F");
    } catch (error) {
      throw console.log(error);
    }
  }

  getStaffById(staffId) {
    try {
      return axios.get(STAFF_API_BASE_URL + "/" + staffId);
    } catch (error) {
      throw console.log(error);
      //console;
    }
  }

  getStaffByName(staffName) {
    try {
      console.log(STAFF_API_GETSTAFFBYNAME + "?name=" + staffName);
      return axios.get(STAFF_API_GETSTAFFBYNAME + "?name=" + staffName);
    } catch (error) {
      throw console.log(error);
    }
  }
  updateStaff(
    accountId,
    fullName,
    dob,
    address,
    village,
    district,
    province,
    sex,
    identityCard,
    phoneNumber,
    ethnicity,
    job,
    email,
    idBranch,
    idRole,
    idConsultingRoom,
    position
  ) {
    console.log(
     "api: " + STAFF_API_BASE_URL ,
    "fn: "+ fullName,
    "dob: "+ dob,
    "add: "+ address,
    "vi: "+ village,
    "dis: "+ district,
    "pro: "+ province,
    "sẽ: "+ sex,
    "idC: "+ identityCard,
    "pn: "+ phoneNumber,
    "eth: "+ ethnicity,
    "job: "+ job,
    "email: "+ email,


    );
    try {
      return axios.put(STAFF_API_BASE_URL + "/" + accountId, {
        accountId,
        fullName,
        dob,
        address,
        village,
        district,
        province,
        sex,
        identityCard,
        phoneNumber,
        ethnicity,
        job,
        email,
        idBranch,
    idRole,
    idConsultingRoom,
    position
      });
    } catch (error) {
      throw console.log(error);
    }

  }

  createStaff(
    fullName,
    dob,
    address,
    village,
    district,
    province,
    sex,
    identityCard,
    phoneNumber,
    ethnicity,
    job,
    email,
    userName,
    passWord,
    confirmPassWord,
    branchId,
    roleId,
    idConsultingRoom,
    position
  ) {
    console.log(
      STAFF_API_BASE_URL + ": " + fullName,
      "dob: " + dob,
      "address: " + address,
      "village: " + village,
      "dis: " + district,
      "pro: " + province,
      "sex: " + sex,
      "identityCard: " + identityCard,
      "phoneNumber: " + phoneNumber,
      "ethnicity: " + ethnicity,
      "job: " + job,
      "email: " + email,
      "userName: " + userName,
      "passWord: " + passWord,
      "confirmPassWord: " + confirmPassWord,
      "branchSId: " + branchId,
      "roleSId: " + roleId,
      "consulId: " + idConsultingRoom,
      "posi: " + position
    );
    try {
      console.log("1");
      return axios.post(STAFF_API_BASE_URL, {
        fullName,
        dob,
        address,
        village,
        district,
        province,
        sex,
        identityCard,
        phoneNumber,
        ethnicity,
        job,
        email,
        userName,
        passWord,
        confirmPassWord,
        branchId,
        roleId,
        idConsultingRoom,
        position,
      });
    } catch (error) {
      throw console.log(error);
    }
  }
  changePassword( username,
    password,
    newPassword,
    renewPassword) {
      console.log(STAFF_API_UPDATEPASSWORD + ": " +
      "userN: " +username,
      "p: "+ password,
      "nP:  "+ newPassword,
      "rnP:  "+ renewPassword);
      try {
       
        return axios.post(STAFF_API_UPDATEPASSWORD,{
          username,
    password,
    newPassword,
    renewPassword
        } );
      } catch (error) {
        throw console.log(error);
      }
    }

    updateStaffImage(staffId ,avatar){
      try {
        return axios.post("http://localhost:8081/api/v1/insert-image/"+staffId ,{avatar})
      } catch (error) {
        throw console.log(error);
      }
    }

    getStaffFollowRole(roleId){
      try {
        return axios.get("http://localhost:8081/api/v1/staffs-info"+"?roleId="+roleId)
      } catch (error) {
        throw console.log(error);
      }
    }
  
    createStaffManager(fullName,dob,address,village,district,province,sex,identityCard,phoneNumber,ethnicity,job,email,userName,passWord,confirmPassWord,branchId,roleId){
      try {
        return axios.post("http://localhost:8081/api/v1/create-staff" , {
          fullName,
          dob,
          address,
          village,
          district,
          province,
          sex,
          identityCard,
          phoneNumber,
          ethnicity,
          job,
          email,
          userName,
          passWord,
          confirmPassWord,
          branchId,
          roleId})
      } catch (error) {
        throw console.log(error);
      }
    }

    deleteStaff(id){
      try {
        return axios.put("http://localhost:8081/api/v1/delete-staff/"+id)
      } catch (error) {
        throw console.log(error);
      }
    }
}

export default new StaffService();
