import axios from "axios";
const MEDICINE_API_CREATEMEDICINE_URL= 'http://localhost:8081/api/v1/create-medicine'
const MEDICONE_API_GETALLMEDICINE_URL = 'http://localhost:8081/api/v1/get-all-medicine'
class MedicineService {
  createMedicine(medicineName,unitId,note,medicineTypeIds) {
    try {
        return axios.post(MEDICINE_API_CREATEMEDICINE_URL,{medicineName,
            unitId,note,medicineTypeIds})
    } catch (error) {
        throw (console.log(error))
    }   
  }

  getAllMedicine() {
    try {
      return axios.get(MEDICONE_API_GETALLMEDICINE_URL)
    } catch (error) {
      throw (console.log(error))
    }
  }
}
export default new MedicineService();