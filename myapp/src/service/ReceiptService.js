import axios from "axios";
const ROLE_API_CREATERECEIPT_URL = "http://localhost:8081/api/v1/create-receipt";
const ROLE_API_GETALLRECEIPTBYBRANCH = "http://localhost:8081/api/v1/receipt-by-branch"
const ROLE_API_GETINFORRECEIPT_URL ="http://localhost:8081/api/v1/receipt-short-by-id"
const ROLE_API_RECEIPTSEARCHBYSTATUSANDBRANCH_URL = "http://localhost:8081/api/v1/receipt-short-by-status"
const ROLE_API_ACCEPTRECEIPT_URL = "http://localhost:8081/api/v1/receipt-by-id"
const ROLE_API_CANCELRECEIPT_URL = "http://localhost:8081/api/v1/cancel-receipt-by-id"
const RECEIPT_CREATE_API="http://localhost:8081/api/v1/receipt-products"
const RECEIPT_DETAIL_API="http://localhost:8081/api/v1/transfer-short-receipt"
const RECEIPT_DETAIL_FULL_API="http://localhost:8081/api/v1/transfer-full-receipt"
const RECEIPT_COMPLETE_API="http://localhost:8081/api/v1/complete-receipt-by-id"

const ReceiptService = {
  // createReceipt(storeHouseId,date,medicineRequests,equipmentRequests) {
  //   try {
  //       // const data = {
  //       //   storeHouseId:storeHouseId,
  //       //   date:date}
  //       console.log(ROLE_API_CREATERECEIPT_URL,
  //         storeHouseId,date,medicineRequests,equipmentRequests)
  //       return axios.post(ROLE_API_CREATERECEIPT_URL,{
  //         storeHouseId,date,medicineRequests,equipmentRequests
  //       })
  //   } catch (error) {
  //     throw console.log(error);
  //   }
  // }

  getAllReceiptByBranch(idBranch){
    try {
      return axios.get(ROLE_API_GETALLRECEIPTBYBRANCH+"?idBranch="+idBranch)
    } catch (error) {
      throw console.log(error);
    }
  },

  getReceiptInfor(idReceipt){
    try {
      return axios.get(ROLE_API_GETINFORRECEIPT_URL+"?idReceipt="+idReceipt)
    } catch (error) {
      throw console.log(error);
    }
  },

  searchRecieveByStatusAndBranch(idBranch,idStatus ){
    try {
      return axios.get(ROLE_API_RECEIPTSEARCHBYSTATUSANDBRANCH_URL+"?idBranch="+idBranch+"&idStatus="+idStatus)
    } catch (error) {
      throw console.log(error);
    }
  },

  acceptReceipt(idRole,idReceipt,storeHouseId,date,medicineRequests,equipmentRequests){
    try {
      return axios.put(ROLE_API_ACCEPTRECEIPT_URL+"?idRole="+idRole,{idReceipt,storeHouseId,date,medicineRequests,equipmentRequests})
    } catch (error) {
      throw console.log(error);
    }
  },

  cancelReceipt(idReceipt,idRole  ){
    try {
      return axios.put(ROLE_API_CANCELRECEIPT_URL+"?idReceipt="+idReceipt+"&idRole="+idRole)
    } catch (error) {
      throw console.log(error);
    }
  },


  // finishReceipt(idReceipt,storeHouseId,totalCost,idStaff,idStaff,date,medicineRequests,equipmentRequests){
  //   try {
  //     return axios.put("http://localhost:8081/api/v1/receipt-products" ,{idReceipt,storeHouseId,totalCost,idStaff,date,medicineRequests,equipmentRequests})
  //   } catch (error) {
  //     throw console.log(error);
  //   }
  // }

  createReceipt (body){
    return axios.post("http://localhost:8081/api/v1/create-receipt", body)
  },

  getReceiptById(idReceipt){
    try {
      return axios.get("http://localhost:8081/api/v1/receipt-detail-by-id"+"?idReceipt="+idReceipt)
    } catch (error) {
      throw console.log(error);
    }
  },

  receiptProduct(receiptId,personInChargeId,date,productRequests){
    try {
      console.log("http://localhost:8081/api/v1/receipt-products",{receiptId,personInChargeId,date,productRequests})
      return axios.post("http://localhost:8081/api/v1/receipt-products",{receiptId,personInChargeId,date,productRequests})
    } catch (error) {
      throw console.log(error);
    }
  },

  receiptHistory(idReceipt){
    try {
      return axios.get("http://localhost:8081/api/v1/receipt-history-by-id"+"?idReceipt=" +idReceipt)
    } catch (error) {
      throw console.log(error);
    }
  },

  updateStatus(idReceipt ){
    try {
      return axios.put("http://localhost:8081/api/v1/complete-receipt-by-id"+"?idReceipt="+idReceipt)
    } catch (error) {
      throw console.log(error);
    }
  },

  // getReceiptByStatusAndBranch(){
  //   try {
  //     return axios.get("http://localhost:8081/api/v1/receipt-short-by-status,")
  //   } catch (error) {
  //     throw console.log(error);
  //   }
  // }
createReceiptProduct: (body) => {
    return axios.post(RECEIPT_CREATE_API, body);
  },
  getDetailReceipt(idReceipt) {
    return axios.get(RECEIPT_DETAIL_API, { params: { idReceipt }})
  },
  getDetailReceiptFull(idReceipt) {
    return axios.get(RECEIPT_DETAIL_FULL_API, { params: { idReceipt }})
  },
  completeReceipt: (id) => {
    return axios.put(RECEIPT_COMPLETE_API + `?idReceipt=${id}`);
  },
}
export default ReceiptService;