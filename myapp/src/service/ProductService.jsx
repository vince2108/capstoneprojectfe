import axios from "axios";
const PRODUCT_API_GETALLMEDICINE = "http://localhost:8081/api/v1/get-all-medicine-type"
const PRODUCT_API_GETALLEQUIPMENTTYPE = "http://localhost:8081/api/v1/get-all-equipmentType"
const PRODUCT_API_GETALLPRODUCTTYPE1 = "http://localhost:8081/api/v1/get-all-productType1"
const PRODUCT_API_GETALLMEDICINETYPE = "http://localhost:8081/api/v1/get-all-medicineType"
const PRODUCT_API_GETALLPRODUCTGROUP = "http://localhost:8081/api/v1/get-all-product-group"
const PRODUCT_API_CREATEPRODUCTGROUP = "http://localhost:8081/api/v1/create-product-group"
const PRODUCT_API_CREATEPRODUCT = "http://localhost:8081/api/v1/create-product"
const PRODUCT_API_GETALLPRODUCT = "http://localhost:8081/api/v1/get-all-product"
class ProductService {
  getAllMedicine() {
    try {
        return axios.get(PRODUCT_API_GETALLMEDICINE);
    } catch (error) {
        throw console.log(error);
    }
  }
  getAllEquipmentType(){
    try {
        return axios.get(PRODUCT_API_GETALLEQUIPMENTTYPE);
    } catch (error) {
        throw console.log(error);
    }
  }

  getAllProductType(){
    try {
        return axios.get(PRODUCT_API_GETALLPRODUCTTYPE1);
    } catch (error) {
        throw console.log(error);
    }
  }

  getAllMedicineType(){
    try {
      return axios.get(PRODUCT_API_GETALLMEDICINETYPE)
    } catch (error) {
      throw console.log(error);
    }
  }
  
  
  createEquipment(equipmentTypeName,productTypeId,use){
    try {
      return axios.post("http://localhost:8081/api/v1/save-equipmentType",{equipmentTypeName,productTypeId,use})
    } catch (error) {
      throw console.log(error);
    }
  }

  createMedicine(medicineTypeName,productTypeId,use){
    try {
      return axios.post("http://localhost:8081/api/v1/save-medicineType",{medicineTypeName,productTypeId,use})
    } catch (error) {
      throw console.log(error);
    }
  }

  getAllProductGroup(){
    try {
      return axios.get(PRODUCT_API_GETALLPRODUCTGROUP)
    } catch (error) {
      throw console.log(error);
    }
  }

  getAllProDuctAndQuantity(storeId,branchId){
    try {
      return axios.get("http://localhost:8081/api/v1/get-all-product-quantity"+"?storeId=" +storeId+"&branchId=" +branchId)
    } catch (error) {
      throw console.log(error);
    }
  }

  createProductGroup(productGroupName){
    try {
      console.log(PRODUCT_API_CREATEPRODUCTGROUP,{productGroupName})
      return axios.post(PRODUCT_API_CREATEPRODUCTGROUP,{productGroupName})
    } catch (error) {
      throw console.log(error);
    }
  }

  createProduct(productName,unitId,price,note,userObject,using,productGroupIds){
    try {
      return axios.post(PRODUCT_API_CREATEPRODUCT,{productName,unitId,price,note,userObject,using,productGroupIds})
    } catch (error) {
      throw console.log(error);
    }
  }

  getAllProduct(){
    try {
      return axios.get(PRODUCT_API_GETALLPRODUCT)
    } catch (error) {
      throw console.log(error);
    }
  }

  deleteProductGroup(idProductGroup){
    try {
      return axios.delete("http://localhost:8081/api/v1/delete-product-group"+"?idProductGroup="+idProductGroup)
    } catch (error) {
      throw console.log(error);
    }
  }

  getProductQuantity(params) {
    return axios.get("http://localhost:8081/api/v1/product-quantity", { params })
  }
}
export default new ProductService();
