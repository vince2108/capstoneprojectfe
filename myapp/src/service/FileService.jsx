import axios from "axios";
const FILE_API_UPLOADFILE_URL="http://localhost:8081/api/v1/upload-file"
class FileService {
    upLoadFileStaff(file){
        try {
            return axios.post(FILE_API_UPLOADFILE_URL+"?typeUpload=image",file)
        } catch (error) {
            throw console.log(error);
        }
    }

}

export default new FileService();