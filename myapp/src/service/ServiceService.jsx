import axios from "axios";
const Service_API_GETSERVICEBYLISTID_URL = "http://localhost:8081/api/v1/get-all-service-by-listID";
const SERVICE_API_GETALLSERVICE = "http://localhost:8081/api/v1/get-all-service";
const SERVICE_API_CREATENEW = "http://localhost:8081/api/v1/create-service";
const SERVICE_API_SEARCHBYNAME = "http://localhost:8081/api/v1/find-service-by-Name";
class ServiceService {
  getServiceByListId(medicalServiceId) {
    try {
      console.log(Service_API_GETSERVICEBYLISTID_URL,{medicalServiceId})
      return axios.post(Service_API_GETSERVICEBYLISTID_URL,{medicalServiceId})
    } catch (error) {
        throw console.log(error);
    }
  }
  getAllService() {
    try {
      return axios.get(SERVICE_API_GETALLSERVICE)
  } catch (error) {
      throw (console.log(error))
  }
  };
  createNewService(serviceName,
    consultingRoomId,
    money) {
      console.log(SERVICE_API_CREATENEW + ": " +
        "Name: " +serviceName,
        "Id: "+ consultingRoomId,
        "price "+ money);
      try {
        return axios.post(SERVICE_API_CREATENEW,{
          serviceName,
          consultingRoomId,
          money
        })
    } catch (error) {
        throw (console.log(error))
    }
    };
    getServiceByName(name){
      console.log(SERVICE_API_SEARCHBYNAME+'?name='+name)
      try {
          return axios.get(SERVICE_API_SEARCHBYNAME+'?name='+name)
      } catch (error) {
          throw (console.log(error))
      }
  };
  
}
export default new ServiceService();
