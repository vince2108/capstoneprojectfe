import axios from "axios";

const TRANSFER_CREATE_API="http://localhost:8081/api/v1/create-transfer"
const TRANSFER_CREATE_EXPORT_API="http://localhost:8081/api/v1/action-transfer-export"
const TRANSFER_LIST_RECEIPT_API="http://localhost:8081/api/v1/all-transfer-receipt"
const TRANSFER_LIST_RECEIPT_STATUS_API="http://localhost:8081/api/v1/transfer-receipt-by-id"
const TRANSFER_LIST_EXPORT_API="http://localhost:8081/api/v1/all-transfer-export"
const TRANSFER_LIST_EXPORT_STATUS_API="http://localhost:8081/api/v1/transfer-export-by-id"
const TRANSFER_DETAIL_EXPORT_API="http://localhost:8081/api/v1/transfer-short-export"
const TRANSFER_DETAIL_FULL_EXPORT_API="http://localhost:8081/api/v1/transfer-full-export"

const TransferService = {
    createTransfer: (body) => {
      return axios.post(TRANSFER_CREATE_API, body);
    },
    createTransferExport(body) {
      return axios.post(TRANSFER_CREATE_EXPORT_API, body);
    },
    getListReceipt(statusId) {
      if (Number(statusId) !== 0) return axios.get(TRANSFER_LIST_RECEIPT_STATUS_API, { params: {statusId }})
      return axios.get(TRANSFER_LIST_RECEIPT_API)
    },
    getListExport(statusId) {
      if (Number(statusId) !== 0) return axios.get(TRANSFER_LIST_EXPORT_STATUS_API, { params: {statusId }})
      return axios.get(TRANSFER_LIST_EXPORT_API)
    },
    getDetailExport(idExport) {
      return axios.get(TRANSFER_DETAIL_EXPORT_API, { params: { idExport }})
    },
    getDetailExportFull(idExport) {
      return axios.get(TRANSFER_DETAIL_FULL_EXPORT_API, { params: { idExport }})
    },
  }

export default TransferService;