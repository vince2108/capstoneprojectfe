import axios from "axios";

const STOREHOUSE_API_GETALL_URL="http://localhost:8081/api/v1/all-store-house"
const STOREHOUSE_API_CREATE_URL="http://localhost:8081/api/v1/create-store-house"
const STOREHOUSE_API_GETBYID_URL="http://localhost:8081/api/v1/store-house-by-id"
const STOREHOUSE_API_UPDATE_URL="http://localhost:8081/api/v1/update-store-house"
const STOREHOUSE_API_GETSTOREHOUSEBYID_URL = "http://localhost:8081/api/v1/store-house-by-id-branch"
class StoreHouse {
    getAllStoreHouse(){
        try {
            return axios.get(STOREHOUSE_API_GETALL_URL);
          } catch (error) {
            throw console.log(error);
          }
    }

    createStoreHouse(storeHouseName, abbreviationName, address, staffId, branchId,location,isAction){
      console.log(STOREHOUSE_API_CREATE_URL+": "+
     
     "name: " +storeHouseName,
     "brief: " +abbreviationName,
     "addr: " +address,
     "Sid: " +staffId,
     "Bid: " +branchId,
     "Loc: " +location,
     "act: " +isAction
      )
      try {
          return axios.post(STOREHOUSE_API_CREATE_URL,{storeHouseName,abbreviationName,  address, staffId,branchId,location, isAction });
        } catch (error) {
          throw console.log(error);
        }
      }
      getStoreHouseById(storeHouseId) {
        try {
          return axios.get(STOREHOUSE_API_GETBYID_URL + "?idStoreHouse=" + storeHouseId);
        } catch (error) {
          throw console.log(error);
          //console;
        }
      }

      updateStorehouse(storeHouseId,storeHouseName,abbreviationName, address, staffId, branchId,location, isAction
        
        ){
          try {
            return axios.put(STOREHOUSE_API_UPDATE_URL,{
              storeHouseId,storeHouseName,abbreviationName, address, staffId, branchId,location,isAction
            })
          } catch (error) {
            throw console.log(error);
          }
    
      }

      getAllStoreHouseByBranchId(idBranch){
        try {
          return axios.get(STOREHOUSE_API_GETSTOREHOUSEBYID_URL+"?idBranch="+idBranch)
        } catch (error) {
          throw console.log(error);
        }
      }
      
  }

export default new StoreHouse();