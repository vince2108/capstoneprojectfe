import axios from "axios";

const Login_API_BASE_URL = 'http://localhost:8081/api/v1/login'
const Register_API_BASE_URL = 'http://localhost:8081/api/v1/register'
const ForgotPassword = 'http://localhost:8081/api/v1/forgot-password'


class AuthenticationService{
    loginAction(username,password){
        try {
            return axios.post(Login_API_BASE_URL,{username,password})
        } catch (error) {
            throw (console.log(error))
        }
    }

    registerAction(username,password){
        return axios.post(Register_API_BASE_URL,username,password)
    }
    forgotPassword(name){
        console.log("api: " + ForgotPassword + "name: "+ name)
        try {
            return axios.post(ForgotPassword+"?name="+name)
        } catch (error) {
            throw (console.log(error))
        }
    }

}

export default new AuthenticationService();