import axios from "axios";
const MEDICALSERVICE_API_GETALL_URL = 'http://localhost:8081/api/v1/service'

class MedicalServiceService{
    
    getAllMedicalService(idBranch){
        try {
            return axios.get(MEDICALSERVICE_API_GETALL_URL+"?idBranch="+idBranch)
        } catch (error) {
            throw (console.log(error))
        }
    }

}
export default new MedicalServiceService();