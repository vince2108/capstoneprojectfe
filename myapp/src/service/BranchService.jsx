import axios from "axios";

const BRANCH_API_GETALLBRANCH = "http://localhost:8081/api/v1/get-all-branch";
const BRANCH_API_CREATENEW = "http://localhost:8081/api/v1/save-branch";
const BRANCH_API_GETBRANCHBYID ="http://localhost:8081/api/v1/branch"
const BRANCH_API_UPDATE = "http://localhost:8081/api/v1/update-branch"

class BranchService {
  getAllBranch() {
    return axios.get(BRANCH_API_GETALLBRANCH);
  }
  createBranch(  address,
  phoneNumber,
  branchName
){
  console.log(BRANCH_API_CREATENEW + ": "+  address,
  "Phone: "+phoneNumber,
  "name: "+branchName
)
    try {
      return axios.post( BRANCH_API_CREATENEW,{
        address,
        phoneNumber,
        branchName
      });
    } catch (error) {
      throw console.log(error);
    }
  }
  getBranchById(branchId){
    try {
      return axios.get(BRANCH_API_GETBRANCHBYID+"/"+branchId)
    } catch (error) {
      throw console.log(error);
    }
  }
  updateBranch(branchId,
    address,
    phoneNumber,
    branchName
    ){
      try {
        return axios.put(BRANCH_API_UPDATE+"/"+branchId,{
          address,
          phoneNumber,
          branchName
        })
      } catch (error) {
        throw console.log(error);
      }

  }
}
export default new BranchService();
