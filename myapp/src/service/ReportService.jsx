import axios from "axios";
const REPORT_API_REPORTPATIENTEXAMINEDDAY_URL= 'http://localhost:8081/api/v1/revenue-report-day'
class ReportService {
  getReportInforCurrentDate(branchId,month,year,day) {
    try {
        return axios.get("http://localhost:8081/api/v1/revenue-report"+"?branchId="+branchId+"&month="+month+"&year="+year+"&day="+day);
    } catch (error) {
        throw (console.log(error))
    }   
  }

  getPatientExaminedDay(branchId,month,year,day){
    try {
        return axios.get("http://localhost:8081/api/v1/report-patient-examined"+"?branchId="+branchId+"&month="+month+"&year="+year+"&day="+day);
    } catch (error) {
        throw (console.log(error))
    }
  }

  revenueByServiceCurrentDate(branchId ,month,year,day ){
    try {
      return axios.get("http://localhost:8081/api/v1/revenue-by-service" + "?branchId=" + branchId + "&month=" +month+"&year=" +year+"&day="+day);
    } catch (error) {
      throw (console.log(error))
    }
  }

  revenueByService(branchId ,month,year ){
    try {
      return axios.get("http://localhost:8081/api/v1/revenue-by-service" + "?branchId=" + branchId + "&month=" +month+"&year=" +year);
    } catch (error) {
      throw (console.log(error))
    }
  }

  ReportPatientUsingService(branchId,month,year,serviceId ){
    try {
      return axios.get("http://localhost:8081/api/v1/report-patient-used-serviceId" + "?branchId=" + branchId + "&month=" +month+"&year=" +year+"&serviceId="+serviceId);
    } catch (error) {
      throw (console.log(error))
    }
  }

  HistoryPatientUsedService(branchId,month,year,patientId ){
    try {
      return axios.get("http://localhost:8081/api/v1/history-patient-used-service" + "?branchId=" + branchId + "&month=" +month+"&year=" +year+"&patientId="+patientId);
    } catch (error) {
      throw (console.log(error))
    }
  }

}
export default new ReportService();