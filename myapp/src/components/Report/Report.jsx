import "./Report.css";
import {
  MenuFoldOutlined,
  ShopOutlined,
  MenuUnfoldOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import {
  Layout,
  Menu,
  Avatar,
  Card,
  Button,
  Row,
  Col,
  Dropdown,
  message,
  Select,
  Tabs,
  Table,
} from "antd";
import React, { useState, useEffect } from "react";
import { Input, Space } from "antd";
import StaffService from "../../service/StaffService";
import { Link, useNavigate } from "react-router-dom";
import ReportService from "../../service/ReportService";
import { click } from "@testing-library/user-event/dist/click";

function Report() {
  let newDate = new Date();
  let day = newDate.getDate();
  let month = newDate.getMonth() + 1;
  let year = newDate.getFullYear();
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [theme2] = useState("light");
  const [current, setCurrent] = useState("1");
  const [currentDateData, setCurrentDateData] = useState([]);
  const [patientExaminedDay, setPatientExaminedDay] = useState();
  const [yearCombox, setYearCombox] = useState();
  const [yearSelected, setYearSelected] = useState(year);
  const [monthSelected,setMonthSelected] = useState(1);
  const [dataForServiceCurrentDay,setDataForServiceCurrentDay] = useState([]);
  const [dataForServiceMonthYear,setDataForServiceMonthYear] = useState([]);
  const [reportPatientUsingService,setReportPatientUsingService] = useState([]);
  const [historyPatientUsedService,setHistoryPatientUsedService] = useState([]);
  
  const navigate = useNavigate();

  // const currentDate = date + "/" + month + "/" + year;
  // let currentDateFormYYYYMMDD = year + "-" + month + "-" + date;

  const numbersOfMonth = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  const numbersOfYear = [year, year - 1, year - 2, year - 3];

  useEffect(() => {
    const setDataForPage = async () => {
      const res = await ReportService.getReportInforCurrentDate(
        localStorage.getItem("branchId"),
        month,
        year,
        day
      );
      const res2 = await ReportService.getPatientExaminedDay(
        localStorage.getItem("branchId"),
        month,
        year,
        day
      );
      const res3 = await ReportService.revenueByServiceCurrentDate(localStorage.getItem("branchId"),
      month,
      year,
      day)
      setCurrentDateData(res.data.data);
      setPatientExaminedDay(res2.data.data);
      setDataForServiceCurrentDay(res3.data.data)
      console.log(res.data.data);
    };
    setDataForPage();
  }, []);

  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/staff");
    }
    if (e.key == 2) {
      navigate("/clinic");
    }
    if (e.key == 3) {
      navigate("/report");
    }
    if (e.key == 4) {
      navigate("/service");
    }
    if (e.key == 5) {
      navigate("/divison");
    }
    if (e.key == 6) {
      navigate("/listReceiptManager");
    }
    if (e.key == 7) {
      navigate("/ListTransfer");
    }
    if (e.key == 8) {
      navigate("/inventoryManager");
    }
    if (e.key == 9) {
      // navigate("/inventoryManager")
    }
    if (e.key == 10) {
      navigate("/warehouse");
    }
  };

  const getMonthToShow = async (key) =>{
    console.log(key)
    console.log(yearSelected)
    setMonthSelected(key)
    if(yearSelected !== undefined){
      const res = await ReportService.revenueByService(localStorage.getItem("branchId"),key,yearSelected)
      setDataForServiceMonthYear(res.data.data)
      console.log(res.data.data)
    }
  }

  const yearValue = async (year) => {
    console.log(year)
    setYearSelected(year)
    if(monthSelected !== undefined){
      const res = await ReportService.revenueByService(localStorage.getItem("branchId"),monthSelected,year)
      setDataForServiceMonthYear(res.data.data)
      console.log(res.data.data)
    }
  }

  const getDetailService = async(medicalServiceId)=>{
    console.log(medicalServiceId)
    document.getElementById("this-dialog").showModal();
    const res =  await ReportService.ReportPatientUsingService(localStorage.getItem("branchId"),monthSelected,year,medicalServiceId)
    setReportPatientUsingService(res.data.data)
  }

  

  const getDetailPatient = async(accountId)=>{
    console.log(accountId)
    document.getElementById("this-dialog-detail-patient").showModal();
    const res =  await ReportService.HistoryPatientUsedService(localStorage.getItem("branchId"),monthSelected,year,accountId)
    setHistoryPatientUsedService(res.data.data)
  }

  const ConfirmAciton = () => {
    document.getElementById("this-dialog").close();
  };

  const ConfirmPatientAciton = () => {
    document.getElementById("this-dialog-detail-patient").close();
  };

  

  // search
  const { Search } = Input;
  const onSearch = (value) => {};

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];

  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo"> </div>
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub1"]}
          selectedKeys={["3"]}
          mode="inline"
          items={MenuItem}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >
                <div>
                  <Avatar
                    size="default"
                    style={{ marginRight: 10 }}
                    src={window.localStorage.getItem("avatar")}
                  ></Avatar>
                  {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <dialog id="this-dialog">
            <p>Chi Tiết Khám Của Dịch Vụ</p>
            <Table
                      className="transfer-table"
                      columns={detailServiceTable}
                      dataSource={reportPatientUsingService}
                      pagination={false}
                      onRow={(record) => {
                        return {
                          onClick: () => getDetailPatient(record?.accountId), // click row
                        };
                      }}
                    />
                    <button onClick={() => ConfirmAciton()}>Đóng</button>
          </dialog>
          <dialog id="this-dialog-detail-patient">
            <p>Chi Tiết Khám Của Bệnh Nhân</p>
            <Table
                      className="transfer-table"
                      columns={detailPatientTable}
                      dataSource={historyPatientUsedService}
                      pagination={false}
                      // onRow={(record) => {
                      //   return {
                      //     onClick: () => getDetailPatient(record?.accountId), // click row
                      //   };
                      // }}
                    />
                    <button onClick={() => ConfirmPatientAciton()}>Đóng</button>
          </dialog>
          <div>
            <p className="title-report-day">HOẠT ĐỘNG TRONG NGÀY</p>
          </div>
          <div
            style={{
              display: "inline-flex",
            }}
          >
            <Card className="card-tu" size="small" title="Lươt khám">
              <p className="card-body">{patientExaminedDay}</p>
            </Card>

            <Card className="card-mn" size="small" title="Tiền">
              <p className="card-body">
                {currentDateData[0]?.totalCost === undefined? "0 VNĐ" : currentDateData[0]?.totalCost +" VNĐ"}
              </p>
            </Card>
          </div>

          <div>
            <p>Chi Tiết Khám</p>
            <Table
                      className="transfer-table"
                      columns={serviceTable}
                      dataSource={dataForServiceCurrentDay}
                      pagination={false}
                      // onRow={(record) => {
                      //   return {
                      //     onClick: () => getDetailPatient(record?.receiptId), // click row
                      //   };
                      // }}
                    />
          </div>

          <div style={{ marginTop: "30px", marginLeft: "30px" }}>
            <p className="title-report-month">
              HOẠT ĐỘNG TRONG NĂM
              <Select
                style={{
                  width: "10vh",
                  marginLeft: "10px",
                  marginTop: "20px",
                }}
              >
                {numbersOfYear?.map((year, index) => {
                  return (
                    <select
                      onChange={(event) => setYearCombox(event.target.value)}
                      value={yearCombox}
                      key={index}
                    >
                      <option
                      onClick={() =>
                        yearValue(year)
                      }
                      >
                        {year}
                      </option>
                    </select>
                  );
                })}
              </Select>
            </p>

            <Tabs defaultActiveKey="1" onChange={(month) => getMonthToShow(month)}>
              {numbersOfMonth?.map((month) => {
                return (
                  <Tabs.TabPane tab={"Tháng " + month} key={month}>
                    <Table
                      className="transfer-table"
                      columns={serviceTable}
                      dataSource={dataForServiceMonthYear}
                      pagination={false}
                      onRow={(record) => {
                        return {
                          onClick: () => getDetailService(record?.medicalServiceId), // click row
                        };
                      }}
                    />
                  </Tabs.TabPane>
                );
              })}

            
            </Tabs>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}

const MenuItem = [
  getItem("Quản lý", "sub1", <ShopOutlined />, [
    getItem("QL Nhân viên", "1"),
    getItem("QL Phòng khám", "2"),
    getItem("Báo cáo phòng khám", "3"),
    getItem("QL Dịch vụ", "4"),
    // getItem("QL Phòng ban", "5"),
  ]),
  getItem("Quản lý Kho Hàng ", "sub2", <ShopOutlined />, [
    getItem("Nhập kho", "6"),
    getItem("Điều chuyển", "7"),
    getItem("Kiểm kê", "8"),
    // getItem("Báo cáo kho", "9"),
    getItem("Quản lý kho", "10"),
  ]),
];

const onChange = (key) => {
  console.log(key);
};
const columnReceipt1 = [
  {
    key: "1",
    label: `Tháng 1`,
    children: `Content of Tab Pane 1`,
  },
  {
    key: "2",
    label: `Tháng 2`,
    children: `Content of Tab Pane 2`,
  },
  {
    key: "3",
    label: `Tháng 3`,
    children: `Content of Tab Pane 3`,
  },
  {
    key: "4",
    label: `Tháng 4`,
    children: `Content of Tab Pane 3`,
  },
  {
    key: "5",
    label: `Tháng 5`,
    children: `Content of Tab Pane 3`,
  },
  {
    key: "6",
    label: `Tháng 6`,
    children: `Content of Tab Pane 3`,
  },
  {
    key: "7",
    label: `Tháng 7`,
    children: `Content of Tab Pane 3`,
  },
  {
    key: "8",
    label: `Tháng 8`,
    children: `Content of Tab Pane 3`,
  },
  {
    key: "9",
    label: `Tháng 9`,
    children: `Content of Tab Pane 3`,
  },
  {
    key: "10",
    label: `Tháng 10`,
    children: `Content of Tab Pane 3`,
  },
  {
    key: "11",
    label: `Tháng 11`,
    children: `Content of Tab Pane 3`,
  },
  {
    key: "12",
    label: `Tháng 12`,
    children: `Content of Tab Pane 3`,
  },
];

const detailPatientTable = [
  {
    key: "serviceName",
    dataIndex: "serviceName",
    title: "Tên Dịch Vụ",
  },
  {
    key: "date",
    dataIndex: "date",
    title: "Ngày Khám",
  },
  {
    key: "priceService",
    dataIndex: "priceService",
    title: "Tổng Tiền",
  },
];

const detailServiceTable = [
  {
    key: "accountId",
    dataIndex: "accountId",
    title: "Mã Bệnh Nhân",
  },
  {
    key: "patientName",
    dataIndex: "patientName",
    title: "Tên bệnh nhân",
  },
  {
    key: "priceService",
    dataIndex: "priceService",
    title: "Tổng Tiền",
  },
];

const serviceTable = [
  {
    key: "serviceName",
    dataIndex: "serviceName",
    title: "Tên dịch vụ",
  },
  {
    key: "medicalVisits",
    dataIndex: "medicalVisits",
    title: "Lượt Khám",
  },
  {
    key: "totalCost",
    dataIndex: "totalCost",
    title: "Tổng Tiền",
  },
];

export default Report;
