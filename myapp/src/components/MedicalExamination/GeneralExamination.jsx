import "./MedicalExamination.css";
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  AppstoreOutlined,
  LogoutOutlined,
  EditOutlined,

} from "@ant-design/icons";
import { Layout, Menu, Row, Col, Avatar, Button, Select, Form, message,
  Dropdown, } from "antd";
import React, { useEffect, useState } from "react";
import { Input, Space } from "antd";
import ConsultingRoomService from "../../service/ConsultingRoomService";
import DoctorService from "../../service/DoctorService";
import PatientService from "../../service/PatientService";
import { Link, useNavigate } from "react-router-dom";
function GeneralExamination() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");
  const [patientInforState, setPatientInforState] = useState([]);
  const [patientMedicalRecord, setPatientMedicalRecord] = useState([]);
  const [selectId, setSelectId] = useState();
  const [currentConsultingRoomState, setCurrentConsultingRoomState] = useState(
    []
  );
  const [loading, setLoading] = useState(false);
  const [allPatientByConsultingRoom, setAllPatientByConsultingRoom] = useState(
    []
  );
  const navigate = useNavigate();
  useEffect(() => {
    const getCurrentConsutingRoomFun = async () => {
      const res = await ConsultingRoomService.getConsultingRoomById(
        localStorage.getItem("consultingRoom")
      );
      setCurrentConsultingRoomState(res.data.data);
      console.log(res.data.data);
    };
    getCurrentConsutingRoomFun();
  }, []);
  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/checkUp");
    }
    if (e.key == 2) {
      navigate("/medicalRecord");
    }
    if (e.key == 3) {
      navigate("/listIssueDoctor");
    }

  };

  useEffect(() => {
    const getAllPatientFuntionFun = async () => {
      console.log(localStorage.getItem("consultingRoom"));
      const res = await DoctorService.getPatientByConsultingRoom(
        localStorage.getItem("consultingRoom"),
        localStorage.getItem("branchId")
      );
      setAllPatientByConsultingRoom(res.data.data);
      setLoading(true);
      console.log(res.data.data);
    };
    getAllPatientFuntionFun();
  }, [loading]);

  const changeSTTAction = (patientId, stt) => {
    const res = DoctorService.updateIndexPatient(
      patientId,
      localStorage.getItem("consultingRoom"),
      stt
    );
    setLoading();
  };

  const getPatientInformationFun = async (accountId) => {
    document.getElementById("patient-infor").style.display = "block";
    console.log(accountId);
    const res = await PatientService.getPatientById(accountId);
    const res2 = await DoctorService.getPatientMedicalRecordById(accountId);
    setPatientInforState(res.data.data);
    setPatientMedicalRecord(res2.data.data);
    setSelectId(accountId);
    console.log(res.data.data);
    console.log(res2.data.data);
  };

  const startService = () => {
    const res = DoctorService.updateStatusExamnination(selectId,localStorage.getItem("consultingRoom"))
    navigate("/detailMedicalRecord", {
      state: { selectId: selectId, canEdit : true },
    });
  };

  const { TextArea } = Input;

  // search
  const { Search } = Input;
  const onSearch = (value) => console.log(value);
  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };
  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];
  return (
    
<Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo" />
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub1"]}
          selectedKeys={["1"]}
          mode="inline"
          items={Menuitems}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >  
              <div>
                <Avatar
                  size="default"
                  style={{ marginRight: 10 }}
                  src={window.localStorage.getItem("avatar")}
                ></Avatar>
                {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <Row>
            <Col md={16} className="bobobo">
              <div className="medical-ex">
                <Row>
                  <Col md={1}></Col>
                  <Col md={16}>
                    <h2>Danh Sách Khám</h2>
                    <div>
                      Phòng khám: &ensp;
                      <Input
                        style={{
                          width: "200px",
                          marginLeft: "30px",
                        }}
                        value={currentConsultingRoomState.roomName}
                      />
                    </div>
                  </Col>
                </Row>
              </div>
              <div style={{ margin: "20px 0px 0px 0px " }}>
                <Row>
                  <Col md={1}></Col>
                  <Col md={22}></Col>
                </Row>
              </div>
              <div class="container">
                <table className="tab1">
                  <thead>
                    <th>Số TT</th>
                    <th>Tên BN</th>
                    <th>Giới Tính</th>
                    <th>Trạng thái</th>
                    <th></th>
                  </thead>
                  <tbody>
                    {allPatientByConsultingRoom === null
                      ? null
                      : allPatientByConsultingRoom.map((patient) => {
                          return (
                            <tr
                              onClick={() =>
                                getPatientInformationFun(patient.accountId)
                              }
                            >
                              <td data-label="col-1">{patient.stt}</td>
                              <td data-label="col-3">{patient.fullName}</td>
                              <td data-label="col-5">{patient.sex}</td>
                              <td
                                data-label="col-5"
                                style={{
                                  font: "italic",
                                }}
                              >
                                {patient.statusName}...
                              </td>
                              <td>
                                <Button
                                  onClick={() =>
                                    changeSTTAction(
                                      patient.accountId,
                                      patient.stt
                                    )
                                  }
                                >
                                  Next
                                </Button>
                              </td>
                            </tr>
                          );
                        })}
                  </tbody>
                </table>
              </div>
            </Col>
            <Col
              id="patient-infor"
              md={8}
              style={{
                display: "none",
              }}
            >
              <div style={{ margin: "0px 0px 0px 10px" }}>
                <h1>Thông Tin Bệnh Nhân</h1>
                <Form>
                  <div>
                    <label>Mã Bn: {patientInforState.accountId}</label>{" "}
                    &emsp;&emsp;&emsp;&emsp;&emsp;
                    <br></br>
                    <label>Họ Tên: {patientInforState.fullName}</label>
                    <br></br>
                    <label>Giới Tính: {patientInforState.sex}</label>
                    <br></br>
                    <label>Ngày sinh: {patientInforState.dob}</label>
                    <br></br>
                    <label>
                      Địa chỉ:{" "}
                      {patientInforState.address +
                        ", " +
                        patientInforState.village +
                        ", " +
                        patientInforState.district +
                        ", " +
                        patientInforState.province}
                    </label>
                  </div>
                  <Form.Item label="CD ban đầu">
                    <TextArea
                      rows={4}
                      placeholder="this text will show in the textarea"
                      value={
                        patientMedicalRecord.note === null
                          ? " "
                          : patientMedicalRecord.note
                      }
                    ></TextArea>
                  </Form.Item>
                  <Form.Item>
                    <Button id="start-exam" onClick={() => startService()}>
                      Khám Bệnh
                    </Button>
                  </Form.Item>
                </Form>
              </div>
            </Col>
          </Row>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const Menuitems = [
  getItem("Khám Bệnh", "sub1", <AppstoreOutlined />, [
    getItem("Danh sách chờ", "1"),
  ]),
  getItem("Hồ sơ bệnh nhân", "sub2", <AppstoreOutlined />, [
    getItem(" Hồ Sơ bệnh  nhân", "2"),
  ]),
  getItem("Yêu cầu xuất thuốc", "sub3", <AppstoreOutlined />, [
    getItem(" Yêu cầu xuất thuốc", "3"),
  ]),
];

export default GeneralExamination;
