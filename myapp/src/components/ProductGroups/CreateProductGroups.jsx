import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  ShopOutlined,
  AppstoreOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { Layout, Menu, Row, Col, Avatar, Button, Form, Select, Input, message,
  Dropdown, } from "antd";
import React, { useEffect, useState } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import ProductService from "../../service/ProductService";
import "./CreateProductGroups.css";

function CreateProductGroups() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");
  const [allProductType, setAllProductType] = useState([])
  const [productType, setProductType] = useState()
  const [prodcutTypeId, setProductTypeId] = useState()
  const [inputProductName, setInputProductName] = useState()
  const navigate = useNavigate();

  useEffect(()=>{
    const getAllProductTypeFun = async () =>{
      const res = await ProductService.getAllProductType();
      setAllProductType(res.data.data)
    }
    getAllProductTypeFun();
  },[])

  const BackAction =()=>{
    navigate(-1)
  }

  const CreateAction =()=>{
    const use = true
    if(productType === 2){
      const res = ProductService.createEquipment(inputProductName,prodcutTypeId,use)
      // navigate("/productGroups")
    }
    if(productType===3){
      const res = ProductService.createMedicine(inputProductName,prodcutTypeId,use)
      // navigate("/productGroups")
    }
  }

  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/overview");
    }
    if (e.key == 2) {
      navigate("/listTransfer");
    }
    if (e.key == 3) {
      navigate("/inventoryWHstaff");
    }
    if (e.key == 4) {
      navigate("/listReceiptWHstaff");
    }
    if (e.key == 5) {
      navigate("/listIssueWHstaff");
    }
    if (e.key == 6) {
      navigate("/productGroups");
    }
    if (e.key == 7) {
      navigate("/unitGroup");
    }
    if (e.key == 8) {
      navigate("/unit");
    }
  };

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];
  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo"> </div>
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub1"]}
          selectedKeys={["6"]}
          mode="inline"
          items={MenuItem}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >
                <div>
                  <Avatar
                    size="default"
                    style={{ marginRight: 10 }}
                    src={window.localStorage.getItem("avatar")}
                  ></Avatar>
                  {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <div className="div1-cpdg">
            <Row>
              <Col md={2}>
              <Button style={{ margin: "0px 0px 0px 10px" }}
                onClick={()=>CreateAction()}>Tạo</Button>
              </Col>
              <Col md={2}>
              <Button 
                style={{ margin: "0px 0px 0px 10px" }}
                onClick={()=>BackAction()}>Hủy</Button>
              </Col>
              <Col md={16}></Col>
              <Col md={4}>
                <i className="AppstoreOutlined">
                  <AppstoreOutlined /> Số Lượng
                </i>
              </Col>
            </Row>
          </div>
          <div className="div2-cpdg">
            <Form
              labelCol={{
                span: 4,
              }}
              wrapperCol={{
                span: 14,
              }}
              layout="horizontal"
            >
              <Form.Item label="Danh mục" label-size="20x">
              <Input onChange={(event)=> setInputProductName(event.target.value)} value={inputProductName}></Input>
              </Form.Item>

              <Form.Item label="Danh mục cha" label-size="20x">
                  {/* {

                    allProductType === null ? null : allProductType.map((product)=>{
                      return (
                        <select
                          onChange={(event) => setProductType(event.target.value)}
                          value={productType}
                          key={product.productTypeId}
                          >
                          <option
                          onClick={()=>setProductTypeId(product.productTypeId)}>
                            {product.name}
                          </option>
                        </select>
                      )
                    })
                   } */}
                  <Select onChange={(key) => setProductType(key)}>
                  <Select.Option value={2}>Vật Tư</Select.Option>
                  <Select.Option value={3}>Thuốc</Select.Option>
                </Select>
              </Form.Item>
            </Form>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const MenuItem = [
  getItem("Kho Hàng ", "sub1", <ShopOutlined />, [
    getItem("Tổng quan", "1"),
    getItem("Điều chuyển", "2"),
    getItem("Kiểm kê", "3"),
    getItem("Nhập kho", "4"),
    getItem("Xuất kho", "5"),
    getItem("Nhóm sản phẩm", "6"),
    getItem("Nhóm đơn vị", "7"),
    getItem("Đơn vị", "8"),
  ]),
];

export default CreateProductGroups;
