import "./login.css";
import { Link, useNavigate } from "react-router-dom";
import { useState } from "react";

import { useEffect } from "react";
import AuthenticationService from "../../service/AuthenticationService";
import { Alert, message } from "antd";

// import useAuth from "../../Hooks/useAuth";
const Login = () => {
  const [username, setUserName] = useState();
  const [password, setPassword] = useState();
  const [errorMessage, setErrorMessage] = useState();
  const navigate = useNavigate();
  const [caplockMessage, setCaplockMessage] = useState(false);

// const { setAuth } = useAuth();

const onKeyDown = (event) => {
  if (event.getModifierState("CapsLock")) {
    setCaplockMessage(true);
  } else {
    setCaplockMessage(false);
  }
};

  const useLoginAction = async event => {
    event.preventDefault();
    if (true) {
      const res = await AuthenticationService.loginAction(username, password);
      console.log(res.data.data);

      if (res.data.status == "Success") {
        console.log("ok");
        localStorage.setItem("loginToken", true);
        localStorage.setItem("RoleToken", res.data.data.roleId);
        localStorage.setItem("idToken", res.data.data.accountId);
        localStorage.setItem("usernameToken",res.data.data.fullName)
        localStorage.setItem("branchId", res.data.data.branchId);
        localStorage.setItem("avatar", res.data.data.avatar);
        console.log(res.data.data.roleId);
        console.log(res.data.data.accountId);
        console.log(res.data.data.fullName);
        console.log(res.data.data.branchId);
        // setAuth({ username, password, roles });
        localStorage.setItem("accountToken", JSON.stringify(res.data.data));

        message.success("Đăng nhập thành công");

        if (res.data.data.roleId == "4") {
          // localStorage.setItem("consultingRoom", res.data.data.consultingId);
          navigate("/receive");
          console.log("den reveive")
        }
        if (res.data.data.roleId == "3") {
          console.log(res.data.data.consultingId)
          localStorage.setItem("consultingRoom", res.data.data.consultingId);
          if (
            res.data.data.consultingId == "1" ||
            res.data.data.consultingId == "2"
          ) {
            navigate("/checkUp");
          } else if (res.data.data.consultingId == "6") {
            navigate("generalExamination");
          } else {
            navigate("/medicalExamination");
          }
        }
        if(res.data.data.roleId == '2'){
          navigate('/base')
        }
        // navigate(from, {replace: true});
        if(res.data.data.roleId == '6'){
         
          // navigate('/listTransfer')
          navigate('/staff')
        }
        if(res.data.data.roleId == '5'){
         
          // navigate('/listTransfer')
          navigate('/overview')
        }
      }
      if (res.data.status !== "Success") {
     
        setErrorMessage(res.data.message)
        document.getElementById('login-error').style.display = "block";
        // errorInformation == "sai tài khoản hoặc mật khẩu"
        // window.setTimeout(document.getElementById('login-error').style.display = "none", 5000)
      }
    }
  };

  useEffect(() => {
    setErrorMessage("");
  }, [username, password]);


  return (
    <section className="login-container">
      <div className="login-title"> ĐĂNG NHẬP</div>
      <form onSubmit={useLoginAction}>
      <label>Tên đăng nhập</label>
        <input
          type="text"
          placeholder="Nhập Tên Đăng Nhập"
          onChange={(event) => setUserName(event.target.value)}
          value={username}
          required
          onKeyDown={(event) => { 
            onKeyDown(event);
          }}
          onInvalid={(e) => e.target.setCustomValidity("Vui lòng điền tên đăng nhập")}
          onInput={(e) => e.target.setCustomValidity("")}
        />
        {caplockMessage && (
          <Alert
            message="Đang bật capslock!"
            type="warning"
            showIcon
            closable
          />
        )}
        <label>Mật khẩu</label>
        <input
          type="password"
          placeholder="Nhập Mật Khẩu "
          onChange={(event) => setPassword(event.target.value)}
          value={password}
          required
          onInvalid={(e) => e.target.setCustomValidity("Vui lòng điền mật khẩu")}
          onInput={(e) => e.target.setCustomValidity("")}
        />
        <label className="login-error" id="login-error">
          {errorMessage}
        </label>
        <Link to="/forgotPassword" className="ForgotPassword">Quên mật khẩu</Link><br/>
        <button type="submit"> ĐĂNG NHẬP </button> &nbsp;&nbsp;&nbsp;&nbsp;
        
      </form>
    </section>
  );
};

export default Login;
