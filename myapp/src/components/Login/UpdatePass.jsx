import "./UpdatePass.css";
function UpdatePass() {
  return (
    <Layout>
      <h2>CẬP NHẬT MẬT KHẨU</h2>
          <div className="div-bodycontent-updatepass" style={{ margin: "15px 0px 0px 0px" }}>
            <Form
              labelCol={{
                span: 5,
              }}
              wrapperCol={{
                span: 15,
              }}
            >
                <Form.Item label="Mật khẩu cũ">
                  <Input />
                </Form.Item>
                <Form.Item label="Mật  Khẩu mới">
                  <Input />
                </Form.Item>
                <Form.Item label="Xác nhận Mật  Khẩu">
                  <Input />
                </Form.Item>
            </Form>
            <div>
              <Row>
                <Col md={8}></Col>
                <Col md={4}>
                  <Button>Lưu</Button>
                </Col>
                <Col md={4}>
                  <Button>Hủy Bỏ</Button>
                </Col>
              </Row>
            </div>
          </div>
    </Layout>
  );
}

