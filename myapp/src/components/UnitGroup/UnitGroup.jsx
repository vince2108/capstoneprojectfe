import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  ShopOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { Layout, Menu, Row, Col, Avatar, Button, Checkbox, Input, message,
  Dropdown, } from "antd";
import React, { Fragment, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import UnitService from "../../service/UnitService";
import "./UnitGroup.css";

function UnitGroup() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");
  const [unitGroupState, setUnitGroupState] = useState([]);
  const [createInputValue, setCreateInputValue] = useState();
  const [selectedUnitGroupId,setSelectedUnitGroupId] = useState([]);
  const [loading,setLoading] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    const getAllUnitGroup = async () => {
      const res = await UnitService.getAllUniGroup();
      console.log(res.data.data);
      setUnitGroupState(res.data.data);
      setLoading(true);
    };
    getAllUnitGroup();
  }, [loading]);



  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/overview");
    }
    if (e.key == 2) {
      navigate("/listTransfer");
    }
    if (e.key == 3) {
      navigate("/inventoryWHstaff");
    }
    if (e.key == 4) {
      navigate("/listReceiptWHstaff");
    }
    if (e.key == 5) {
      navigate("/listIssueWHstaff");
    }
    if (e.key == 6) {
      navigate("/productGroups");
    }
    if (e.key == 7) {
      navigate("/unitGroup");
    }
    if (e.key == 8) {
      navigate("/unit");
    }
    
    if (e.key == 9) {
      navigate("/product");
    }
  };

  const handleKeyDown = async(event) => {
    if (event.key === "Enter") {
      const res =await UnitService.createUnitGroup(createInputValue);
      setLoading();
      document.getElementById('button-create').style.display = "none";
      setCreateInputValue()
    }
  };

  const CreateActionFun = () => {
    document.getElementById('button-create').style.display = "block";
  };

  const DeleteActionFun =async () =>{
    console.log(selectedUnitGroupId)
    const res = await UnitService.deleteUnitGroup(selectedUnitGroupId)
    setLoading();
  }

  const setUnitGroupCheckedId =(event,unitGroupsId)=>{
    if(event.target.checked){
      setSelectedUnitGroupId([...selectedUnitGroupId, unitGroupsId])
    }
    if(!event.target.checked){
      const id =Object.values(selectedUnitGroupId).filter(id=>id!==unitGroupsId)
      console.log(id)
      setSelectedUnitGroupId(id)
    }
  }

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];
  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo"> </div>
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub1"]}
          selectedKeys={["7"]}
          mode="inline"
          items={MenuItem}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >
                <div>
                  <Avatar
                    size="default"
                    style={{ marginRight: 10 }}
                    src={window.localStorage.getItem("avatar")}
                  ></Avatar>
                  {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <div className="div1-ug">
            <Row>
              <Col md={2}>
                <Button
                  style={{ margin: "0px 0px 0px 10px" }}
                  onClick={() => CreateActionFun()}
                >
                  Tạo
                </Button>
              </Col>
              <Col md={2}>
                <Button style={{ margin: "0px 0px 0px 10px" }}
                onClick={() => DeleteActionFun()}>Xóa</Button>
              </Col>
            </Row>
          </div>
          <div className="div2-ug">
            <Row className="Row">
              <Col md={4}>
                {unitGroupState === null
                  ? null
                  : unitGroupState.map((unitGroup) => {
                      return (
                        <Fragment>
                          <Checkbox
                            key={unitGroup.unitGroupId}
                            onChange={(event) => setUnitGroupCheckedId(event,unitGroup.unitGroupId)}
                          >
                            {unitGroup.name}
                          </Checkbox>{" "}
                          <br></br>
                        </Fragment>
                      );
                    })}
                <br />
                <Input
                  id="button-create"
                  style={{
                    display: "none",
                  }}
                  required
                  onKeyPress={(event) => {
                    handleKeyDown(event)
                  }}
                  value={createInputValue}
                  onChange={(event) => setCreateInputValue(event.target.value)}
                ></Input>
              </Col>
            </Row>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const MenuItem = [
  getItem("Kho Hàng ", "sub1", <ShopOutlined />, [
    getItem("Tổng quan", "1"),
    getItem("Điều chuyển", "2"),
    getItem("Kiểm kê", "3"),
    getItem("Nhập kho", "4"),
    getItem("Xuất kho", "5"),
    getItem("Nhóm sản phẩm", "6"),
    getItem("Nhóm đơn vị", "7"),
    getItem("Đơn vị", "8"),
    getItem("Sản phẩm", "9"),
  ]),
];

export default UnitGroup;
