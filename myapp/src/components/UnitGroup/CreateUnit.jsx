import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  ShopOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import {
  Layout,
  Menu,
  Row,
  Col,
  Avatar,
  Button,
  Checkbox,
  Select,
  Form,
  Input, message,
  Dropdown,
} from "antd";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import UnitService from "../../service/UnitService";
import "./CreateUnit.css";

function CreateUnit() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");
  const [allUnitGroupState,setAllUnitGroupState] = useState([]);
  const [unitGroupState,setUnitGroupState] = useState();
  const [unitGroupId,setUnitGroupId] = useState();
  const [unitName, setUnitName] = useState();
  const navigate = useNavigate();
  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/overview");
    }
    if (e.key == 2) {
      navigate("/listTransfer");
    }
    if (e.key == 3) {
      navigate("/inventoryWHstaff");
    }
    if (e.key == 4) {
      navigate("/listReceiptWHstaff");
    }
    if (e.key == 5) {
      navigate("/listIssueWHstaff");
    }
    if (e.key == 6) {
      navigate("/productGroups");
    }
    if (e.key == 7) {
      navigate("/unitGroup");
    }
    if (e.key == 8) {
      navigate("/unit");
    }
    if (e.key == 9) {
      navigate("/product");
    }
  };


  useEffect (()=>{
    const getAllUnitGroupData = async ()=>{
      const res = await UnitService.getAllUniGroup();
      setAllUnitGroupState(res.data.data)
    }
    getAllUnitGroupData();
  },[])

  const BackAction =()=>{
    navigate(-1)
  }

  const SaveAction =async()=>{
    const res = await UnitService.createUnit(unitName,unitGroupId);
    navigate('/unit')
  }


  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];
  return (
    <Layout>
    <Sider trigger={null} collapsible collapsed={collapsed}>
      <div className="logo"> </div>
      <Menu
        theme={theme}
        onClick={onClick}
        defaultOpenKeys={["sub1"]}
        selectedKeys={["8"]}
        mode="inline"
        items={MenuItem}
      />
    </Sider>
    <Layout className="site-layout">
      <Header
        className="site-layout-background"
        style={{
          padding: 0,
        }}
      >
        <Row>
          <Col md={20}>
            {React.createElement(
              collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
              {
                className: "trigger",
                onClick: () => setCollapsed(!collapsed),
              }
            )}
          </Col>
          <Col md={4}>
            <Dropdown
              menu={{
                items,
                onClick: handleMenuClick,
              }}
              placement="bottomLeft"
            >
              <div>
                <Avatar
                  size="default"
                  style={{ marginRight: 10 }}
                  src={window.localStorage.getItem("avatar")}
                ></Avatar>
                {window.localStorage.getItem("usernameToken")}
              </div>
            </Dropdown>
          </Col>
        </Row>
      </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <div className="div1-cu">
            <Row>
              <Col md={2}>
                <Button style={{ margin: "0px 0px 0px 10px" }}
                onClick={() => SaveAction()}>Lưu</Button>
              </Col>
              <Col md={2}>
                <Button style={{ margin: "0px 0px 0px 10px" }} 
                onClick={()=>BackAction()}>Hủy Bỏ</Button>
              </Col>
            </Row>
          </div>
          <div className="div2-cu">
            <Row>
              <Col md={16}>
                <Form
                  labelCol={{
                    span: 4,
                  }}
                  wrapperCol={{
                    span: 14,
                  }}
                  layout="horizontal"
                >
                  <Form.Item label="Đơn vị" label-size="20x">
                    <Input onChange={(event)=> setUnitName(event.target.value)} value={unitName}></Input>
                  </Form.Item>
                  <Form.Item label="Chuyên mục" label-size="20x">
                    <Select
                      style={{
                        width: "100%",
                      }}
                    >
                    {
                      allUnitGroupState===null? null : allUnitGroupState.map((unitGroup)=>{
                        return (
                          <select
                            onChange={(event) => setUnitGroupState(event.target.value)}
                            value={unitGroupState}
                            key={unitGroup.unitGroupId}
                            >
                            <option
                              onClick={()=> 
                                setUnitGroupId(unitGroup.unitGroupId)}>
                              {unitGroup.name}
                            </option>
                          </select>
                        )
                      })
                    }
                    </Select>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const MenuItem = [
  getItem("Kho Hàng ", "sub1", <ShopOutlined />, [
    getItem("Tổng quan", "1"),
    getItem("Điều chuyển", "2"),
    getItem("Kiểm kê", "3"),
    getItem("Nhập kho", "4"),
    getItem("Xuất kho", "5"),
    getItem("Nhóm sản phẩm", "6"),
    getItem("Nhóm đơn vị", "7"),
    getItem("Đơn vị", "8"),
    getItem("Sản phẩm", "9"),

  ]),
];
export default CreateUnit;
