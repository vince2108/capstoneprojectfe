import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  ShopOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import {
  Layout,
  Menu,
  Row,
  Col,
  Avatar,
  Button,
  Form,
  Input,
  Select,
  InputNumber,
  Table,message, Dropdown
} from "antd";
import { cloneDeep } from "lodash";
import React, { useState } from "react";
import { useEffect } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import TransferService from "../../service/TransferService";
import { showError, showSuccess } from "../../utils";
import "./TransferIssue.css";
import moment from 'moment'

function TransferIssue() {
  let newDate = new Date();
  let date = newDate.getDate();
  let month = newDate.getMonth() + 1;
  let year = newDate.getFullYear();
  const currentDate = date + "/" + month + "/" + year;
  let currentDateFormYYYYMMDD = year + "-" + month + "-" + date;
  const { id } = useParams()
  const navigate = useNavigate()

  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");

  const [detail, setDetail] = useState(null)
  const [isDone, setIsDone] = useState(false)
  const onClick = (e) => {
  console.log("click ", e);
  // setCurrent(e.key);
  if (e.key == 1) {
    navigate("/overview");
  }
  if(e.key ==2){
    navigate("/listTransfer");
  }
  if(e.key ==3 ){
    navigate("/inventoryWHstaff");
  }
  if(e.key==4){
    navigate("/listReceiptWHstaff");
  }
  if (e.key == 5) {
    navigate("/listIssueWHstaff");
  }
  if (e.key == 6) {
    navigate("/productGroups");
  }
  if (e.key == 7) {
    navigate("/unitGroup");
  }
  if (e.key == 8) {
    navigate("/unit");
  }
  
  if (e.key == 9) {
    navigate("/product");
  }
};

  const onChangeAmount = (row, value) => {
    const newData = cloneDeep(detail)
    const index = newData.productResponses.findIndex(item => item.productId === row.productId)
    if (index !== -1) {
      newData.productResponses[index].amountInput = value
      setDetail(newData)
    }
  };

  const onChangeNote = (row, value) => {
    const newData = cloneDeep(detail)
    const index = newData.productResponses.findIndex(item => item.productId === row.productId)
    if (index !== -1) {
      newData.productResponses[index].note = value
      setDetail(newData)
    }
  };

  const fetchDetail = async () => {
    let res;
    let statusDone = false
    const {data} = await TransferService.getDetailExport(id)
    res = data
    if (Number(data.data.statusID) === 2) {
      statusDone = true
      const { data: newData } = await TransferService.getDetailExportFull(id)
      res = newData;
    }
    res.data.productResponses = res.data.productResponses.map(item => ({
      ...item,
      amountInput: null,
      note: ''
    }))
    setDetail(res.data)
    setIsDone(statusDone)
  }

  const onSubmit = async () => {
    let hasError = false
    detail.productResponses.forEach(item => {
      console.log(item)
      if (!item.amountInput) hasError = true
    })
    if (hasError) {
      showError('Hãy nhập đủ các trường.')
      return;
    }
    try {
      const body = {
        idExport: id,
        exportDate: moment().format('YYYY-MM-DD'),
        personInChargeId: localStorage.getItem('idToken'),
        productExportRequests: detail.productResponses.map(({ productId, amountInput, note}) => ({
          idProduct: productId,
          amount: amountInput,
          note
        }))
      }
      const { data } = await TransferService.createTransferExport(body)
      if (data?.status && data.status === 'Error') {
        throw new Error;
      }
      showSuccess()
      navigate('/listTransfer')
       
    } catch (error) {
      console.log(error)
      showError('Create failed!')
    }
  }

  useEffect(() => {
    fetchDetail()
  }, [id])
  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };  

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];

  return (
    <Layout>
    <Sider trigger={null} collapsible collapsed={collapsed}>
      <div className="logo"> </div>
      <Menu
        theme={theme}
        onClick={onClick}
        defaultOpenKeys={["sub1"]}
        selectedKeys={["2"]}
        mode="inline"
        items={MenuItem}
      />
    </Sider>
    <Layout className="site-layout">
      <Header
        className="site-layout-background"
        style={{
          padding: 0,
        }}
      >
        <Row>
          <Col md={20}>
            {React.createElement(
              collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
              {
                className: "trigger",
                onClick: () => setCollapsed(!collapsed),
              }
            )}
          </Col>
          <Col md={4}>
            <Dropdown
              menu={{
                items,
                onClick: handleMenuClick,
              }}
              placement="bottomLeft"
            >
              <div>
                <Avatar
                  size="default"
                  style={{ marginRight: 10 }}
                  src={window.localStorage.getItem("avatar")}
                ></Avatar>
                {window.localStorage.getItem("usernameToken")}
              </div>
            </Dropdown>
          </Col>
        </Row>
      </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <div className="div-createIssue-headercontent">
            <Row>
              <Col md={1}></Col>
              <Col md={2}>
                <Button onClick={onSubmit}>Lưu</Button>
              </Col>
              <Col md={2}>
                <Link to='/listTransfer'>
                  <Button>Hủy Bỏ</Button>
                </Link>
              </Col>
            </Row>
          </div>
          <h1 style={{ marginLeft: "190px" }}>MÃ PHIẾU: {detail?.exportId}</h1>
          <div className="div-createIssue-bodycontent">
            <Form
              labelCol={{
                span: 5,
              }}
              wrapperCol={{
                span: 15,
              }}
            >
              <Form.Item label="Kho Nhập" style={{ marginBottom: 0 }}>
                <Row>
                  <Col md={12}>
                    <Form.Item
                      style={{
                        display: "inline-block",
                        width: "calc(100% - 8px)",
                        color: "#0648ff",
                      }}
                    >
                      {detail?.destinationStoreName}
                    </Form.Item>
                  </Col>
                  <Col md={12}>
                    <Form.Item
                      label="Người phụ trách"
                      style={{
                        display: "inline-block",
                        width: "calc(100% - 8px)",
                        color: "#0648ff",
                      }}
                    >
                      {isDone ? detail?.petitionerName : localStorage.getItem('usernameToken')}
                    </Form.Item>
                  </Col>
                </Row>
              </Form.Item>

              <Form.Item label="Kho xuất" style={{ marginBottom: 0 }}>
                <Row>
                  <Col md={12}>
                    <Form.Item
                      style={{
                        display: "inline-block",
                        width: "calc(100% - 8px)",
                        color: "#0648ff",
                      }}
                    >
                      {detail?.sourceStoreName}
                    </Form.Item>
                  </Col>
                  <Col md={12}>
                    <Form.Item
                      label="Ngày dự kiến"
                      style={{
                        display: "inline-block",
                        width: "calc(100% - 8px)",
                        color: "#0648ff",
                      }}
                    >
                      {detail?.expectedDate}
                    </Form.Item>
                  </Col>
                </Row>
              </Form.Item>

              <Form.Item label="Ngày Xuất kho" style={{ marginBottom: 0 }}>
                <Row>
                  <Col md={12}>
                    <Form.Item
                      style={{
                        display: "inline-block",
                        width: "calc(100% - 8px)",
                        color: "#0648ff",
                      }}
                    >
                      {currentDateFormYYYYMMDD}
                    </Form.Item>
                  </Col>
                </Row>
              </Form.Item>
            </Form>
            <div className="container">
              <Table
                className="transfer-table"
                columns={renderColumn(onChangeAmount, onChangeNote, isDone)}
                dataSource={detail?.productResponses}
                pagination={false}/>
            </div>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const MenuItem = [
  getItem("Kho Hàng ", "sub1", <ShopOutlined />, [
    getItem("Tổng quan", "1"),
    getItem("Điều chuyển", "2"),
    getItem("Kiểm kê", "3"),
    getItem("Nhập kho", "4"),
    getItem("Xuất kho", "5"),
    getItem("Nhóm sản phẩm", "6"),
    getItem("Nhóm đơn vị", "7"),
    getItem("Đơn vị", "8"),
    getItem("Sản phẩm", "9"),

  ]),
];
const renderColumn = (onChangeAmount, onChangeNote, isDone) => ([
  {
    title: 'Tên Sản Phẩm',
    dataIndex: 'productName',
    key: 'productName',
    width:' 30%',
  },
  {
    title: 'Nhu Cầu',
    dataIndex: 'numberOfRequests',
    key: 'numberOfRequests',
  },
  {
    title: isDone ? 'Đã xuất' : undefined,
    dataIndex: isDone ? 'amount' : undefined,
    key: isDone ? 'amount' : undefined,
  },
  {
    title: 'Số lượng xuất',
    dataIndex: 'amountInput',
    key: 'amountInput',
    render: (_, row) => <InputNumber disabled={isDone} min={0} max={row?.numberOfRequests} onChange={(val) => onChangeAmount(row, val)} className="input-transfer" />
  },
  {
    title: 'Note',
    dataIndex: 'note',
    key: 'note',
    render: (_, row) => <Input disabled={isDone} onChange={(e) => onChangeNote(row, e.target.value)} className="input-transfer" />
  },
])

export default TransferIssue;
