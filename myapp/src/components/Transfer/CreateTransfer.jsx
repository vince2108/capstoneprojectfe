import "./CreateTransfer.css";
import {
  MenuFoldOutlined,
  ShopOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  MinusCircleOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import {
  Layout,
  Menu,
  Row,
  Col,
  Avatar,
  Button,
  Input,
  Form,
  Select,
  Table,
  InputNumber,
  notification,
  message,
  Dropdown,
} from "antd";
import { DatePicker, Space } from "antd";
import React, { useEffect, useState } from "react";
import { cloneDeep } from "lodash";
import StoreHouse from "../../service/StoreHouse";
import ProductService from "../../service/ProductService";
import TransferService from "../../service/TransferService";
import { Link, useNavigate } from "react-router-dom";
import { showError, showSuccess } from "../../utils";
const dateFormatList = ["YYYY-MM-DD"];

function CreateTransfer() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");
  const navigate = useNavigate();
  
  const [indexRow, setIndexRow] = useState(0)
  const [listStore, setListStore] = useState([])
  const [listProduct, setListProduct] = useState([])
  const [dataSource, setDataSource] = useState([])
  const [receiveStore, setReceiveStore] = useState(null)
  const [transferStore, setTransferStore] = useState(null)
  const [dateTransfer, setDateTransfer] = useState()


  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/staff");
    }
    if (e.key == 2) {
      navigate("/clinic");
    }
    if (e.key == 3) {
      navigate("/report");
    }
    if (e.key == 4) {
      navigate("/service");
    }
    if (e.key == 5) {
      navigate("/divison");
    }
    if (e.key == 6) {
      navigate("/listReceiptManager");
    }
    if (e.key == 7) {
      navigate("/ListTransfer")
    }
    if (e.key == 8) {
      navigate("/inventoryManager")
    }
    if (e.key == 9) {
      // navigate("/inventoryManager")
    }
    if (e.key == 10) {
      navigate("/warehouse")
    }
  };

  const onChangeStore = (type, value) => {
      const newListStore = cloneDeep(listStore)
      if (type === 'receive') {
      const newIndex = newListStore.findIndex(item => item.value === value)
      if (newIndex !== -1) {
        if (receiveStore) {
          const oldIndex = receiveStore && newListStore.findIndex(item => item.value === receiveStore)
        
          oldIndex !== -1 && delete newListStore[oldIndex].disabled
        }
        newListStore[newIndex].disabled = true
      }
      setReceiveStore(value)
    } else {
      const newIndex = newListStore.findIndex(item => item.value === value)
      if (newIndex !== -1) {
        if (transferStore) {
          const oldIndex = transferStore && newListStore.findIndex(item => item.value === transferStore)

          oldIndex !== -1 && delete newListStore[oldIndex].disabled
        }
        newListStore[newIndex].disabled = true
      }
      setTransferStore(value)
    }
    setListStore(newListStore)
  };
  const onSearch = (value) => {
    console.log("search:", value);
  };

  const onChangeDate = (date, dateString) => {
    setDateTransfer(dateString)
  };

  const onChangeProduct = (row, value) => {
    const newData = cloneDeep(dataSource)
    const index = newData.findIndex(item => item.id === row.id)
    if (index !== -1) {
      newData[index].productId = value
      setDataSource(newData)
    }
  };

  const onChangeNumberRequest = (row, value) => {
    const newData = cloneDeep(dataSource)
    const index = newData.findIndex(item => item.id === row.id)
    if (index !== -1) {
      newData[index].numberOfRequest = value
      setDataSource(newData)
    }
  }

  const addNewRow = () => {
    const newData = cloneDeep(dataSource)
    newData.push({
      ...defaultRow,
      id: indexRow + 1
    })
    setDataSource(newData)
    setIndexRow(indexRow + 1)
  }

  const deleteRow = (row) => {
    const newData = cloneDeep(dataSource)
    const index = newData.findIndex(item => item.id === row.id)
    if (index !== -1) {
      newData.splice(index, 1)
      setDataSource(newData)
    }
  }

  const fetchProduct = async () => {
    const { data: { data, message }} = await ProductService.getAllProduct()
    if (message === "MSG_SUCCESS") {
      setListProduct(data.map(item => ({
        value: item.productId,
        label: item.productName,
        unit: item.unitName
      })))
    }
  }

  const fetchStore = async () => {
    const {data: { data, message} } = await StoreHouse.getAllStoreHouse()
    if (message === "MSG_SUCCESS") {
      setListStore(data.map(item => ({
        value: item.storeHouseId,
        label: item.storeHouseName
      })))
    }
  }

  const onSubmit = async () => {
    let hasError = false
    let errorDuplicate = false
    let productIds = []
    if (dataSource.length === 0) {
      showError('Hãy thêm ít nhất 1 sản phẩm để chuyển.')
      return;
    }
    dataSource.forEach(item => {
      if (!item.productId || !item.numberOfRequest) hasError = true
      if (productIds.includes(item.productId)) {
        errorDuplicate = true
      }
      productIds.push(item.productId)
    })
    if (!receiveStore || !transferStore || !dateTransfer) {
      hasError = true
    }
    if (hasError) {
      showError('Hãy nhập đủ các trường.')
      return;
    }
    if(receiveStore === transferStore) {
      showError('Kho nhận không được trùng với kho chuyển.')
      return;
    }
    if (errorDuplicate) {
      showError('Không được chọn cùng 1 sản phẩm trên nhiều dòng.')
      return;
    }
    try {
      const body = {
        receivingWareHouseId: receiveStore,
        wareHouseTransferId: transferStore,
        expectedDate: dateTransfer,
        petitionerId: localStorage.getItem('idToken'),
        productExRequests: dataSource.map(({ productId, numberOfRequest}) => ({
          productId,
          numberOfRequest
        }))
      }
      const { data } = await TransferService.createTransfer(body)
      if (data?.status && data.status === 'Error') {
        throw new Error;
      }
      showSuccess()
      navigate("/ListTransfer")
    } catch (error) {
      console.log(error)
      showError('Create failed!')
    }
  }

  useEffect(() => {
    fetchStore()
    fetchProduct()
  }, [])

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };  

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];
  

  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo"> </div>
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub2"]}
          selectedKeys={["7"]}
          mode="inline"
          items={MenuItem}
        />
      </Sider>
      <Layout className="site-layout">
      <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >
                <div>
                  <Avatar
                    size="default"
                    style={{ marginRight: 10 }}
                    src={window.localStorage.getItem("avatar")}
                  ></Avatar>
                  {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <div className="div1-ctf">
            <Space>
              <Button onClick={onSubmit} type="ghost">
                Lưu
              </Button>
              <Link to='/listTransfer'>
                <Button
                  type="ghost"
                >
                  Hủy bỏ
                </Button>
              </Link>
            </Space>
          </div>
          <div style={{ margin: "15px 0px 0px 0px" }}>
            <Form
              labelCol={{
                span: 5,
              }}
              wrapperCol={{
                span: 16,
              }}
              layout="horizontal"
            >
              <Form.Item label="Người yêu cầu">
                 <Input value={localStorage.getItem("usernameToken")} disabled />
              </Form.Item>
              <Form.Item label="Kho nhận">
                <Select
                  showSearch
                  placeholder="Select StoreWarehouse"
                  optionFilterProp="children"
                  value={receiveStore}
                  onChange={(value) => onChangeStore('receive', value)}
                  onSearch={onSearch}
                  filterOption={(input, option) =>
                    (option?.label ?? "")
                      .toLowerCase()
                      .includes(input.toLowerCase())
                  }
                  options={listStore}
                />
              </Form.Item>
              <Form.Item label="Kho chuyển">
                <Select
                  showSearch
                  placeholder="Select StoreWarehouse"
                  optionFilterProp="children"
                  value={transferStore}
                  onChange={(value) => onChangeStore('transfer', value)}
                  onSearch={onSearch}
                  filterOption={(input, option) =>
                    (option?.label ?? "")
                      .toLowerCase()
                      .includes(input.toLowerCase())
                  }
                  options={listStore}
                />
              </Form.Item>
              <Form.Item label="Ngày dự kiến">
                <Space direction="vertical" size={12}>
                  <DatePicker onChange={onChangeDate} format={dateFormatList} />
                </Space>
              </Form.Item>
            </Form>
          </div>
          <Button type="primary" onClick={addNewRow}>Thêm 1 dòng</Button>
          <div className="container">
            <Table
              className="transfer-table"
              columns={renderColumn(listProduct, deleteRow, onChangeProduct, onChangeNumberRequest)}
              dataSource={dataSource}
              pagination={false}/>
          </div>
         
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const MenuItem = [
  getItem("Quản lý", "sub1", <ShopOutlined />, [
    getItem("QL Nhân viên", "1"),
    getItem("QL Phòng khám", "2"),
    getItem("Báo cáo phòng khám", "3"),
    getItem("QL Dịch vụ", "4"),
    // getItem("QL Phòng ban", "5"),
  ]),
  getItem("Quản lý Kho Hàng ", "sub2", <ShopOutlined />, [
    getItem("Nhập kho", "6"),
    getItem("Điều chuyển", "7"),
    getItem("Kiểm kê", "8"),
    getItem("Báo cáo kho", "9"),
    getItem("Quản lý kho", "10"),
  ]),

];
const defaultRow = {
  productId: '',
  numberOfRequest: '',
}

const renderColumn = (listProduct, deleteRow, onChangeProduct, onChangeNumberRequest) => ([
  {
    title: '',
    dataIndex: 'delete',
    key: 'delete',
    width: '5%',
    render: (_, row) => <Button className="btn-del-trans" onClick={() => deleteRow(row)} type="primary" icon={<MinusCircleOutlined />} danger />
  },
  {
    title: 'Sản Phẩm',
    dataIndex: 'productId',
    key: 'productId',
    width:' 30%',
    render: (_, row) => (
      <Select
        showSearch
        placeholder="Select Product"
        style={{ width: '100%' }}
        optionFilterProp="children"
        onChange={(val) => onChangeProduct(row, val)}
        filterOption={(input, option) =>
          (option?.label ?? "")
            .toLowerCase()
            .includes(input.toLowerCase())
        }
        options={listProduct}
      />
    )
  },
  {
    title: 'Nhu Cầu',
    dataIndex: 'numberOfRequest',
    key: 'numberOfRequest',
    render: (_, row) => <InputNumber min={1} onChange={(val) => onChangeNumberRequest(row, val)} className="input-transfer" />
  },
  {
    title: 'Đơn vị',
    dataIndex: 'unit',
    key: 'unit',
    render: (_, record) => record.productId && listProduct.find(prod => prod.value === record.productId)?.unit
  },
])

export default CreateTransfer;