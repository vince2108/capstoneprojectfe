import "./ListTransfer.css";
import {
  MenuFoldOutlined,
  ShopOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { Layout, Menu, Row, Col, Avatar, Button, Select, Tabs, Table, Dropdown, message } from "antd";
import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import TransferService from "../../service/TransferService";
import { STATUSES } from "../Issue/ListIssueWHstaff";

const handleChange = (value) => {
  console.log(`selected ${value}`);
};

function ListTransfer() {
  const navigate = useNavigate()
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");

  const [listReceipt, setListReceipt] = useState([])
  const [listExport, setListExport] = useState([])
  const [status, setStatus] = useState('0')

  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/staff");
    }
    if (e.key == 2) {
      navigate("/clinic");
    }
    if (e.key == 3) {
      navigate("/report");
    }
    if (e.key == 4) {
      navigate("/service");
    }
    if (e.key == 5) {
      navigate("/divison");
    }
    if (e.key == 6) {
      navigate("/listReceiptManager");
    }
    if (e.key == 7) {
      navigate("/ListTransfer")
    }
    if (e.key == 8) {
      navigate("/inventoryManager")
    }
    if (e.key == 9) {
      // navigate("/inventoryManager")
    }
    if (e.key == 10) {
      navigate("/warehouse")
    }
  };

  const onChange = (value) => {
    setStatus(value)
  };

  const onSelectExportRow = (id) => {
    id && navigate(`/transferIssue/${id}`)
  }

  const onSelectReceiptRow = (id) => {
    id && navigate(`/transferReceipt/${id}`)
  }

  const fetchAllList = async() => {
    const { data: { data: dataReceipt } } = await TransferService.getListReceipt(status)
    const { data: { data: dataExport } } = await TransferService.getListExport(status)
    setListReceipt(dataReceipt)
    setListExport(dataExport)
  }

  useEffect(() => {
    fetchAllList()
  }, [status])

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };  

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];


  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo"> </div>
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub2"]}
          selectedKeys={["7"]}
          mode="inline"
          items={MenuItem}
        />
      </Sider>
      <Layout className="site-layout">
      <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >
                <div>
                  <Avatar
                    size="default"
                    style={{ marginRight: 10 }}
                    src={window.localStorage.getItem("avatar")}
                  ></Avatar>
                  {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <div
          className="div1-tf">
          <Row>
            <Col md={20}>
              <Link to='/createTransfer'>
                <Button className="btn-tf">Tạo</Button>
              </Link>
            </Col>

            <Col md={4}>
              <Select
                className="select-status-issue"
                placeholder="Chọn trạng thái"
                value={status}
                onChange={onChange}
                options={STATUSES}
              />
            </Col>
          </Row>
          </div>
          <div className="containzer">
            <Tabs defaultActiveKey="1">
              <Tabs.TabPane tab="Danh sách phiếu điều chuyển nhập" key="1">
                <Table
                  className="transfer-table"
                  columns={columnReceipt}
                  dataSource={listReceipt}
                  pagination={false}
                  onRow={(record) => {
                    return {
                      onClick: () => onSelectReceiptRow(record?.receiptId), // click row
                    };
                  }}
                   />
              </Tabs.TabPane>
              <Tabs.TabPane tab="Danh sách phiếu điều chuyển xuất" key="2">
                <Table
                className="transfer-table"
                columns={columneExport}
                dataSource={listExport}
                pagination={false}
                onRow={(record) => {
                  return {
                    onClick: () => onSelectExportRow(record?.exportId), // click row
                  };
                }}
                />
              </Tabs.TabPane>
            </Tabs>
          </div>
       
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const MenuItem = [
  getItem("Quản lý", "sub1", <ShopOutlined />, [
    getItem("QL Nhân viên", "1"),
    getItem("QL Phòng khám", "2"),
    getItem("Báo cáo phòng khám", "3"),
    getItem("QL Dịch vụ", "4"),
    // getItem("QL Phòng ban", "5"),
  ]),
  getItem("Quản lý Kho Hàng ", "sub2", <ShopOutlined />, [
    getItem("Nhập kho", "6"),
    getItem("Điều chuyển", "7"),
    getItem("Kiểm kê", "8"),
    // getItem("Báo cáo kho", "9"),
    getItem("Quản lý kho", "10"),
  ]),

];


const columnReceipt = [
  {
    key: 'receiptId',
    dataIndex: 'receiptId',
    title: 'Mã phiếu'
  },
  {
    key: 'destinationStoreName',
    dataIndex: 'destinationStoreName',
    title: 'Đích đến'
  },
  {
    key: 'sourceStoreName',
    dataIndex: 'sourceStoreName',
    title: 'Nguồn'
  },
  {
    key: 'expectedDate',
    dataIndex: 'expectedDate',
    title: 'Ngày dự kiến'
  },
  {
    key: 'statusName',
    dataIndex: 'statusName',
    title: 'Tình trạng'
  },
]

const columneExport = [
  {
    key: 'exportId',
    dataIndex: 'exportId',
    title: 'Mã phiếu'
  },
  {
    key: 'destinationStoreName',
    dataIndex: 'destinationStoreName',
    title: 'Đích đến'
  },
  {
    key: 'sourceStoreName',
    dataIndex: 'sourceStoreName',
    title: 'Nguồn'
  },
  {
    key: 'expectedDate',
    dataIndex: 'expectedDate',
    title: 'Ngày dự kiến'
  },
  {
    key: 'statusName',
    dataIndex: 'statusName',
    title: 'Tình trạng'
  },
]

export default ListTransfer;
