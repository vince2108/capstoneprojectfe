import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  ShopOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { Layout, Menu, Row, Col, Avatar, Button, Checkbox, message, Dropdown  } from "antd";
import React, { useState, useEffect } from "react";
import "./Service.css";
import { Input } from 'antd';
import {  useNavigate } from "react-router-dom";
import ServiceService from "../../service/ServiceService";

const { Search } = Input;

function Service() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");

  const [serviceData, setServiceData]= useState([])
  const navigate = useNavigate();
  const [searchValue, setSerchValue] = useState();

  useEffect(()=>{
    const getAllServiceData = async ()=>{
      const res = await ServiceService.getAllService()
      setServiceData(res.data.data)
    }
    getAllServiceData()
  }, [])

  const createNewService = () => {
    navigate("/createService");
  };

  const onClick = (e) => {
    console.log("click ", e);
    if (e.key == 1) {
      navigate("/staff");
    }
    if (e.key == 2) {
      navigate("/clinic");
    }
    if (e.key == 3) {
      navigate("/report");
    }
    if (e.key == 4) {
      navigate("/service");
    }
    if (e.key == 5) {
      navigate("/divison");
    }
    if (e.key == 6) {
      navigate("/listReceiptManager");
    }
    if (e.key == 7) {
      navigate("/ListTransfer")
    }
    if (e.key == 8) {
      navigate("/inventoryManager")
    }
    if (e.key == 9) {
      // navigate("/inventoryManager")
    }
    if (e.key == 10) {
      navigate("/warehouse")
    }
  };
  const onSearch = (value) => console.log(value);

  const onChange = (e) => {
    console.log(`checked = ${e.target.checked}`);
  };

  const searchServiceByNameFun = async () => {
    console.log(searchValue);
    const resS = await ServiceService.getServiceByName(
      searchValue
      
    );
    console.log(resS.data);
    setServiceData(resS.data.data)
  };

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];

  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo" />
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub1"]}
          selectedKeys={["4"]}
          mode="inline"
          items={MenuItem}
        />
      </Sider>
      <Layout className="site-layout">
      <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >
                <div>
                  <Avatar
                    size="default"
                    style={{ marginRight: 10 }}
                    src={window.localStorage.getItem("avatar")}
                  ></Avatar>
                  {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <div className="div-service-headercontent">
            <Row>
              <Col md={1}></Col>
              <Col md={2}>
                <Button onClick={createNewService}>Tạo</Button>
              </Col>
              <Col md={2}><Button>Xóa</Button></Col>
              <Col md={10}></Col>
              <Col md={8}>
              <div>
                <Input
                  placeholder="Tìm Dịch Vụ"
                  style={{ width: "75%" }}
                  value={searchValue}
                  onChange={(event) => setSerchValue(event.target.value)}
                ></Input>
                <Button onClick={() => searchServiceByNameFun()}>Tìm kiếm</Button>
              </div>
              </Col>
            </Row>
          </div>
          <div className="div-service-bodycontent">
          <table className="tab1-service">
                <thead>
                  <th></th>
                  <th>Tên dịch vụ</th>
                  <th>Phòng khám</th>
                  <th>Giá Tiền</th>
                </thead>
                <tbody>
                  {serviceData?.map((service) =>{
                    return(
                      <tr key={service.medicalServiceId}>
                      <td data-label="col-1"><Checkbox onChange={onChange}></Checkbox></td>
                      <td data-label="col-2">{service != null ? service.serviceName : ""}</td>
                      <td data-label="col-3">{service != null ? service.roomName : ""}</td>
                      <td data-label="col-4">{service != null ? service.price : ""}</td>
                    </tr>
                    )
                  })}
                </tbody>
              </table>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const MenuItem = [
  getItem("Quản lý", "sub1", <ShopOutlined />, [
    getItem("QL Nhân viên", "1"),
    getItem("QL Phòng khám", "2"),
    getItem("Báo cáo phòng khám", "3"),
    getItem("QL Dịch vụ", "4"),
    // getItem("QL Phòng ban", "5"),
  ]),
  getItem("Quản lý Kho Hàng ", "sub2", <ShopOutlined />, [
    getItem("Nhập kho", "6"),
    getItem("Điều chuyển", "7"),
    getItem("Kiểm kê", "8"),
    // getItem("Báo cáo kho", "9"),
    getItem("Quản lý kho", "10"),
  ]),

];


export default Service;
