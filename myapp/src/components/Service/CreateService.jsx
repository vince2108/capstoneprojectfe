import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  ShopOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import {
  Layout,
  Menu,
  Row,
  Col,
  Avatar,
  Button,
  Form,
  Select,
  Dropdown,
  message,
} from "antd";
import React, { useState, useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import "./CreateService.css";
import { Input } from "antd";
import ConsultingRoomService from "../../service/ConsultingRoomService";
import ServiceService from "../../service/ServiceService";

function CreateService() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");
  const [conslutingRoomS, setConslutingRoomSelect] = useState([]);
  const [conslutingRoomId, setConslutingRoomSelectId] = useState([]);
  const [serviceName, setServiceName] = useState([]);
  const [price, setPrice] = useState([]);
  const navigate = useNavigate();

  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/staff");
    }
    if (e.key == 2) {
      navigate("/clinic");
    }
    if (e.key == 3) {
      navigate("/report");
    }
    if (e.key == 4) {
      navigate("/service");
    }
    if (e.key == 5) {
      navigate("/divison");
    }
    if (e.key == 6) {
      navigate("/listReceiptManager");
    }
    if (e.key == 7) {
      navigate("/ListTransfer");
    }
    if (e.key == 8) {
      navigate("/inventoryManager");
    }
    if (e.key == 9) {
      // navigate("/inventoryManager")
    }
    if (e.key == 10) {
      navigate("/warehouse");
    }
  };
  const onChange = (value) => {
    console.log(`selected ${value}`);
  };
  const onSearch = (value) => {
    console.log("search:", value);
  };
  const handleConslutingRoomChange = (value) => {
    console.log(value);
    setConslutingRoomSelectId(value);
  };

  useEffect(() => {
    const getAllConsultingRoomSelect = async () => {
      const res3 = await ConsultingRoomService.getAllConsultingRoom(
        localStorage.getItem("branchId")
      );
      setConslutingRoomSelect(res3.data.data);
    };
    getAllConsultingRoomSelect();
  }, []);

  const SaveAction = async (values) => {
    console.log(values);
    const resData = await ServiceService.createNewService(
      values.serviceName,
      conslutingRoomId,
      values.price
    );
    console.log(serviceName);
    console.log(conslutingRoomId);
    console.log(price);
    navigate("/service");
  };
  const ReturnAction = () => {
    navigate(-1);
  };

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];

  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo" />
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub1"]}
          selectedKeys={["4"]}
          mode="inline"
          items={MenuItem}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >
                <div>
                  <Avatar
                    size="default"
                    style={{ marginRight: 10 }}
                    src={window.localStorage.getItem("avatar")}
                  ></Avatar>
                  {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          

          <div className="div-createservice-bodycontent">
            <Form
              onFinish={(values) => SaveAction(values)}
              labelCol={{
                span: 5,
              }}
              wrapperCol={{
                span: 15,
              }}
            >
            <div className="div-createservice-headercontent">
            <Row>
              <Col md={1}><Button type="primary" htmlType="submit">
                Lưu
              </Button>
              <Button onClick={ReturnAction}>Hủy</Button></Col>
            </Row>
          </div>
              

              <Form.Item
                label="Tên dịch vụ"
                name="serviceName"
                rules={[
                  {
                    required: true,
                    message: "Vui Lòng Điền Tên Dịch Vụ",
                  },
                ]}
                hasFeedback
              >
                <Input></Input>
              </Form.Item>
              <Form.Item
                label="Phòng Ban"
                name="consultingRoomId"
                rules={[
                  {
                    required: true,
                    message: "Vui Lòng Chọn Phòng Khám",
                  },
                ]}
                hasFeedback
              >
                <Select onChange={handleConslutingRoomChange}>
                  {conslutingRoomS.map((conslutingRoom) => {
                    return (
                      <Select.Option
                        value={conslutingRoom.consultingRoomId}
                        key={conslutingRoom.roomName}
                      >
                        {conslutingRoom.roomName}
                      </Select.Option>
                    );
                  })}
                </Select>
              </Form.Item>
              <Form.Item
                label="Giá tiền"
                name="price"
                rules={[
                  {
                    required: true,
                    message: "Vui Lòng Điền Giá Tiền",
                  },
                ]}
                hasFeedback
              >
                <Input></Input>
              </Form.Item>
            </Form>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const MenuItem = [
  getItem("Quản lý", "sub1", <ShopOutlined />, [
    getItem("QL Nhân viên", "1"),
    getItem("QL Phòng khám", "2"),
    getItem("Báo cáo phòng khám", "3"),
    getItem("QL Dịch vụ", "4"),
    // getItem("QL Phòng ban", "5"),
  ]),
  getItem("Quản lý Kho Hàng ", "sub2", <ShopOutlined />, [
    getItem("Nhập kho", "6"),
    getItem("Điều chuyển", "7"),
    getItem("Kiểm kê", "8"),
    // getItem("Báo cáo kho", "9"),
    getItem("Quản lý kho", "10"),
  ]),
];
export default CreateService;
