import "./Product.css";
import {
  MenuFoldOutlined,
  ShopOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { Layout, Menu, Row, Col, Avatar, message, Dropdown, Table } from "antd";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import ProductService from "../../service/ProductService";

function Product() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");
  const [allProduct,setAllProduct] = useState([]);
  const navigate = useNavigate(); 

  useEffect(() => {
    const setDataForPage = async () => {
      const res = await ProductService.getAllProDuctAndQuantity(1,localStorage.getItem("branchId"));
      setAllProduct(res.data.data)
    };
    setDataForPage();
  }, []);


  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/overview");
    }
    if(e.key ==2){
      navigate("/listTransfer");
    }
    if(e.key ==3 ){
      navigate("/inventoryWHstaff");
    }
    if(e.key==4){
      navigate("/listReceiptWHstaff");
    }
    if (e.key == 5) {
      navigate("/listIssueWHstaff");
    }
    if (e.key == 6) {
      navigate("/productGroups");
    }
    if (e.key == 7) {
      navigate("/unitGroup");
    }
    if (e.key == 8) {
      navigate("/unit");
    }
    if (e.key == 9) {
      navigate("/product");
    }
  };

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };  

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];


  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo"> </div>
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub1"]}
          selectedKeys={["9"]}
          mode="inline"
          items={MenuItem}
        />
      </Sider>
      <Layout className="site-layout">
      <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >
                <div>
                  <Avatar
                    size="default"
                    style={{ marginRight: 10 }}
                    src={window.localStorage.getItem("avatar")}
                  ></Avatar>
                  {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>

        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <div className="div1-p">
              <p className="title-product">Danh sách thuốc</p>
          </div>
          <div className="site-card-wrapper">
          <Table
                      className="transfer-table"
                      columns={productTable}
                      dataSource={allProduct}
                      pagination={false}
                      // onRow={(record) => {
                      //   return {
                      //     onClick: () => getDetailPatient(record?.accountId), // click row
                      //   };
                      // }}
                    />
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const MenuItem = [
  getItem("Kho Hàng ", "sub1", <ShopOutlined />, [
    getItem("Tổng quan", "1"),
    getItem("Điều chuyển", "2"),
    getItem("Kiểm kê", "3"),
    getItem("Nhập kho", "4"),
    getItem("Xuất kho", "5"),
    getItem("Nhóm sản phẩm", "6"),
    getItem("Nhóm đơn vị", "7"),
    getItem("Đơn vị", "8"),
    getItem("Sản phẩm", "9"),
  ]),
];

const productTable = [
  {
    key: "productId",
    dataIndex: "productId",
    title: "Mã Thuốc",
  },
  {
    key: "productName",
    dataIndex: "productName",
    title: "Tên Thuốc",
  },
  {
    key: "quantity",
    dataIndex: "quantity",
    title: "Số lượng trong kho",
  },

];


export default Product;
