import {
    MenuFoldOutlined,
    MenuUnfoldOutlined,
    UserOutlined,
    ShopOutlined,
  } from "@ant-design/icons";
  import {
    Layout,
    Menu,
    Row,
    Col,
    Avatar,
    Button,
    Form,
    Input,
    Image,
    Select,
  } from "antd";
  import React, { useState } from "react";
  import "./CreateEquipment.css";
  
  function CreateEquipment() {
    const { Header, Sider, Content } = Layout;
    const [collapsed, setCollapsed] = useState(false);
    const [theme] = useState("dark");
    const [current, setCurrent] = useState("1");
    const [nameProduct, setNameProduct] = useState([]);
    const onClick = (e) => {
      console.log("click ", e);
      setCurrent(e.key);
    };
  
    return (
      <Layout>
        <Sider trigger={null} collapsible collapsed={collapsed}>
          <div className="logo" />
          <Menu
            theme={theme}
            onClick={onClick}
            defaultOpenKeys={["sub1"]}
            selectedKeys={[current]}
            mode="inline"
            items={items}
          />
        </Sider>
        <Layout className="site-layout">
          <Header
            className="site-layout-background"
            style={{
              padding: 0,
            }}
          >
            <Row>
              <Col md={18}>
                {React.createElement(
                  collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                  {
                    className: "trigger",
                    onClick: () => setCollapsed(!collapsed),
                  }
                )}
              </Col>
              <Col md={2}></Col>
              <Col md={4}>
                <div>
                  <Avatar
                    size="default"
                    icon={<UserOutlined />}
                    style={{ marginRight: 10 }}
                  ></Avatar>
                  Nguyen Van A
                </div>
              </Col>
            </Row>
          </Header>
          <Content
            className="site-layout-background"
            style={{
              overflow: "scroll",
              margin: "24px 16px",
              padding: 24,
              minHeight: 280,
            }}
          >
            <div className="div1-cp">
              <Row>
                <Col md={1}>
                  <Button style={{ margin: "0px 0px 0px 10px" }}>Lưu</Button>
                </Col>
                <Col md={2}>
                  <Button style={{ margin: "0px 0px 0px 30px" }}>Hủy bỏ</Button>
                </Col>
              </Row>
            </div>
            <div style={{ margin: "20px 0px 0px 0px" }}>
              <Row>
                <Col md={18}></Col>
                <Col md={6}></Col>
              </Row>
            </div>
            <div>
              <Form
                labelCol={{
                  span: 6,
                }}
                wrapperCol={{
                  span: 18,
                }}
              >
                <Row>
                  <Col md={12}>
                    <Form.Item label="Tên Sản Phẩm">
                      <Input
                      value={nameProduct}
                      onChange={(event) => setNameProduct(event.target.value)}
                      rules={[
                        {
                          required: true,
                        },
                      ]} />
                    </Form.Item>
                    <Form.Item label="Nhóm Đơn Vị">
                      <Select
                        showSearch
                        placeholder="Search to Select"
                        optionFilterProp="children"
                        filterOption={(input, option) =>
                          (option?.label ?? "").includes(input)
                        }
                        filterSort={(optionA, optionB) =>
                          (optionA?.label ?? "")
                            .toLowerCase()
                            .localeCompare((optionB?.label ?? "").toLowerCase())
                        }
                        options={[
                          {
                            value: "1",
                            label: "kim tiêm",
                          },
                          {
                            value: "2",
                            label: "băng gạc",
                          },
                          {
                            value: "3",
                            label: "tai nghe y tế",
                          },
                        ]}
                      />
                    </Form.Item>
                    <Form.Item label="Đơn vị">
                      <Select
                        showSearch
                        placeholder="Search to Select"
                        optionFilterProp="children"
                        filterOption={(input, option) =>
                          (option?.label ?? "").includes(input)
                        }
                        filterSort={(optionA, optionB) =>
                          (optionA?.label ?? "")
                            .toLowerCase()
                            .localeCompare((optionB?.label ?? "").toLowerCase())
                        }
                        options={[
                          {
                            value: "1",
                            label: "vỉ",
                          },
                          {
                            value: "2",
                            label: "viên",
                          },
                          {
                            value: "3",
                            label: "hộp",
                          },
                        ]}
                      />
                    </Form.Item>
                    <Form.Item label="Kiểu Vật Tư">
                    {/* {allMedicineType?.map((medicinetype) => {
                          return (
                            <Fragment>
                              <Checkbox
                                key={medicinetype.medicineTypeId}
                                onChange={(event) =>
                                  setMedicineTypeCheckAction(
                                    event,
                                    medicinetype.medicineTypeId
                                  )
                                }
                              >
                                {medicinetype.medicineTypeName}
                              </Checkbox>
                              <br></br>
                            </Fragment>
                          );
                        })} */}
                  </Form.Item>                 
                  </Col>
                </Row>
              </Form>
            </div>
          </Content>
        </Layout>
      </Layout>
    );
  }
  function getItem(label, key, icon, children, type) {
    return {
      key,
      icon,
      children,
      label,
      type,
    };
  }
  const items = [
    getItem("Kho Hàng ", "kh", <ShopOutlined />, [
      getItem("Tổng quan", "1"),
      getItem("Điều chuyển", "2"),
      getItem("Kiểm kê", "3"),
      getItem("Quản lý kho", "4"),
      getItem("Nhóm sản phẩm", "5"),
      getItem("Nhóm đơn vị", "6"),
      getItem("Đơn vị", "7"),
      getItem("Nhập kho", "8"),
    ]),
  ];
  
  export default CreateEquipment;
  