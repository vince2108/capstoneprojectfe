import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  ShopOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import {
  Layout,
  Menu,
  Row,
  Col,
  Avatar,
  Button,
  Form,
  Input,
  Image,
  DatePicker,
  Select,
  Checkbox,
  Modal,
  message, Dropdown,
} from "antd";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import MedicineService from "../../service/MedicineService";
import ProductService from "../../service/ProductService";
import UnitService from "../../service/UnitService";
import ProductGroups from "../ProductGroups/ProductGroups";
import "./CreateProduct.css";

function CreateProduct() {
  const { TextArea } = Input;
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");
  const [allUnit, setAllUnit] = useState([]);
  const [allProductGroup, setAllProductGroup] = useState([]);
  const [selecteddUnit,setSelectedUnit] = useState();
  const [selectedProductGroupList,setSelectedProductGroupList] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    const setDataAllUnitGroup = async () => {
      const res = await UnitService.getAllUnit();
      const res2 = await ProductService.getAllProductGroup();
      setAllUnit(res.data.data);
      setAllProductGroup(res2.data.data);
      console.log(res2.data.data);
      console.log(res.data.data);
    };
    setDataAllUnitGroup();
  }, []);

  const setProductGroupCheckedId =(event,productGroupsId)=>{
    if(event.target.checked){
      setSelectedProductGroupList([...selectedProductGroupList, productGroupsId])
    }
    if(!event.target.checked){
      const id =Object.values(selectedProductGroupList).filter(id=>id!==productGroupsId)
      console.log(id)
      setSelectedProductGroupList(id)
    }
  }

  const SaveAction = async (values) => {
    if(Object.keys(selectedProductGroupList).length ===0 || Object.keys(selecteddUnit) ===0 ){
      Modal.warning({
        title: 'Cảnh báo!',
        content: 'Vui lòng điền đủ thông tin',
      });
    }else{
      console.log(values)
      var Note = values.Note;
      var UserObject = values.UserObject;
      var Using = values.Using;
      if(values.Note === undefined){
        Note = null
      }
      if(values.UserObject === undefined){
        UserObject = null
      }
      if(values.Using === undefined){
        Using = null
      }
      console.log(Note)
      console.log(UserObject)
      console.log(Using)
      const res = await ProductService.createProduct(values.productName,selecteddUnit,null,Note,UserObject,Using,selectedProductGroupList)
      navigate('/createReceiptManager',{replace: true})
    }
  };

  const ReturnAction = () => {
    navigate(-1);
  };

  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/staff");
    }
    if (e.key == 2) {
      navigate("/clinic");
    }
    if (e.key == 3) {
      navigate("/report");
    }
    if (e.key == 4) {
      navigate("/service");
    }
    if (e.key == 5) {
      navigate("/divison");
    }
    if (e.key == 6) {
      navigate("/listReceiptManager");
    }
    if (e.key == 7) {
      navigate("/ListTransfer")
    }
    if (e.key == 8) {
      navigate("/inventoryManager")
    }
    if (e.key == 9) {
      // navigate("/inventoryManager")
    }
    if (e.key == 10) {
      navigate("/warehouse")
    }
  };

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };  

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];

  return (
    <Layout>
    <Sider trigger={null} collapsible collapsed={collapsed}>
      <div className="logo"> </div>
      <Menu
        theme={theme}
        onClick={onClick}
        defaultOpenKeys={["sub2"]}
        selectedKeys={["8"]}
        mode="inline"
        items={MenuItem}
      />
    </Sider>
    <Layout className="site-layout">
    <Header
        className="site-layout-background"
        style={{
          padding: 0,
        }}
      >
        <Row>
          <Col md={20}>
            {React.createElement(
              collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
              {
                className: "trigger",
                onClick: () => setCollapsed(!collapsed),
              }
            )}
          </Col>
          <Col md={4}>
            <Dropdown
              menu={{
                items,
                onClick: handleMenuClick,
              }}
              placement="bottomLeft"
            >
              <div>
                <Avatar
                  size="default"
                  style={{ marginRight: 10 }}
                  src={window.localStorage.getItem("avatar")}
                ></Avatar>
                {window.localStorage.getItem("usernameToken")}
              </div>
            </Dropdown>
          </Col>
        </Row>
      </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <div style={{ margin: "20px 0px 0px 0px" }}>
            <Row>
              <Col md={18}></Col>
              <Col md={6}></Col>
            </Row>
          </div>
          <div>
            <Form
              onFinish={(values) => SaveAction(values)}
              labelCol={{
                span: 6,
              }}
              wrapperCol={{
                span: 15,
              }}
            >
              <Form.Item>
                <Row>
                  <Col md={1}>
                    <Button
                      style={{ margin: "0px 0px 0px 10px" }}
                      type="primary"
                      htmlType="submit"
                    >
                      Lưu
                    </Button>
                  </Col>
                  <Col md={6}>
                    <Button
                      style={{ margin: "0px 0px 0px 40px" }}
                      onClick={ReturnAction}
                    >
                      Hủy bỏ
                    </Button>
                  </Col>
                </Row>
                <div className="div1-cp"></div>
                <div style={{ margin: "20px 0px 0px 0px" }}>
                  <Row>
                    <Col md={18}></Col>
                    <Col md={6}></Col>
                  </Row>
                </div>
              </Form.Item>
              <Form.Item
                label="Tên Sản Phẩm"
                name="productName"
                rules={[
                  {
                    required: true,
                    whitespace: true,
                    message: "Vui lòng điền tên sản phẩm!",
                  },
                ]}
              >
                <Input />
              </Form.Item>
              <Form.Item label="Đơn vị">
                <Select
                  showSearch
                  placeholder="Chọn đơn vị"
                  optionFilterProp="children"
                  onChange={(value) => setSelectedUnit(value)}
                  filterOption={(input, option) =>
                    option.props.children
                      .toLowerCase()
                      .includes(input.toLowerCase()) ||
                    option.props.value
                      .toLowerCase()
                      .includes(input.toLowerCase())
                  }
                >
                  {allUnit?.map((unit) => (
                    <Select.Option key={unit.unitId}>
                      {unit.unitName}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item
                label="Nhóm sản phẩm"
              >
                {allProductGroup?.map((productGroup) => {
                  return (
                    <Checkbox
                      key={productGroup.productGroupId}
                      onChange={(event)=>setProductGroupCheckedId(event,productGroup.productGroupId)
                      }
                    >
                      {productGroup.productGroupName}
                    </Checkbox>
                  );
                })}
                <br></br>
              </Form.Item>
              <Form.Item name="Using" label="Cách dùng">
                <TextArea />
              </Form.Item>
              <Form.Item name="UserObject" label="Hạn chế">
                <TextArea />
              </Form.Item>
              <Form.Item name="Note" label="Ghi Chú">
                <TextArea />
              </Form.Item>
            </Form>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}

const MenuItem = [
  getItem("Quản lý", "sub1", <ShopOutlined />, [
    getItem("QL Nhân viên", "1"),
    getItem("QL Phòng khám", "2"),
    getItem("Báo cáo phòng khám", "3"),
    getItem("QL Dịch vụ", "4"),
    getItem("QL Phòng ban", "5"),
  ]),
  getItem("Quản lý Kho Hàng ", "sub2", <ShopOutlined />, [
    getItem("Nhập kho", "6"),
    getItem("Điều chuyển", "7"),
    getItem("Kiểm kê", "8"),
    getItem("Báo cáo kho", "9"),
    getItem("Quản lý kho", "10"),
  ]),

];


export default CreateProduct;
