import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  LogoutOutlined,
  EditOutlined,
} from "@ant-design/icons";
import {
  Layout,
  Menu,
  Row,
  Col,
  Avatar,
  Select,
  Button,
  Tag,
  Table,
  Input,
  message,
  Dropdown,
} from "antd";
import React, { useState, useEffect, useRef } from "react";
import "./CheckUp.css";
import { AppstoreOutlined, ReloadOutlined } from "@ant-design/icons";
import { Link, useNavigate } from "react-router-dom";
import { VariableSizeGrid as Grid } from "react-window";
import ResizeObserver from "rc-resize-observer";
import classNames from "classnames";
import ConsultingRoomService from "../../service/ConsultingRoomService";
import DoctorService from "../../service/DoctorService";
import PatientService from "../../service/PatientService";
import { Fragment } from "react";

function CheckUp() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");
  const [currentConsultingRoomState, setCurrentConsultingRoomState] = useState([]);
  const [allPatientByConsultingRoom, setAllPatientByConsultingRoom] = useState([]);
  const [patientInforState, setPatientInforState] = useState([]);
  const [patientMedicalRecord, setPatientMedicalRecord] = useState([]);
  const [selectId, setSelectId] = useState();
  const [stt, setStt] = useState();
  const [loading, setLoading] = useState(false);
  const [patientStatus, setPatientStatus] = useState([]);

  const navigate = useNavigate();

  useEffect(() => {
    const getCurrentConsutingRoomFun = async () => {
      const res = await ConsultingRoomService.getConsultingRoomById(
        localStorage.getItem("consultingRoom")
      );
      setCurrentConsultingRoomState(res.data.data);
      console.log(res.data.data);
    };
    getCurrentConsutingRoomFun();
  }, []);

  useEffect(() => {
    const getDataPatientStatusIsWaiting = async () => {
      const res = await DoctorService.getPatientWithStatus(
        localStorage.getItem("consultingRoom"),
        localStorage.getItem("branchId")
      );
      setPatientStatus(res.data.data);
      console.log(res.data.data);
    };
    getDataPatientStatusIsWaiting();
  }, []);


  useEffect(() => {
    const getAllPatientFuntionFun = async () => {
      console.log(localStorage.getItem("consultingRoom"))
      console.log(localStorage.getItem("branchId"))
      const res = await DoctorService.getPatientByConsultingRoom(
        localStorage.getItem("consultingRoom"),
        localStorage.getItem("branchId")

      );
      setAllPatientByConsultingRoom(res.data.data);
      setLoading(true)
      console.log(res.data.data);
    };
    getAllPatientFuntionFun();
  }, [loading]);

  const getPatientInformationFun = async (accountId,stt) => {
    document.getElementById("patient-infor").style.display = "block";
    console.log(accountId);
    const res = await PatientService.getPatientById(accountId);
    const res2 = await DoctorService.getPatientMedicalRecordById(accountId)
    setPatientInforState(res.data.data);
    setPatientMedicalRecord(res2.data.data)
    setSelectId(accountId)
    setStt(stt)
    console.log(res.data.data); 
    console.log(res2.data.data);
  };

  const getCheckUpDetailFun = () => {
    const res = DoctorService.updateStatusExamnination(
      selectId,
      localStorage.getItem("consultingRoom")
    );
    navigate("/detailCheckUp", {
      state: { selectId: selectId,stt: stt},
    });
  };

  const getMedicalRecordDetailFun = () => {
    navigate("/detailMedicalRecord", {
      state: { selectId: selectId, canEdit : false },
    });
  };

  const changeSTTAction = (patientId, stt) => {
    const res = DoctorService.updateIndexPatient(
      patientId,
      localStorage.getItem("consultingRoom"),
      stt
    );
    setLoading();
  };

  
  const { TextArea } = Input;

  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/checkUp");
    }
    if (e.key == 2) {
      navigate("/medicalRecord");
    }
    if (e.key == 3) {
      navigate("/listIssueDoctor");
    }

  };

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };
  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];


  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo" />
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub1"]}
          selectedKeys={[current]}
          mode="inline"
          items={Menuitems}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >  
              <div>
                <Avatar
                  size="default"
                  style={{ marginRight: 10 }}
                  src={window.localStorage.getItem("avatar")}
                ></Avatar>
                {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <div>
            <h2>Danh Sách Khám</h2>
            <label>Phòng Khám:</label>
            <Input
              style={{
                width: "200px",
                marginLeft: "30px",
              }}
              value={currentConsultingRoomState.roomName}
            />
          </div>
          <Row style={{ margin: "20px 0px 0px 0px" }}>
            <Col md={14} className="checkup-tab-kham">
              <div>
                <table className="tab1">
                  <thead>
                    <th>Số TT</th>
                    <th>Tên BN</th>                   
                    <th>Giới Tính</th>
                    <th>Trạng thái</th>
                    <th></th>
                  </thead>
                  <tbody>
                  {allPatientByConsultingRoom === null ? (
                      <Fragment>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </Fragment>
                      ) : (
                        allPatientByConsultingRoom.map((patient) => {
                          return (
                            <tr
                              onClick={() =>
                                getPatientInformationFun(
                                  patient.accountId,
                                  patient.stt
                                )
                              }
                            >
                              <td data-label="col-1">{patient.stt}</td>
                              <td data-label="col-3">{patient.fullName}</td>
                              <td data-label="col-5">{patient.sex}</td>
                              <td
                                data-label="col-5"
                                style={{
                                  font: "italic",
                                }}
                              >
                              {patient.statusName}...
                            </td>
                            <td>
                              <Button
                                onClick={() =>
                                  changeSTTAction(
                                    patient.accountId,
                                    patient.stt
                                  )
                                }
                              >
                                Tiếp Theo
                              </Button>
                            </td>
                          </tr>
                        );
                      })
                    )}
                  </tbody>
                </table>
                <br></br>
                <br></br>
                <h1>DANH SÁCH CHỜ LẤY KẾT QUẢ</h1>
                <table className="tab1">
                  { patientStatus !==null &&
                      <thead>
                        <th>Số TT</th>
                        <th>Tên BN</th>
                        <th>Giới Tính</th>
                        <th>Trạng thái</th>
                      </thead>
                  }
                  {patientStatus?.map((patient,index) => {
                    return (
                      <tr>
                        <td data-label="col-1">{index+1}</td>
                        <td data-label="col-3">{patient.fullName}</td>
                        <td data-label="col-5">{patient.sex}</td>
                        <td
                          data-label="col-5"
                          style={{
                            font: "italic",
                          }}
                        >
                          {patient.statusName}...
                        </td>
                        
                      </tr>
                    );
                  })}
                </table>
              </div>
            </Col>
            <Col
              id="patient-infor"
              md={10}
              style={{
                display: "none",
              }}
            >
              <div style={{ margin: "0px 0px 0px 10px" }}>
                <h1>Thông tin bệnh Nhân</h1>
                <div>
                  <label>Mã Bn: {patientInforState.accountId}</label>{" "}
                  &emsp;&emsp;&emsp;&emsp;&emsp;
                  <label style={{ marginLeft: "80px" }}>HSBA:</label>{" "}
                  <Button onClick={() => getMedicalRecordDetailFun()}>
                    <UserOutlined />
                  </Button>
                  <br></br>
                  <label>Họ Tên: {patientInforState.fullName}</label>
                  <br></br>
                  <label>Giới Tính: {patientInforState.sex}</label>
                  <br></br>
                  <label>Ngày sinh: {patientInforState.dob}</label>
                  <br></br>
                  <label>
                    Địa chỉ:{" "}
                    {patientInforState.address +
                      ", " +
                      patientInforState.village +
                      ", " +
                      patientInforState.district +
                      ", " +
                      patientInforState.province}
                  </label>
                </div>
                <br></br>
                <br></br>
                <label>Tiền Sử Bệnh:</label>
                <br></br>
                <TextArea
                readOnly={true}
                  rows={3}
                  style={{ width: "600px" }}
                  value={
                    patientMedicalRecord.pastMedicalHistory === null
                      ? " "
                      : patientMedicalRecord.pastMedicalHistory
                  }
                ></TextArea>
                <br></br>
                <br></br>
                <label>Dị Ứng:</label>
                <br></br>
                <TextArea
                 readOnly={true}
                  rows={3}
                  style={{ width: "600px" }}
                  value={
                    patientMedicalRecord.allergies === null
                      ? " "
                      : patientMedicalRecord.allergies
                  }
                ></TextArea>
                <br></br>
                <br></br>
                <br></br>
                <Button onClick={() => getCheckUpDetailFun()}>Khám Bệnh</Button>
              </div>
            </Col>
          </Row>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const Menuitems = [
  getItem("Khám Bệnh", "sub1", <AppstoreOutlined />, [
    getItem("Danh sách chờ", "1"),
  ]),
  getItem("Hồ sơ bệnh nhân", "sub2", <AppstoreOutlined />, [
    getItem(" Hồ Sơ bệnh  nhân", "2"),
  ]),
  getItem("Yêu cầu xuất thuốc", "sub3", <AppstoreOutlined />, [
    getItem(" Yêu cầu xuất thuốc", "3"),
  ]),

];

export default CheckUp;
