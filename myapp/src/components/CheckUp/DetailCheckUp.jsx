import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  LogoutOutlined,
  EditOutlined
} from "@ant-design/icons";
import {
  Layout,
  Menu,
  Row,
  Col,
  Avatar,
  Dropdown,
  Input,
  message,
  Button,
} from "antd";
import React, { useState, useEffect, useRef } from "react";
import "./DetailCheckUp.css";
import { AppstoreOutlined } from "@ant-design/icons";
import { Form, Link, useLocation, useNavigate } from "react-router-dom";
import { Checkbox } from "antd";
import { VariableSizeGrid as Grid } from "react-window";
import ResizeObserver from "rc-resize-observer";
import classNames from "classnames";
import ConsultingRoomService from "../../service/ConsultingRoomService";
import PaymentService from "../../service/PaymentService";
import DoctorService from "../../service/DoctorService";

function DetailCheckUp() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");
  const { TextArea } = Input;
  const [allConsultingRoomState, getAllConsultingRoomState] = useState([]);
  const [serviceFollowRoom, setServiceFollowRoom] = useState([]);
  const [newService, setNewService] = useState([]);
  const [patientMedicalRecord, setPatientMedicalRecord] = useState([]);
  const [noteMedicalRecord, setNoteMedicalRecord] = useState();
  const [loading, setLoading] = useState(false);
  const [consultingRoomNameGoTo,setConsultingRoomNameGoTo] = useState();
  const location = useLocation();
  const navigate = useNavigate();
  let newDate = new Date();
  let date = newDate.getDate();
  let month = newDate.getMonth() + 1;
  let year = newDate.getFullYear();
  const currentDate = date + "/" + month + "/" + year;
  let currentDateFormYYYYMMDD = year + "-" + month + "-" + date;

  useEffect(()=>{
    const getMedicalRecord = async () =>{
      const res = await DoctorService.getPatientMedicalRecordById(location.state.selectId);
      setPatientMedicalRecord(res.data.data)
      setNoteMedicalRecord(res.data.data.note)
    }
    getMedicalRecord();
  },[])

  useEffect(() => {
    const getAllConsultingRoomFun = async () => {
      const res = await ConsultingRoomService.getAllConsultingRoomAndService(location.state.selectId, localStorage.getItem("branchId"));
      console.log(res.data.data);
      getAllConsultingRoomState(res.data.data);
    };
    getAllConsultingRoomFun();
  }, []);

  useEffect(() => {
    const getServiceOfPatient = async () => {
      const res = await PaymentService.getPatientUseService(
        location.state.selectId
      );
      console.log(res.data.data);
      setServiceFollowRoom(res.data.data);
    };
    getServiceOfPatient();
  }, []);

  const getNewConsultingRoomService = (
    serviceId,
    serviceName,
    servicePrice,
    consultingRoomName,
    consultingRoomId
  ) => {
    const newObjectService = {
      serviceId: serviceId,
      serviceName: serviceName,
      servicePrice: servicePrice,
      consultingRoomName: consultingRoomName,
      consultingRoomId: consultingRoomId,
    };
    setNewService([...newService, newObjectService]);
    const newRoomState2 = allConsultingRoomState.map((consultingRoom) => {
      return consultingRoom.medicalServices.filter(
        (medicalServiceId) => medicalServiceId.medicalServiceId !== serviceId
      );
    });
    console.log(newRoomState2);
    console.log(serviceId);
    document.getElementById(serviceId).style.display = "none";
  };

  const deleteNewConsultingRoomService = (serviceId) => {
    const id = Object.values(newService).filter(
      (service) => service.serviceId !== serviceId
    );
    document.getElementById(serviceId).style.display = "block";
    console.log(id);
    console.log(serviceId);
    setNewService(id);
  };

  const SaveNewConsultingRoomService = (serviceId,consultingRoomId,consultingRoomName) =>{
    const res = DoctorService.addService(location.state.selectId,localStorage.getItem("consultingRoom"),noteMedicalRecord,location.state.stt,consultingRoomId,serviceId)
    setConsultingRoomNameGoTo("Bệnh nhân "+patientMedicalRecord.fullName+ " đến phòng thanh toán để được điều chuyển đến phòng "+consultingRoomName)
    document.getElementById('this-dialog').showModal();
  }

  const finishFun=()=>{
    const res = DoctorService.finishService(location.state.selectId,localStorage.getItem("consultingRoom"),noteMedicalRecord)
    navigate('/checkUp')
  }

  const BackToCheckUp = () =>{
    navigate("/checkUp", { replace: true });
  }
  
  const onChange = (e) => {
    console.log(`checked = ${e.target.checked}`);
  };

  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/checkUp");
    }
    if (e.key == 2) {
      navigate("/medicalRecord");
    }
    if (e.key == 3) {
      navigate("/listIssueDoctor");
    }
  };

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };
  const handleMenuClick = (e) => {
    if (e.key === "1") {
      console.log("click", e.key);
    } else if (e.key === "2") {
      logOutAction();
    }

  };
  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];

  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo" />
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub1"]}
          selectedKeys={[current]}
          mode="inline"
          items={Menuitems}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
            <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >  
                <div>
                <Avatar
                  size="default"
                  style={{ marginRight: 10 }}
                  src={window.localStorage.getItem("avatar")}
                ></Avatar>
               {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content  
          className="site-layout-background"
          style={{
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
            overflow: "scroll",
          }}
        >
          <dialog id="this-dialog">
            <p> {consultingRoomNameGoTo}</p>
            <button onClick={() => BackToCheckUp()}>Đóng</button>
          </dialog>
          <div className="detailcheckup-headercontent">
            <Row>
              <Col md={2}></Col>
              <Col md={12}>
                Bác Sĩ Chỉ Định :{" "}
                <Input
                  style={{
                    width: "50vh",
                  }}
                  value={localStorage.getItem("usernameToken")}
                />
              </Col>
              <Col md={6}>
                <label
                  style={{
                    marginLeft: "30px",
                  }}
                >
                  Ngày Hiện Tại:
                </label>
                <Input
                  placeholder="Ngày Hiện tại"
                  style={{
                    width: "120px",
                    marginLeft: "10px",
                  }}
                  value={currentDate}
                ></Input>
              </Col>
            </Row>
          </div>
          <div className="detailcheckup-bodycontent">
            <Row>
              <Col md={1}></Col>
              <Col md={11} className="detailcheckup-bodyleft">
                Chuẩn Đoán:
                <br></br>
                <TextArea
                  rows={3}
                  value ={noteMedicalRecord == null ? ' ' : noteMedicalRecord}
                  onChange={(event) =>
                      setNoteMedicalRecord(event.target.value)
                    }
                  style={{ width: "calc(100% - 50px)", marginLeft: "20px" }
                  
                  }
                />
                <br></br>
                Danh Sách Dịch Vụ:
                <Menu mode="inline">
                  {allConsultingRoomState === null
                    ? null
                    : allConsultingRoomState.map((consultingRoom) => {
                        return (
                          <Menu.SubMenu
                            title={consultingRoom.consultingRoom.roomName}
                            // onTitleClick={()=> getServiceInConsultingRoom()}
                          >
                            {consultingRoom.medicalServices === null
                              ? null
                              : consultingRoom.medicalServices.map(
                                  (service) => {
                                    return (
                                      <Menu.Item
                                        id={service.medicalServiceId}
                                        onClick={() =>
                                          getNewConsultingRoomService(
                                            service.medicalServiceId,
                                            service.serviceName,
                                            service.price,
                                            consultingRoom.consultingRoom
                                              .roomName,
                                            consultingRoom.consultingRoom
                                              .consultingRoomId
                                          )
                                        }
                                      >
                                        {service.serviceName}
                                      </Menu.Item>
                                    );
                                  }
                                )}
                          </Menu.SubMenu>
                        );
                      })}
                </Menu>
              </Col>
              <Col md={11} className="detailcheckup-bodyright">
                <div>
                  <h1>Thông tin bệnh Nhân</h1>
                  <div>
                    <label>Họ Tên: {patientMedicalRecord.fullName}</label>
                    <br></br>
                    <label>Giới Tính: {patientMedicalRecord.sex}</label>
                    <br></br>
                    <label>Chiều cao: {patientMedicalRecord.height===null? 'Chưa có thông tin' : patientMedicalRecord.height}</label>
                    <br></br>
                    <label>Cân năng: {patientMedicalRecord.weight===null ? 'Chưa có thông tin' : patientMedicalRecord.weight}</label>
                    <br></br>
                    <label>Huyết áp: {patientMedicalRecord.bloodPressure ===null ? 'Chưa có thông tin' : patientMedicalRecord.bloodPressure}</label>
                    <br></br>
                    <label>Mạch: {patientMedicalRecord.circuit === null ? 'Chưa có thông tin' : patientMedicalRecord.circuit}</label>
                    <br></br>
                  </div>
                  <br></br>
                  <label>Tiền Sử Bệnh:</label>
                  <br></br>
                  <TextArea rows={3} style={{ width: "600px" }} value={patientMedicalRecord.pastMedicalHistory ===null ? "Chưa có thông tin" : patientMedicalRecord.pastMedicalHistory}></TextArea>
                  <br></br>
                  <br></br>
                  <label>Dị Ứng: </label>
                  <br></br>
                  <TextArea rows={3} style={{ width: "600px" }} value={patientMedicalRecord.allergies ===null ? "Chưa có thông tin" : patientMedicalRecord.allergies}></TextArea>
                </div>
              </Col>
            </Row>
          </div>

          <div class="Detailcheckip-tab1">
            <table className="tab1">
              <thead>
                <th>Tên dịch vụ</th>
                <th>Thành Tiền</th>
                <th>Nơi Thực Hiện</th>
                <th></th>
              </thead>
              <tbody>
                {serviceFollowRoom === null
                  ? null
                  : serviceFollowRoom.map((service) => {
                      return (
                        <tr>
                          <td data-label="col-1">{service.serviceName}</td>
                          <td data-label="col-2">{service.totalCost}</td>
                          <td data-label="col-3">{service.roomName}</td>
                          <td data-label="col-4"></td>
                        </tr>
                      );
                    })}
                {newService === null
                  ? null
                  : newService.map((service) => {
                      return (
                        <tr>
                          <td data-label="col-1">{service.serviceName}</td>
                          <td data-label="col-2">{service.servicePrice}</td>
                          <td data-label="col-3">
                            {service.consultingRoomName}
                          </td>
                          <td>
                            <Button
                              onClick={() =>
                                SaveNewConsultingRoomService(
                                  service.serviceId,service.consultingRoomId,service.consultingRoomName
                                )
                              }>Lưu</Button>&ensp;
                            <Button
                              onClick={() =>
                                deleteNewConsultingRoomService(
                                  service.serviceId
                                )
                              }
                            >
                              Xóa
                            </Button>
                          </td>
                        </tr>
                      );
                    })}
              </tbody>
            </table>
          </div>
          <Button className="detaicheckup-btn-kt" onClick={()=>finishFun()}>Kết thúc</Button>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const Menuitems = [
  getItem("Khám Bệnh", "sub1", <AppstoreOutlined />, [
    getItem("Danh sách chờ", "1"),
  ]),
  getItem("Hồ sơ bệnh nhân", "sub2", <AppstoreOutlined />, [
    getItem(" Hồ Sơ bệnh  nhân", "2"),
  ]),
  getItem("Yêu cầu xuất thuốc", "sub3", <AppstoreOutlined />, [
    getItem(" Yêu cầu xuất thuốc", "3"),
  ]),
];
export default DetailCheckUp;
