import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  ShopOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import {
  Layout,
  Menu,
  Row,
  Col,
  Avatar,
  Button,
  Form,
  Input,
  Select,
  Table,
  InputNumber,
  Space,
  message,
Dropdown,
} from "antd";
import React, { useState } from "react";
import { useEffect } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import ExportService from "../../service/ExportService";
import StoreHouse from "../../service/StoreHouse";
import { showError, showSuccess } from "../../utils";
import { cloneDeep } from "lodash";
import "./Issue.css";
import ProductService from "../../service/ProductService";

function IssueWHstaff() {
  const { id } = useParams()
  const navigate = useNavigate()

  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");

  const [listStore, setListStore] = useState([])
  const [selectedStore, setSelectedStore] = useState('')
  const [detail, setDetail] = useState(null)
  const [isDone, setIsDone] = useState(false)
  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/overview");
    }
    if (e.key == 2) {
      navigate("/listTransfer");
    }
    if (e.key == 3) {
      navigate("/inventoryWHstaff");
    }
    if (e.key == 4) {
      navigate("/listReceiptWHstaff");
    }
    if (e.key == 5) {
      navigate("/listIssueWHstaff");
    }
    if (e.key == 6) {
      navigate("/productGroups");
    }
    if (e.key == 7) {
      navigate("/unitGroup");
    }
    if (e.key == 8) {
      navigate("/unit");
    }
    if (e.key == 9) {
      navigate("/product");
    }
  };


  const onChangeStore = (value) => {
    setSelectedStore(value)
  };
  const onSearch = (value) => {
    console.log("search:", value);
  };

  const fetchStore = async () => {
    const {data: { data, message} } = await StoreHouse.getAllStoreHouse()
    if (message === "MSG_SUCCESS") {
      setListStore(data.map(item => ({
        value: item.storeHouseId,
        label: item.storeHouseName
      })))
    }
  }

  const onChangeAmount = (row, value) => {
    const newData = cloneDeep(detail)
    const index = newData.iproductExportResponses.findIndex(item => item.productId === row.productId)
    if (index !== -1) {
      newData.iproductExportResponses[index].amountInput = value
      setDetail(newData)
    }
  };

  const onChangeNote = (row, value) => {
    const newData = cloneDeep(detail)
    const index = newData.iproductExportResponses.findIndex(item => item.productId === row.productId)
    if (index !== -1) {
      newData.iproductExportResponses[index].note = value
      setDetail(newData)
    }
  };

  const fetchQuantity = async () => {
    const clone = cloneDeep(detail)
    const dataQuantity = await Promise.all(
      clone.iproductExportResponses.map((item) =>
        ProductService.getProductQuantity({
          idStoreHouse: selectedStore,
          idProduct: item.productId,
        })
      )
    );
    clone.iproductExportResponses = clone.iproductExportResponses.map((item, index) => ({
      ...item,
      quantityStock: dataQuantity[index]?.data?.data?.quantity || 0
    }))
    setDetail(clone)
  }

  const fetchDetail = async () => {
    let res;
    let statusDone = false
    const {data} = await ExportService.getDetail(id)
    res = data
    if (Number(data.data?.statusID) === 2) {
      statusDone = true
      const { data: newData } = await ExportService.getDetailFull(id)
      res = newData;
    }
    
    res.data.iproductExportResponses = res.data.iproductExportResponses.map(item => ({
      ...item,
      amountInput: item?.amount || null,
      note: item?.note || ''
    }))
    if (statusDone) {
      setSelectedStore(res.data.storeHouseId)
    }
    setDetail(res.data)
    setIsDone(statusDone)
  }

  const onSubmit = async () => {
    let hasError = false
    detail.iproductExportResponses.forEach(item => {
      if (!item.amountInput) hasError = true
    })
    if (hasError || !selectedStore) {
      showError('Hãy nhập đủ các trường.')
      return;
    }
    try {
      const body = {
        idExport: id,
        exportDate: detail.exportDate,
        personInChargeId: localStorage.getItem('idToken'),
        storeHouseId: selectedStore,
        productExportRequests: detail.iproductExportResponses.map(({ productId, amountInput, note, quantityStock }) => {
          if (amountInput > quantityStock) hasError = true
          return {
            idProduct: productId,
            amount: amountInput,
            note
          }
        })
      }
      if (hasError) {
        showError('Không được vượt quá số lượng trong kho.')
        return;
      }
      const { data } = await ExportService.exportProduct(body);
      if (data?.status && data.status === 'Error') {
        throw new Error;
      }
      showSuccess()
      navigate('/listIssueWHstaff')
       
    } catch (error) {
      console.log(error)
      showError('Create failed!')
    }
  }

  useEffect(() => {
    fetchDetail()
    fetchStore()
  }, [])

  useEffect(() => {
    fetchQuantity()
  }, [selectedStore])

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];
  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo"> </div>
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub1"]}
          selectedKeys={["5"]}
          mode="inline"
          items={MenuItem}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >
                <div>
                  <Avatar
                    size="default"
                    style={{ marginRight: 10 }}
                    src={window.localStorage.getItem("avatar")}
                  ></Avatar>
                  {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <div className="div-createIssue-headercontent">
            <Space>
              <Button onClick={onSubmit}>Lưu</Button>
              <Link to='/listIssueWHstaff'>
                <Button>Hủy Bỏ</Button>
              </Link>
            </Space>
          </div>
          <div className="div-createIssue-bodycontent">
            <h2>
              Mã xuất kho: {detail?.receiptIssueId}
            </h2>
            <Form
              labelCol={{
                span: 5,
              }}
              wrapperCol={{
                span: 15,
              }}
            >
              <Form.Item label="Người Yêu cầu">
                <Input value={detail?.petitionerName} disabled />
              </Form.Item>
              <Form.Item label="Người phụ trách">
                <Input disabled value={localStorage.getItem('usernameToken')} />
              </Form.Item>
              <Form.Item label="Kho hàng">
              <Select
                        className="select-ExportStoreWarehouse-receipt"
                        showSearch
                        value={selectedStore}
                        placeholder="Chọn kho"
                        optionFilterProp="children"
                        onChange={onChangeStore}
                        filterOption={(input, option) =>
                          (option?.label ?? "")
                            .toLowerCase()
                            .includes(input.toLowerCase())
                        }
                        options={listStore}
                        disabled={isDone}
                      />
              </Form.Item>
              <div className="container">
                <Table
                  className="transfer-table"
                  columns={renderColumn(onChangeAmount, onChangeNote, isDone)}
                  dataSource={detail?.iproductExportResponses}
                  pagination={false}/>
              </div>
            </Form>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const MenuItem = [
  getItem("Kho Hàng ", "sub1", <ShopOutlined />, [
    getItem("Tổng quan", "1"),
    getItem("Điều chuyển", "2"),
    getItem("Kiểm kê", "3"),
    getItem("Nhập kho", "4"),
    getItem("Xuất kho", "5"),
    getItem("Nhóm sản phẩm", "6"),
    getItem("Nhóm đơn vị", "7"),
    getItem("Đơn vị", "8"),
    getItem("Sản phẩm", "9"),
  ]),
];

const renderColumn = (onChangeAmount, onChangeNote, isDone) => ([
  {
    title: 'Tên Sản Phẩm',
    dataIndex: 'productName',
    key: 'productName',
    width:' 30%',
  },
  {
    title: 'Nhu Cầu',
    dataIndex: 'numberOfRequests',
    key: 'numberOfRequests',
  },
  // {
  //   title: isDone ? 'Đã xuất' : undefined,
  //   dataIndex: isDone ? 'amount' : undefined,
  //   key: isDone ? 'amount' : undefined,
  // },
  {
    title: 'Số lượng kho',
    dataIndex: 'quantityStock',
    key: 'quantityStock',
  },
  {
    title: 'Số lượng xuất',
    dataIndex: 'amountInput',
    key: 'amountInput',
    render: (_, row) => <InputNumber
      value={_}
      min={0}
      max={row?.quantityStock !== 0 ? row?.quantityStock : 0}
      onChange={(val) => onChangeAmount(row, val)}
      className="input-transfer"
      disabled={isDone}
      />
  },
  {
    title: 'Ghi chú',
    dataIndex: 'note',
    key: 'note',
    render: (_, row) => <Input value={_} onChange={(e) => onChangeNote(row, e.target.value)} className="input-transfer" disabled={isDone}/>
  },
])

export default IssueWHstaff;
