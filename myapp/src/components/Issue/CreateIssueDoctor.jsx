import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  ShopOutlined,
  MinusCircleOutlined,
  LogoutOutlined,
  EditOutlined,
  AppstoreOutlined

} from "@ant-design/icons";
import {
  Layout,
  Menu,
  Row,
  Col,
  Avatar,
  Button,
  Form,
  Input,
  Select,
  notification,
  DatePicker,
  InputNumber,
  Table,
  message,
  Dropdown,

} from "antd";
import React, { useEffect, useState } from "react";
import "./CreateIssue.css";
import ProductService from "../../service/ProductService";
import { cloneDeep } from "lodash";
import ExportService from "../../service/ExportService";
import { useNavigate } from "react-router-dom";
const dateFormatList = ["YYYY-MM-DD"];

function CreateIssueDoctor() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");

  const [indexRow, setIndexRow] = useState(0)
  const [productSelect, setProductSelect] = useState([]);
  const [dataSource, setDataSource] = useState([])
  const [dateTransfer, setDateTransfer] = useState()
  const navigate = useNavigate();
  const backListIssue = ()=>{navigate(-1);}

  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/checkUp");
    }
    if (e.key == 2) {
      navigate("/medicalRecord");
    }
    if (e.key == 3) {
      navigate("/listIssueDoctor");
    }

  };

  const onChangeDate = (date, dateString) => {
    setDateTransfer(dateString)
  };

  const onChangeProduct = (row, value) => {
    const newData = cloneDeep(dataSource)
    const index = newData.findIndex(item => item.id === row.id)
    if (index !== -1) {
      newData[index].productId = value
      setDataSource(newData)
    }
  };

  const onChangeNumberRequest = (row, value) => {
    const newData = cloneDeep(dataSource)
    const index = newData.findIndex(item => item.id === row.id)
    if (index !== -1) {
      newData[index].numberOfRequest = value
      setDataSource(newData)
    }
  }

  const addNewRow = () => {
    const newData = cloneDeep(dataSource)
    newData.push({
      ...defaultRow,
      id: indexRow + 1
    })
    setDataSource(newData)
    setIndexRow(indexRow + 1)
  }

  const deleteRow = (row) => {
    const newData = cloneDeep(dataSource)
    const index = newData.findIndex(item => item.id === row.id)
    if (index !== -1) {
      newData.splice(index, 1)
      setDataSource(newData)
    }
  }

  const showError = (description) => {
    notification.error({
      message: 'Opps!',
      description,
    });
  };

  const onSubmit = async () => {
    let hasError = false
    let errorDuplicate = false
    let productIds = []
    if (dataSource.length === 0) {
      showError('Hãy thêm ít nhất 1 sản phẩm.')
      return;
    }
    dataSource.forEach(item => {
      if (!item.productId || !item.numberOfRequest) hasError = true
      if (productIds.includes(item.productId)) {
        errorDuplicate = true
      }
      productIds.push(item.productId)
    })
    if (!dateTransfer) {
      hasError = true
    }
    if (hasError) {
      showError('Hãy nhập đủ các trường.')
      return;
    }
    if (errorDuplicate) {
      showError('Không được chọn cùng 1 sản phẩm trên nhiều dòng.')
      return;
    }
    try {
      const body = {
        expectedDate: dateTransfer,
        petitionerId: localStorage.getItem('idToken'),
        productExRequests: dataSource.map(({ productId, numberOfRequest}) => ({
          productId,
          numberOfRequest
        }))
      }
      const { data } = await ExportService.create(body)
      if (data?.status && data.status === 'Error') {
        throw new Error;
      }
      showSuccess();
      navigate("/listIssueDoctor")
    } catch (error) {
      console.log(error)
      showError('Create failed!')
    }
  }

  const showSuccess = (description) => {
    notification.success({
      message: 'Success!',
      description,
    });
  }

  useEffect(()=>{
    const getAllProductSelect = async () =>{
      const res = await ProductService.getAllProduct()
      setProductSelect(res.data.data.map(item => ({
        value: item.productId,
        label: item.productName,
      })))
    }
    getAllProductSelect()
  },[])

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };
  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];

  return (
    
<Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo" />
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub3"]}
          selectedKeys={["3"]}
          mode="inline"
          items={Menuitems}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >  
              <div>
                <Avatar
                  size="default"
                  style={{ marginRight: 10 }}
                  src={window.localStorage.getItem("avatar")}
                ></Avatar>
                {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <div className="div-createIssue-headercontent">
            <Row>
              <Col md={1}></Col>
              <Col md={2}>
                <Button onClick={onSubmit}>Lưu</Button>
              </Col>
              <Col md={2}>
                <Button onClick={backListIssue}>Hủy Bỏ</Button>
              </Col>
            </Row>
          </div>
          
          <div className="div-createIssue-bodycontent">
            <Form
              labelCol={{
                span: 5,
              }}
              wrapperCol={{
                span: 15,
              }}
            >
              <Form.Item label="Người yêu cầu">
                <Input disabled value={localStorage.getItem("usernameToken")} />
              </Form.Item>
              <Form.Item label="Ngày yêu cầu">
                <DatePicker onChange={onChangeDate} format={dateFormatList} />
              </Form.Item>
              <Button type="primary" onClick={addNewRow}>Thêm 1 dòng</Button>
              <div className="container">
                <Table
                  className="transfer-table"
                  columns={renderColumn(productSelect, deleteRow, onChangeProduct, onChangeNumberRequest)}
                  dataSource={dataSource}
                  pagination={false}/>
              </div>

            </Form>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const Menuitems = [
  getItem("Khám Bệnh", "sub1", <AppstoreOutlined />, [
    getItem("Danh sách chờ", "1"),
  ]),
  getItem("Hồ sơ bệnh nhân", "sub2", <AppstoreOutlined />, [
    getItem(" Hồ Sơ bệnh  nhân", "2"),
  ]),
  getItem("Yêu cầu xuất thuốc", "sub3", <AppstoreOutlined />, [
    getItem(" Yêu cầu xuất thuốc", "3"),
  ]),
];

const defaultRow = {
  productId: '',
  numberOfRequest: '',
}

const renderColumn = (listProduct, deleteRow, onChangeProduct, onChangeNumberRequest) => ([
  {
    title: '',
    dataIndex: 'delete',
    key: 'delete',
    width: '5%',
    render: (_, row) => <Button className="btn-del-trans" onClick={() => deleteRow(row)} type="primary" icon={<MinusCircleOutlined />} danger />
  },
  {
    title: 'Sản Phẩm',
    dataIndex: 'productId',
    key: 'productId',
    width:' 30%',
    render: (_, row) => (
      <Select
        showSearch
        placeholder="Select Product"
        style={{ width: '100%' }}
        optionFilterProp="children"
        onChange={(val) => onChangeProduct(row, val)}
        filterOption={(input, option) =>
          (option?.label ?? "")
            .toLowerCase()
            .includes(input.toLowerCase())
        }
        options={listProduct}
      />
    )
  },
  {
    title: 'Số lượng',
    dataIndex: 'numberOfRequest',
    key: 'numberOfRequest',
    render: (_, row) => <InputNumber onChange={(val) => onChangeNumberRequest(row, val)} className="input-transfer" min={0} />
  },
])

export default CreateIssueDoctor;
