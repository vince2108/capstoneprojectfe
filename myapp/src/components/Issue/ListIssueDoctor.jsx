import {
    MenuFoldOutlined,
    MenuUnfoldOutlined,
    AppstoreOutlined,
    EditOutlined,
    LogoutOutlined,
  } from "@ant-design/icons";
  import {
  Layout,
  Menu,
  Row,
  Col,
  Avatar,
  Form,
  Select,
  Button,
  Table,
  message,
  Dropdown,
  } from "antd";
  import React, { useEffect, useState } from "react";
  import { useNavigate } from "react-router-dom";
  import ExportService from "../../service/ExportService";
  import "./ListIssue.css";
  
  function ListIssueDoctor() {
    const navigate = useNavigate()
  
    const { Header, Sider, Content } = Layout;
    const [collapsed, setCollapsed] = useState(false);
    const [theme] = useState("dark");
    const [current, setCurrent] = useState("1");
    const [status, setStatus] = useState('0')
    const [listIssue, setListIssue] = useState();
    const goToCreateIssue = ()=>{navigate("/createIssueDoctor");}
  
    const onClick = (e) => {
      console.log("click ", e);
      // setCurrent(e.key);
      if (e.key == 1) {
        navigate("/checkUp");
      }
      if (e.key == 2) {
        navigate("/medicalRecord");
      }
      if (e.key == 3) {
        navigate("/listIssueDoctor");
      }
  
    };
  
    const onChange = (value) => {
      setStatus(value)
    };
  
    const onSelectRow = (id) => {
      id && navigate(`/issueWHstaff/${id}`)
    }
  
    const fetchList = () => {
      if (status === '0') {
        ExportService.getListExport({ idBranch: localStorage.getItem('branchId') }).then(({ data }) => {
          setListIssue(data.data)
        })
      } else {
        ExportService.searchListExport({ idBranch: localStorage.getItem('branchId'), idStatus: status }).then(({ data }) => {
          setListIssue(data.data)
        })
      }
    }
  
    useEffect(() => {
      fetchList()
    }, [status])
    const logOutAction = () => {
      localStorage.clear();
      message.success("Đăng xuất thành công");
      navigate("/");
    };
  
    const handleMenuClick = (e) => {
      if (e.key === "1") {
        navigate("/updateStaff", {
          state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
        });
      } else if (e.key === "2") {
        logOutAction();
      }
    };
  
    const items = [
      {
        label: "Cập Nhập Thông Tin",
        key: "1",
        icon: <EditOutlined />,
      },
      {
        label: "Đăng Xuất",
        key: "2",
        icon: <LogoutOutlined />,
        style: { color: "red" },
      },
    ];
    return (
      <Layout>
        <Sider trigger={null} collapsible collapsed={collapsed}>
          <div className="logo"> </div>
          <Menu
            theme={theme}
            onClick={onClick}
            defaultOpenKeys={["sub3"]}
            selectedKeys={["3"]}
            mode="inline"
            items={MenuItem}
          />
        </Sider>
        <Layout className="site-layout">
          <Header
            className="site-layout-background"
            style={{
              padding: 0,
            }}
          >
            <Row>
              <Col md={20}>
                {React.createElement(
                  collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                  {
                    className: "trigger",
                    onClick: () => setCollapsed(!collapsed),
                  }
                )}
              </Col>
              <Col md={4}>
                <Dropdown
                  menu={{
                    items,
                    onClick: handleMenuClick,
                  }}
                  placement="bottomLeft"
                >
                  <div>
                    <Avatar
                      size="default"
                      style={{ marginRight: 10 }}
                      src={window.localStorage.getItem("avatar")}
                    ></Avatar>
                    {window.localStorage.getItem("usernameToken")}
                  </div>
                </Dropdown>
              </Col>
            </Row>
          </Header>
          <Content
            className="site-layout-background"
            style={{
              overflow: "scroll",
              margin: "24px 16px",
              padding: 24,
              minHeight: 280,
            }}
          >
            <div className="div-issue-headercontent">
              <Row>
                <Col md={1}></Col>
                <Col md={8}>
                 <Button onClick={goToCreateIssue}>Tạo</Button> 
                </Col>
                <Col md={10}></Col>
                <Col md={5}> Sắp xếp: &nbsp;
                  <Select
                    className="select-status-issue"
                    placeholder="Chọn trạng thái"
                    value={status}
                    onChange={onChange}
                    options={STATUSES}
                  />
                </Col>
              </Row>
            </div>
            <div className="div-issue-bodycontent">
              <h1>Danh Sách Xuất Kho</h1>
              <Form
                labelCol={{
                  span: 5,
                }}
                wrapperCol={{
                  span: 15,
                }}
              >
                 <div className="container">
                 <Table
                  className="transfer-table"
                  columns={columns}
                  dataSource={listIssue}
                  pagination={false}
                  // onRow={(record) => {
                  //   return {
                  //     onClick: () => onSelectRow(record?.receiptIssueId), // click row
                  //   };
                  // }}
                   />
                </div>
              </Form>
            </div>
          </Content>
        </Layout>
      </Layout>
    );
  }
  function getItem(label, key, icon, children, type) {
    return {
      key,
      icon,
      children,
      label,
      type,
    };
  }
  const MenuItem = [
    getItem("Khám Bệnh", "sub1", <AppstoreOutlined />, [
      getItem("Danh sách chờ", "1"),
    ]),
    getItem("Hồ sơ bệnh nhân", "sub2", <AppstoreOutlined />, [
      getItem(" Hồ Sơ bệnh  nhân", "2"),
    ]),
    getItem("Yêu cầu xuất thuốc", "sub3", <AppstoreOutlined />, [
      getItem(" Yêu cầu xuất thuốc", "3"),
    ]),
  ];
  
  export const STATUSES = [
    {
      value: "0",
      label: "Tất cả",
    },
    {
      value: "1",
      label: "Sẵn sàng",
    },
    {
      value: "2",
      label: "Hoàn thành",
    },
    {
      value: "3",
      label: "Hủy bỏ",
    },
  ]
  
  const columns = [
    {
      key: 'receiptIssueId',
      dataIndex: 'receiptIssueId',
      title: 'Mã phiếu'
    },
    {
      key: 'petitionerName',
      dataIndex: 'petitionerName',
      title: 'Người yêu cầu'
    },
    {
      key: 'expectedDate',
      dataIndex: 'expectedDate',
      title: 'Ngày yêu cầu'
    },
    {
      key: 'statusName',
      dataIndex: 'statusName',
      title: 'Trạng thái'
    },
  ]
  
  export default ListIssueDoctor;
  