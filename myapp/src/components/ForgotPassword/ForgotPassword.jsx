import "./ForgotPassword.css";
import { Row, Col, Button, Input, Form, message } from "antd";
import AuthenticationService from "../../service/AuthenticationService";

import React from "react";
import {  useNavigate } from "react-router-dom";

function ForgotPassword() {
  const navigate = useNavigate();
  const SaveAction = async (values) => {
    
   
    const resdata = await AuthenticationService.forgotPassword(
   
      values.name
     
    );
    console.log(values.name)
    if(resdata.data.status !== "Success"){
      message.error("Không có tài khoản nào như thế!")
    } else{
      navigate("/changePassword");
    }
    
  };
  const ReturnAction = () => {
    navigate(-1);
  };
  return (
    <div className="forgotPassword-body">
      <div className="forgotPassword-form">
        <h2 className="forgorpassword-title">QUÊN MẬT KHẨU</h2>
        <Form
        onFinish={(values) => SaveAction(values)}
     
          labelCol={{
            span: 4,
          }}
          wrapperCol={{
            span: 16,
          }}
          
        >
          <Form.Item   label="Nhập tài khoản" 
              label-size="20x"
              name="name"

                 rules={[
                  {
                    required: true,
                    message: 'Vui Lòng Điền Tên Tài Khoản',
                  },
                ]}
                
                hasFeedback>
            <Input className="forgotpassword-input" />
          </Form.Item>
          <Form.Item>
                <Row>
                  <Col md={1}></Col>
                  <Col md={5}>
                    <Button
                      type="primary"
                      htmlType="submit"
                    >
                      Lưu
                    </Button>
                  </Col>
                  <Col md={2}>
                    <Button onClick={ReturnAction}>Hủy</Button>
                  </Col>
                </Row>
              </Form.Item>
        </Form>
    
      </div>
    </div>
  );
}
export default ForgotPassword;
