import "./ForgotPassword2.css";
import { Row, Col, Button,Avatar } from "antd";

import React from "react";

function ForgotPassword2() {
  return (
    <div className="forgotPassword2-body">
      <div className="forgotPassword2-form">
        <h2 className="forgotPassword2-title">QUÊN MẬT KHẨU</h2>
        <Row>
          <Col md={12}>
            <label className="label">Email đăng ký:</label>
            <br />
            <label className="label">LongND@gmail.com</label>
          </Col>
          <Col md={12}>
            <label className="label">Tài khoản</label>
            <br />
            <Avatar className="forgotPassword2-avatar"></Avatar>
            <br />
            <label className="label">LongND</label>
          </Col>
        </Row>
        <div>
          <Row>
            <Col md={8}></Col>
            <Col md={4}>
              <Button>Tiếp tục</Button>
            </Col>
            <Col md={4}>
              <Button>trở về</Button>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
}
export default ForgotPassword2;
