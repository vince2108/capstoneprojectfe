import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  ShopOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import {
  Layout,
  Menu,
  Row,
  Col,
  Avatar,
  Button,
  Form,
  Input,
  Card,
  InputNumber,
  message,
  Dropdown,
} from "antd";
import React, { Fragment, useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import ReceiptService from "../../service/ReceiptService";
import "./Receipt.css";

function ReceiptWHstaff() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");
  const [receiptId,setReceiptId] = useState();
  const [supplierName, setSupplierName] = useState();
  const [storeHouseName, setStoreHouseName] = useState();
  const [storeHouseId, setStoreHouseId] = useState();
  const [expectedDate, setExpectedDate] = useState();
  const [productList, setProductList] = useState([]);
  const [maxAmount, setMaxAmount] = useState(0);
  const [receiptListHistory,setReceiptListHistory] = useState([]);
  const navigate = useNavigate();
  const location = useLocation();
  let newDate = new Date();
  let date = newDate.getDate();
  let month = newDate.getMonth() + 1;
  let year = newDate.getFullYear();
  const currentDate = date + "/" + month + "/" + year;
  let currentDateFormYYYYMMDD = year + "-" + month + "-" + date;
  useEffect(() => {
    const setDataForPage = async () => {
      const res = await ReceiptService.getReceiptById(location.state.receiptId);
      console.log(res.data.data);
      setReceiptId(res.data.data.receiptIssueId);
      setSupplierName(res.data.data.supplierName);
      setStoreHouseName(res.data.data.storeHouseName);
      setExpectedDate(res.data.data.ExpectedDate);
      setProductList(res.data.data.productsResponse);
      setStoreHouseId(res.data.data.receivingWarehouseID);
      const res2 = await ReceiptService.receiptHistory(location.state.receiptId)
      console.log(res2.data.data)
      setReceiptListHistory(res2.data.data)
    };
    setDataForPage();
  }, []);

  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/overview");
    }
    if (e.key == 2) {
      navigate("/listTransfer");
    }
    if (e.key == 3) {
      navigate("/inventoryWHstaff");
    }
    if (e.key == 4) {
      navigate("/listReceiptWHstaff");
    }
    if (e.key == 5) {
      navigate("/listIssueWHstaff");
    }
    if (e.key == 6) {
      navigate("/productGroups");
    }
    if (e.key == 7) {
      navigate("/unitGroup");
    }
    if (e.key == 8) {
      navigate("/unit");
    }
  };

  const BackAction = () => {
    navigate(-1);
  };

  const SaveAction = async () => {
    console.log(localStorage.getItem("idToken"))
    const res = await ReceiptService.receiptProduct(
      location.state.receiptId,
      localStorage.getItem("idToken"),
      currentDateFormYYYYMMDD,
      productList
    );
    var isFinish = true
    for(var i =0; i<productList.length; i++){
      if(productList[i].numberOfRequests !== productList[i].numberReceipted+productList[i].amount){
        isFinish = false
        console.log(isFinish)
      }
    }
    if(isFinish=== true){
      const res = await ReceiptService.updateStatus(location.state.receiptId)
      message.success("Kết thúc phiếu nhập MP" + location.state.receiptId)
    }else {
      message.success("Cập nhật số lượng phiếu nhập MP" + location.state.receiptId)
    }
    navigate(-1);

  };

  const inputRealValue = (event, productId, numberOfRequests,numberReceipted1) => {
    console.log(event)
    const value = Math.max(
      0,
      Math.min(numberOfRequests-numberReceipted1, Number(event))
    );
    console.log(value)
    setMaxAmount(value);
    const productRequests = productList.map((obj) => {
      if (obj.productId === productId) {
        return { ...obj, amount: value };
      }
      return obj; 
    });
    setProductList(productRequests);
  };

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];

  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo"> </div>
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub1"]}
          selectedKeys={["4"]}
          mode="inline"
          items={MenuItem}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >
                <div>
                  <Avatar
                    size="default"
                    style={{ marginRight: 10 }}
                    src={window.localStorage.getItem("avatar")}
                  ></Avatar>
                  {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <Row>
            <Col md={17} className="body-left-receipt">
              <div className="div-receipt-headercontent">
                <Row>
                  <Col md={1}></Col>
                  <Col md={3}>
                    <Button onClick={() => SaveAction()}>Xác nhận</Button>
                  </Col>
                  <Col md={2}>
                    <Button onClick={() => BackAction()}>Hủy bỏ</Button>
                  </Col>
                </Row>
              </div>
              <div className="div-receipt-bodycontent">
                <h1 style={{ marginLeft: "190px" }}>MÃ PHIẾU: {"NK"+receiptId}</h1>
                <Form
                  labelCol={{
                    span: 5,
                  }}
                  wrapperCol={{
                    span: 15,
                  }}
                >
                  <Form.Item label="Nhập từ" style={{ marginBottom: 0 }}>
                    <Row>
                      <Col md={12}>
                        <Form.Item
                          style={{
                            display: "inline-block",
                            width: "calc(100% - 8px)",
                            color: "#0648ff",
                          }}
                        >
                          {supplierName}
                        </Form.Item>
                      </Col>
                      <Col md={12}>
                        <Form.Item
                          label="Ngày dự kiến"
                          style={{
                            display: "inline-block",
                            width: "calc(100% - 8px)",
                            color: "#0648ff",
                          }}
                        >
                          {expectedDate}
                        </Form.Item>
                      </Col>
                    </Row>
                  </Form.Item>

                  <Form.Item label="Kho hàng" style={{ marginBottom: 0 }}>
                    <Row>
                      <Col md={12}>
                        <Form.Item
                          style={{
                            display: "inline-block",
                            width: "calc(100% - 8px)",
                            color: "#0648ff",
                          }}
                        >
                          {storeHouseName}
                        </Form.Item>
                      </Col>
                      <Col md={12}>
                        <Form.Item
                          label="Ngày nhập kho"
                          style={{
                            display: "inline-block",
                            width: "calc(100% - 8px)",
                            color: "#0648ff",
                          }}
                        >
                          {currentDateFormYYYYMMDD}
                        </Form.Item>
                      </Col>
                    </Row>
                  </Form.Item>
                </Form>
                <table className="tab1-receipt">
                  <thead>
                    <th>Tên thuốc</th>
                    <th>Nhu cầu</th>
                    <th>Số lượng đã nhập</th>
                    <th>Số lượng nhập</th>
                  </thead>
                  <tbody>
                    {productList?.map((product) => {
                      return (
                        <tr>
                          <td data-label="col-1">{product.productName}</td>
                          <td data-label="col-2">{product.numberOfRequests}</td>
                          <td data-label="col-3">{product.numberReceipted ===null? 0 : product.numberReceipted}</td>
                          <td data-label="col-4">
                            <InputNumber
                              type="number"
                              placeholder="Số lượng nhập"
                              // value={maxAmount}
                              max={product.numberOfRequests - product.numberReceipted}
                              onChange={(event) =>
                                inputRealValue(
                                  event,
                                  product.productId,
                                  product.numberOfRequests,
                                  product.numberReceipted
                                )
                              }
                            ></InputNumber>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </Col>
            <Col md={6} title="Lịch sử phiếu nhập">
            <h1>Lịch Sử phiếu nhập</h1>
            {receiptListHistory?.map((receipt)=>{
              return(
                <Fragment>
                <Card
                className="card-receipt "
                title={"Ngày Nhập:" + receipt.dateReceipt}
                bordered={false}
              >
                <div>
                  <Avatar
                    size="default"
                    style={{ marginRight: 10 }}
                    src={receipt.Avatar}
                  ></Avatar>
                  {receipt.fullName}
                </div>
              </Card>
              <br></br>
              </Fragment>
              )
            })}
              
            </Col>
          </Row>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const MenuItem = [
  getItem("Kho Hàng ", "sub1", <ShopOutlined />, [
    getItem("Tổng quan", "1"),
    getItem("Điều chuyển", "2"),
    getItem("Kiểm kê", "3"),
    getItem("Nhập kho", "4"),
    getItem("Xuất kho", "5"),
    getItem("Nhóm sản phẩm", "6"),
    getItem("Nhóm đơn vị", "7"),
    getItem("Đơn vị", "8"),
    getItem("Sản phẩm", "9"),
  ]),
];

export default ReceiptWHstaff;
