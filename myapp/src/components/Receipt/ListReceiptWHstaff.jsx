import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  ShopOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import {
  Layout,
  Menu,
  Row,
  Col,
  Avatar,
  Select,
  message,
  Dropdown,
} from "antd";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import ReceiptService from "../../service/ReceiptService";
import "./ListReceipt.css";

function ListReceiptWHstaff() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");
  const [allListReceipt, setAllListReceipt] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    const setDataForPage = async () => {
      const res = await ReceiptService.getAllReceiptByBranch(
        localStorage.getItem("branchId")
      );
      setAllListReceipt(res.data.data);
      console.log(res.data.data);
    };
    setDataForPage();
  }, []);

  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/overview");
    }
    if (e.key == 2) {
      navigate("/listTransfer");
    }
    if (e.key == 3) {
      navigate("/inventoryWHstaff");
    }
    if (e.key == 4) {
      navigate("/listReceiptWHstaff");
    }
    if (e.key == 5) {
      navigate("/listIssueWHstaff");
    }
    if (e.key == 6) {
      navigate("/productGroups");
    }
    if (e.key == 7) {
      navigate("/unitGroup");
    }
    if (e.key == 8) {
      navigate("/unit");
    }
    if (e.key == 9) {
      navigate("/product");
    }
  };

  const onChange = async (value) => {
    console.log(value);
    if (value === "0") {
      const res = await ReceiptService.getAllReceiptByBranch(
        localStorage.getItem("branchId")
      );
      console.log(res.data.data);
      setAllListReceipt(res.data.data);
    } else {
      const res = await ReceiptService.searchRecieveByStatusAndBranch(
        localStorage.getItem("branchId"),
        value
      );
      setAllListReceipt(res.data.data);
      console.log(res.data.data);
    }
  };
  const onSearch = (value) => {
    console.log("search:", value);
  };

  const getDetailReceipt = (receiptId) => {
    navigate("/receiptWHstaff", { state: { receiptId: receiptId } });
  };

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];

  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo"> </div>
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub1"]}
          selectedKeys={["4"]}
          mode="inline"
          items={MenuItem}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >
                <div>
                  <Avatar
                    size="default"
                    style={{ marginRight: 10 }}
                    src={window.localStorage.getItem("avatar")}
                  ></Avatar>
                  {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <div className="div-listreceipt-headercontent">
            <Row>
              <Col md={5}><h1>DANH SÁCH NHẬP KHO</h1></Col>
              <Col md={13}></Col>
              <Col md={6}>
                {" "}
                Sắp xếp: &nbsp;
                <Select
                  className="select-status-listreceipt"
                  showSearch
                  placeholder="chọn trạng thái"
                  defaultValue={"Tất cả"}
                  optionFilterProp="children"
                  onChange={onChange}
                  onSearch={onSearch}
                  filterOption={(input, option) =>
                    (option?.label ?? "")
                      .toLowerCase()
                      .includes(input.toLowerCase())
                  }
                  options={[
                    {
                      value: "0",
                      label: "Tất Cả",
                    },
                    {
                      value: "1",
                      label: "Sẵn Sàng",
                    },
                    {
                      value: "2",
                      label: "Hoàn thành",
                    },
                    {
                      value: "3",
                      label: "Hủy bỏ",
                    },
                  ]}
                />
              </Col>
            </Row>
          </div>
          <div className="div-listreceipt-bodycontent">
            
            <table className="tab1-listreceipt">
              <thead>
                <th>Mã Phiếu</th>
                <th>Tên Kho</th>
                <th>Nhà cung cầu</th>
                <th>Ngày dự kiến</th>
                <th>Trạng thái</th>
              </thead>
              <tbody>
                {allListReceipt?.map((receipt) => {
                  return (
                    <tr
                      key={receipt.receiptIssueId}
                      onDoubleClick={() =>
                        getDetailReceipt(receipt.receiptIssueId)
                      }
                    >
                      <td data-label="col-1">
                        {"MP" + receipt.receiptIssueId}
                      </td>
                      <td data-label="col-2">{receipt.storeHouseName}</td>
                      <td data-label="col-3">{receipt.supplierName}</td>
                      <td data-label="col-4">{receipt.expectedDate}</td>
                      <td data-label="col-5">{receipt.statusName}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const MenuItem = [
  getItem("Kho Hàng ", "sub1", <ShopOutlined />, [
    getItem("Tổng quan", "1"),
    getItem("Điều chuyển", "2"),
    getItem("Kiểm kê", "3"),
    getItem("Nhập kho", "4"),
    getItem("Xuất kho", "5"),
    getItem("Nhóm sản phẩm", "6"),
    getItem("Nhóm đơn vị", "7"),
    getItem("Đơn vị", "8"),
    getItem("Sản phẩm", "9"),
  ]),
];

export default ListReceiptWHstaff;
