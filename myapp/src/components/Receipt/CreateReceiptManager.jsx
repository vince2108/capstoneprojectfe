import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  ShopOutlined,
  MinusCircleOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import {
  Layout,
  Menu,
  Row,
  Col,
  Avatar,
  Button,
  Form,
  Input,
  Select,
  DatePicker,
  Space,
  InputNumber,
  message,
  Table,
  Dropdown,
} from "antd";
import { cloneDeep } from 'lodash';
import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import ProductService from "../../service/ProductService";
import ReceiptService from "../../service/ReceiptService";
import StoreHouse from "../../service/StoreHouse";
import SupplierService from "../../service/SupplierService";
import { showError, showSuccess } from "../../utils";
import ProductGroups from "../ProductGroups/ProductGroups";
import "./CreateReceipt.css";
const dateFormatList = ["DD/MM/YYYY", "DD/MM/YY"];

function CreateReceiptManager() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");
  const [allSupplier, getAllSupplier] = useState([]);
  const [selectedSupplier, setSelectedSupplier] = useState();
  const [allStoreHouse, getAllStoreHouse] = useState([]);
  const [selectedStoreHouse, setSelectedStoreHouse] = useState();
  const [orderDate, setOrderDate] = useState();
  const [expectedDate, setexpectedDate] = useState();
  const [firstClickAddNew, setFirstClickAddNew] = useState(true);
  const [allProduct, setAllProduct] = useState([]);

  const [productId, setProductId] = useState();
  const [productName, setProductName] = useState();
  const [unitChoice, setUnitChoice] = useState();
  const [productAmount, setProductAmount] = useState();
  const [unitPrice, setUnitPrice] = useState();
  const [listProductShow, setListProductShow] = useState([]);
  const [listProductInsert, setlistProductInsert] = useState([]);
  const [valueCombobox, setValueCombobox] = useState("Chọn Thuốc");
  const [indexRow, setIndexRow] = useState(0)

  const [dataSource, setDataSource] = useState([])
  const navigate = useNavigate();
  let newDate = new Date();
  let date = newDate.getDate() + 1;
  let month = newDate.getMonth() + 1;
  let year = newDate.getFullYear();
  const currentDate = date + "/" + month + "/" + year;
  let currentDateFormYYYYMMDD = year + "-" + month + "-" + date;

  useEffect(() => {
    const setDataForPage = async () => {
      const res = await SupplierService.getAllSupplier();
      const res2 = await StoreHouse.getAllStoreHouse();
      const res3 = await ProductService.getAllProduct();
      getAllSupplier(res.data.data);
      getAllStoreHouse(res2.data.data);
      setAllProduct(res3.data.data.map(item => ({
        value: item.productId,
        label: item.productName,
        unit: item.unitName
      })));
    };
    setDataForPage();
  }, []);
  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/staff");
    }
    if (e.key == 2) {
      navigate("/clinic");
    }
    if (e.key == 3) {
      navigate("/report");
    }
    if (e.key == 4) {
      navigate("/service");
    }
    if (e.key == 5) {
      navigate("/divison");
    }
    if (e.key == 6) {
      navigate("/listReceiptManager");
    }
    if (e.key == 7) {
      navigate("/ListTransfer")
    }
    if (e.key == 8) {
      navigate("/inventoryManager")
    }
    if (e.key == 9) {
      // navigate("/inventoryManager")
    }
    if (e.key == 10) {
      navigate("/warehouse")
    }
  };

  const onChange = (value) => {
    console.log(`selected ${value}`);
  };
  const onSearch = (value) => {
    console.log("search:", value);
  };

  const onChangeProduct = (row, value) => {
    const newData = cloneDeep(dataSource)
    const index = newData.findIndex(item => item.id === row.id)
    if (index !== -1) {
      newData[index].productId = value
      setDataSource(newData)
    }
  }

  const onChangeNumberRequest = (row, value) => {
    const newData = cloneDeep(dataSource)
    const index = newData.findIndex(item => item.id === row.id)
    if (index !== -1) {
      newData[index].numberOfRequest = value
      setDataSource(newData)
    }
  }

  const onChangePrice = (row, value) => {
    const newData = cloneDeep(dataSource)
    const index = newData.findIndex(item => item.id === row.id)
    if (index !== -1) {
      newData[index].price = value
      setDataSource(newData)
    }
  }

  const addNewRow = () => {
  const newData = cloneDeep(dataSource)
    newData.push({
      ...defaultRow,
      id: indexRow + 1
    })
    setDataSource(newData)
    setIndexRow(indexRow + 1)
  }

  const deleteRow = (row) => {
    const newData = cloneDeep(dataSource)
    const index = newData.findIndex(item => item.id === row.id)
    if (index !== -1) {
      newData.splice(index, 1)
      setDataSource(newData)
    }
  }

  const createProduct = () => {
    navigate("/createProduct");
  };

  const clickAddNewMedicineAction = () => {
    document.getElementById("inputAddNew").style.display = "table-row";
    if (firstClickAddNew === false) {
      const ObjectShow = {
        productId: productId,
        productName: productName,
        numberOfRequest: productAmount,
        price: unitPrice,
        entryPrice: productAmount * unitPrice,
        unitName: unitChoice,
      };

      const ObjectInsert = {
        productId: productId,
        numberOfRequest: productAmount,
        entryPrice: productAmount * unitPrice,
      };

      console.log(ObjectShow);
      console.log(ObjectInsert);

      setListProductShow([...listProductShow, ObjectShow]);
      setlistProductInsert([...listProductInsert, ObjectInsert]);
      // console.log(ObjectMedicineShow)
      // console.log(ObjectMedicineInsert)
      // setMedicineListShow([...medicineListShow, ObjectMedicineShow]);
      // setMedicienListInsert([...medicineListInsert,ObjectMedicineInsert])
      setValueCombobox("chọn thuốc");
      setUnitPrice();
      setProductAmount();
      setUnitChoice();
    }
    setFirstClickAddNew(false);
  };

  const setValue = (key, value, event) => {
    console.log(value);
    console.log(key);
    setUnitChoice(value.value);
    setProductId(value.key);
    setProductName(value.children);
    setValueCombobox(value.children);
    // console.log("id thuoc " + value.value);
    // console.log("ten thuoc " + value.label);
    // const newListMedicine = medicineList.filter(
    //   (medicine) => medicine.medicineId != value.value
    // );
    // setMedicineList(newListMedicine);
  };

  const SaveActionFun = async() => {
    const ObjectInsert = {
      productId: productId,
      numberOfRequest: productAmount,
      entryPrice: productAmount * unitPrice,
    };

    console.log(ObjectInsert);
    const productRequests = [...listProductInsert, ObjectInsert]
    console.log(productRequests)
    const res = await ReceiptService.createReceipt(selectedStoreHouse,expectedDate,localStorage.getItem("idToken"),selectedSupplier,productRequests);
    message.success("Tạo yêu cầu thành công")
    navigate('/listReceiptManager',{replace: true})
};

  const onSubmit = async () => {
    let hasError = false
    let errorDuplicate = false
    let productIds = []
    if (dataSource.length === 0) {
      showError('Hãy thêm ít nhất 1 sản phẩm.')
      return;
    }
    dataSource.forEach(item => {
      if (!item.productId || !item.numberOfRequest || !item.price) hasError = true
      if (productIds.includes(item.productId)) {
        errorDuplicate = true
      }
      productIds.push(item.productId)
    })
    if (hasError) {
      showError('Hãy nhập đủ các trường.')
      return;
    }
    if (errorDuplicate) {
      showError('Không được chọn cùng 1 sản phẩm trên nhiều dòng.')
      return;
    }
    const body = {
      receivingWareHouseId: selectedStoreHouse,
      expectedDate,
      petitionerId: localStorage.getItem("idToken"),
      supplierId: selectedSupplier,
      productReRequests: dataSource.map(({ productId, numberOfRequest, price}) => ({
        productId,
        numberOfRequest,
        entryPrice: price * numberOfRequest
      }))
    }
    try {
      const { data } = await ReceiptService.createReceipt(body);
      if (data?.status && data.status === 'Error') {
        throw new Error;
      }
      showSuccess()
      navigate("/listReceiptManager")
    } catch (error) {
      showError('Tạo thất bại!')
    }
  }

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];

  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo" />
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub2"]}
          selectedKeys={["6"]}
          mode="inline"
          items={MenuItem}
        />
      </Sider>
      <Layout className="site-layout">
      <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >
                <div>
                  <Avatar
                    size="default"
                    style={{ marginRight: 10 }}
                    src={window.localStorage.getItem("avatar")}
                  ></Avatar>
                  {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <div className="div-createReceipt-headercontent">
            <Row>
              <Col md={1}></Col>
              <Col md={2}>
                <Button onClick={onSubmit}>Xác nhận</Button>
              </Col>
              <Col md={2}>
                <Button onClick={() => navigate(-1)}>Hủy Bỏ</Button>
              </Col>
              <Col md={2}>
                <Button onClick={createProduct}>Sản phẩm mới</Button>
              </Col>
            </Row>
          </div>
          <div className="div-createReceipt-bodycontent">
            <Form
              labelCol={{
                span: 5,
              }}
              wrapperCol={{
                span: 15,
              }}
            >
              <Form.Item label="Tên Kho">
                <Select
                  className="select-divison-consultingRoom"
                  showSearch
                  placeholder="Chọn Kho Nhập"
                  optionFilterProp="children"
                  onChange={(value) => setSelectedStoreHouse(value)}
                  filterOption={(input, option) =>
                    option.props.children
                      .toLowerCase()
                      .includes(input.toLowerCase()) ||
                    option.props.value
                      .toLowerCase()
                      .includes(input.toLowerCase())
                  }
                >
                  {allStoreHouse?.map((allStoreHouse) => (
                    <Select.Option key={allStoreHouse.storeHouseId}>
                      {allStoreHouse.storeHouseName}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item
                label="Hạn chốt đặt"
                rules={[
                  {
                    required: true,
                  },
                ]}
                style={{
                  display: "inline-block",
                  width: "calc(100% - 8px)",
                }}
                hasFeedback
              >
                <Space direction="vertical">
                  <DatePicker
                    style={{
                      width: "100%",
                      height: "38px",
                    }}
                    onChange={(value) => setexpectedDate(value)}
                    disabledDate={(d) =>
                      !d || d.isBefore(currentDateFormYYYYMMDD)
                    }
                    placeholder="Hạn chốt đặt"
                  />
                </Space>
              </Form.Item>
              <Form.Item label="Nhà Cung Cấp">
                <Select
                  className="select-divison-consultingRoom"
                  showSearch
                  placeholder="Chọn Nhà Cung Cấp"
                  optionFilterProp="children"
                  onChange={(value) => setSelectedSupplier(value)}
                  filterOption={(input, option) =>
                    option.props.children
                      .toLowerCase()
                      .includes(input.toLowerCase()) ||
                    option.props.value
                      .toLowerCase()
                      .includes(input.toLowerCase())
                  }
                >
                  {allSupplier?.map((allSupplier) => (
                    <Select.Option key={allSupplier.supplierId}>
                      {allSupplier.supplierName}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
            </Form>
            <Button type="primary" onClick={addNewRow} style={{
                  margin: "0px 0px 0px 50px",
                }}>Thêm 1 dòng</Button>
            <div className="container">
              <Table
                className="transfer-table"
                columns={renderColumn(allProduct, deleteRow, onChangeProduct, onChangeNumberRequest, onChangePrice)}
                dataSource={dataSource}
                pagination={false}/>
            </div>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const MenuItem = [
  getItem("Quản lý", "sub1", <ShopOutlined />, [
    getItem("QL Nhân viên", "1"),
    getItem("QL Phòng khám", "2"),
    getItem("Báo cáo phòng khám", "3"),
    getItem("QL Dịch vụ", "4"),
    // getItem("QL Phòng ban", "5"),
  ]),
  getItem("Quản lý Kho Hàng ", "sub2", <ShopOutlined />, [
    getItem("Nhập kho", "6"),
    getItem("Điều chuyển", "7"),
    getItem("Kiểm kê", "8"),
    // getItem("Báo cáo kho", "9"),
    getItem("Quản lý kho", "10"),
  ]),

];
const defaultRow = {
  productId: '',
  numberOfRequest: '',
  entryPrice: ''
}

const renderColumn = (listProduct, deleteRow, onChangeProduct, onChangeNumberRequest, onChangePrice) => ([
  {
    title: '',
    dataIndex: 'delete',
    key: 'delete',
    width: '5%',
    render: (_, row) => <Button className="btn-del-trans" onClick={() => deleteRow(row)} type="primary" icon={<MinusCircleOutlined />} danger />
  },
  {
    title: 'Sản Phẩm',
    dataIndex: 'productId',
    key: 'productId',
    width:' 10%',
    render: (_, row) => (
      <Select
        showSearch
        placeholder="Select Product"
        style={{ width: '100%' }}
        optionFilterProp="children"
        onChange={(val) => onChangeProduct(row, val)}
        filterOption={(input, option) =>
          (option?.label ?? "")
            .toLowerCase()
            .includes(input.toLowerCase())
        }
        options={listProduct}
      />
    )
  },
  {
    title: 'Số lượng',
    dataIndex: 'numberOfRequest',
    key: 'numberOfRequest',
    width:' 15%',
    render: (_, row) => <InputNumber min={1} onChange={(val) => onChangeNumberRequest(row, val)} className="input-transfer" />
  },
  {
    title: 'Đơn vị',
    dataIndex: 'unit',
    width:' 8%',
    key: 'unit',
    render: (_, record) => record.productId && listProduct.find(prod => prod.value === record.productId)?.unit
  },
  {
    title: 'Đơn giá',
    dataIndex: 'price',
    width: '15%',
    key: 'price',
    render: (_, row) => <InputNumber min={1} onChange={(val) => onChangePrice(row, val)} className="input-transfer" />
  },
  {
    title: 'Thành tiền',
    width: '20%',
    dataIndex: 'total',
    key: 'total',
    render: (_, record) => {
      if (record.productId) {
        return !record?.price || !record?.numberOfRequest ? 0 : 
        fortmatVND(record.price * record.numberOfRequest)
      }

      return 0;
    }
  },
])

const fortmatVND = (value) => {
  return parseFloat(value).toLocaleString('it-IT', { style: 'currency', currency: 'VND' }).replaceAll('.', ',')

}

export default CreateReceiptManager;
