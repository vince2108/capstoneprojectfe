import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  FormOutlined,
  DollarCircleOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import {
  Layout,
  Menu,
  Row,
  Col,
  Avatar,
  Select,
  Input,
  DatePicker,
  Button,
  Form,
  Modal,
  Dropdown,
  message,
} from "antd";
// import DatePicker from "react-datepicker"
import React, { Fragment, useEffect, useState } from "react";
import "./DetailReceive.css";
import { useLocation, useNavigate } from "react-router-dom";

import DoctorService from "../../service/DoctorService";
import MedicalServiceService from "../../service/MedicalServiceService";
import ConsultingRoomService from "../../service/ConsultingRoomService";
import moment from "moment";
function DetailReceive() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const { TextArea } = Input;
  const [accountId, setAccountId] = useState();
  const [pastMedicalHistory, setPastMedicalHistory] = useState();
  const [allergies, setAllergies] = useState();
  const [allService, setAllService] = useState([]);
  const [serviceState, setServiceState] = useState();
  const [consultingRoomState, setConsultingRoomState] = useState([]);
  const [setConsultingRoomIdState] = useState();
  const [medicalServiceIdState, setMedicalServiceIdState] = useState();
  const [form] = Form.useForm();
  const location = useLocation();
  const navigate = useNavigate();
  let newDate = new Date();
  let date = newDate.getDate();
  let month = newDate.getMonth() + 1;
  let year = newDate.getFullYear();
  let currentDateFormYYYYMMDD = year + "-" + month + "-" + date;

  const getConsultingRoombyService = async (medicalServiceId) => {
    setMedicalServiceIdState(medicalServiceId);
    const resdata = await ConsultingRoomService.getConsultingRoomByService(
      medicalServiceId
    );
    console.log(resdata.data.data);
    setConsultingRoomState(resdata.data.data);
    setConsultingRoomIdState(consultingRoomState);
  };

  useEffect(() => {
    const setDataMedicalService = async () => {
      const res = await MedicalServiceService.getAllMedicalService(
        localStorage.getItem("branchId")
      );
      setAllService(res.data.data);
    };
    setDataMedicalService();
  }, []);

  useEffect(() => {
    if (location.state.isOldPatient === true) {
      const setDataOfOldPatient = async () => {
        setAccountId(location.state.accountId);
        const res2 = await DoctorService.getPatientMedicalRecordById(
          location.state.accountId
        );
        form.setFieldsValue({
          fullName: res2.data.data.fullName,
          dateState: moment(res2.data.data.dob, "DD-MM-YYYY"),
          sex: res2.data.data.sex,
          job: res2.data.data.job,
          identityCard: res2.data.data.identityCard,
          address: res2.data.data.address,
          village: res2.data.data.village,
          district: res2.data.data.district,
          province: res2.data.data.province,
          phoneNumber: res2.data.data.phoneNumber,
          email: res2.data.data.email,
          ethnicity: res2.data.data.ethnicity,
        });
        setPastMedicalHistory(res2.data.data.pastMedicalHistory);
        setAllergies(res2.data.data.allergies);
      };
      setDataOfOldPatient();
    } else {
      console.log("Not Old Patient");
    }
  });


  const ReturnAction = () => {
    navigate(-1);
  };

  const SaveAction = async (values) => {
    let staffId = localStorage.getItem("idToken");
    let branchId = localStorage.getItem("branchId");
    console.log(values);
    console.log("1");
    let dateStateFrom = values.dateState.format("YYYY-MM-DD");
    console.log(dateStateFrom);
    console.log(consultingRoomState);
    if (Object.keys(consultingRoomState).length === 0) {
      Modal.warning({
        title: "Cảnh báo!",
        content: "Vui lòng chọn dịch vụ",
      });
    } else {
      if (location.state.isOldPatient === true) {
        const res = await DoctorService.updateExaminationUpdate(
          accountId,
          allergies,
          pastMedicalHistory,
          medicalServiceIdState,
          consultingRoomState[0].consultingRoomId,
          staffId,
          branchId,
          values.fullName,
          dateStateFrom,
          values.address,
          values.village,
          values.district,
          values.province,
          values.sex,
          values.identityCard,
          values.phoneNumber,
          values.ethnicity,
          values.job,
          values.email
        );
        navigate("/createPay", {
          replace: true,
          state: { accountId: accountId },
        });
      } else {
        const res = await DoctorService.startExaminationCreate(
          allergies,
          pastMedicalHistory,
          medicalServiceIdState,
          consultingRoomState[0].consultingRoomId,
          staffId,
          branchId,
          values.fullName,
          dateStateFrom,
          values.address,
          values.village,
          values.district,
          values.province,
          values.sex,
          values.identityCard,
          values.phoneNumber,
          values.ethnicity,
          values.job,
          values.email
        );
        console.log(res.data.status);
        navigate("/createPay", {
          replace: true,
          state: { accountId: res.data.data.accountId },
        });
      }
    }
  };

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };
  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];

  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key === 1) {
      navigate("/receive");
    }
    if (e.key === 2) {
      navigate("/pay");
    }
  };

  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo" />
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub1"]}
          selectedKeys={["1"]}
          mode="inline"
          items={Menuitems}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >
                <div>
                  <Avatar
                    size="default"
                    style={{ marginRight: 10 }}
                    src={window.localStorage.getItem("avatar")}
                  ></Avatar>
                  {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <div className="detail-receive-div-btn-save"></div>
          <Row style={{ margin: "20px 0px 0px 0px" }}>
            <Col md={16}>
              <Form
                onFinish={(values) => SaveAction(values)}
                form={form}
                labelCol={{
                  span: 4,
                }}
                wrapperCol={{
                  span: 16,
                }}
              >
                <Form.Item
                  style={{
                    display: location.state.isOldPatient ? "block" : "none",
                  }}
                  label="Mã BN:"
                >
                  <Input value={accountId} disabled={true}></Input>
                </Form.Item>
                <Form.Item
                  label="Tên BN:"
                  name="fullName"
                  rules={[
                    {
                      required: true,
                      whitespace: true,
                      message: "Vui lòng điền tên bệnh nhân!",
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  label="Ngày sinh"
                  name="dateState"
                  rules={[
                    {
                      required: true,
                      message: "Vui lòng chọn ngày sinh",
                    },
                  ]}
                  hasFeedback
                >
                  <DatePicker
                    style={{
                      width: "100%",
                      height: "38px",
                    }}
                    allowClear={true}
                    disabledDate={(d) =>
                      !d || d.isAfter(currentDateFormYYYYMMDD)
                    }
                  />
                </Form.Item>
                <Form.Item
                  label="Giới Tính"
                  name="sex"
                  rules={[
                    {
                      required: true,
                      whitespace: true,
                      message: "Vui lòng điền giới tính!",
                    },
                  ]}
                  hasFeedback
                >
                  <Input />
                </Form.Item>
                <Form.Item label="Nghề Nghiệp" name="job">
                  <Input />
                </Form.Item>
                <Form.Item
                  label="Số CMT/CCCD:"
                  name="identityCard"
                  rules={[
                    {
                      whitespace: true,
                      message: "Vui lòng điền CMT/CCCD!",
                    },
                  ]}
                  hasFeedback
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  label="Số Nhà:"
                  name="address"
                  rules={[
                    {
                      required: true,
                      whitespace: true,
                      message: "Vui lòng điền số nhà",
                    },
                  ]}
                  hasFeedback
                >
                  <Input></Input>
                </Form.Item>
                <Form.Item
                  label="Phường/Xã:"
                  name="village"
                  rules={[
                    {
                      required: true,
                      whitespace: true,
                      message: "Vui lòng điền Phường/Xã!",
                    },
                  ]}
                  hasFeedback
                >
                  <Input></Input>
                </Form.Item>
                <Form.Item
                  label="Q/Huyện:"
                  name="district"
                  rules={[
                    {
                      required: true,
                      whitespace: true,
                      message: "Vui lòng điền Q/Huyện!",
                    },
                  ]}
                  hasFeedback
                >
                  <Input></Input>
                </Form.Item>

                <Form.Item
                  label="Tỉnh/TP:"
                  name="province"
                  rules={[
                    {
                      required: true,
                      whitespace: true,
                      message: "Vui lòng điền tỉnh thành phố!",
                    },
                  ]}
                  hasFeedback
                >
                  <Input></Input>
                </Form.Item>
                <Form.Item
                  label="Điện Thoại"
                  name="phoneNumber"
                  rules={[
                    {
                      required: true,
                      whitespace: true,
                      message: "Vui lòng điền số điện thoại!",
                      // type: "regexp",
                      pattern: /(84|0[2|3|5|7|8|9])+([0-9]{8,9})\b/g,
                      // min: 9,
                      // max: 11,
                    },
                  ]}
                  hasFeedback
                >
                  <Input></Input>
                </Form.Item>
                <Form.Item
                  label="Email"
                  name="email"
                  rules={[
                    {
                      whitespace: true,
                      type: "email",
                      message: "email nhập sai",
                    },
                  ]}
                >
                  <Input></Input>
                </Form.Item>
                <Form.Item label="Dân tộc" name="ethnicity">
                  <Input />
                </Form.Item>

                <Form.Item label="Tiền Sử:">
                  <TextArea
                    rows={2}
                    value={pastMedicalHistory}
                    onChange={(event) =>
                      setPastMedicalHistory(event.target.value)
                    }
                  />
                </Form.Item>
                <Form.Item label="Dị Ứng:">
                  <TextArea
                    rows={2}
                    value={allergies}
                    onChange={(event) => setAllergies(event.target.value)}
                  />
                </Form.Item>
                <div>
                  <Row>
                    <Col md={19}></Col>
                    <Col md={3}>
                      <Button type="primary" htmlType="submit">
                        Lưu
                      </Button>
                    </Col>
                    <Col md={2}>
                      <Button onClick={ReturnAction}>Hủy</Button>
                    </Col>
                  </Row>
                </div>
              </Form>
            </Col>
            <Col md={8}>
              <div>
                <Form.Item label="Dịch vụ">
                  <Select>
                    {allService?.map((service) => {
                      return (
                        <select
                          onChange={(event) =>
                            setServiceState(event.target.value)
                          }
                          value={serviceState}
                          key={service.medicalServiceId}
                        >
                          <option
                            onClick={() =>
                              getConsultingRoombyService(
                                service.medicalServiceId
                              )
                            }
                          >
                            {service.serviceName}
                          </option>
                        </select>
                      );
                    })}
                  </Select>
                </Form.Item>
                {consultingRoomState.map((consultingRoomStateData) => {
                  return (
                    <Fragment>
                      <Form.Item label="Thuộc Phòng Khám: ">
                        <Input
                          value={consultingRoomStateData?.roomName}
                          onChange={(event) =>
                            setServiceState(event.target.value)
                          }
                        ></Input>
                      </Form.Item>
                    </Fragment>
                  );
                })}
              </div>
            </Col>
          </Row>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const Menuitems = [
  getItem("Tiếp Đón", "sub1", <FormOutlined />, [getItem("Tiếp Đón", "1")]),
  getItem("Thanh Toán", "sub2", <DollarCircleOutlined />, [
    getItem("Thanh Toán", "2"),
  ]),
];

export default DetailReceive;
