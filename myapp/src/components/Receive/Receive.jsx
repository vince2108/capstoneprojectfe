import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  DollarCircleOutlined,
  UserOutlined,
  FormOutlined,
  HomeOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import {
  Layout,
  Menu,
  Row,
  Col,
  Avatar,
  Button,
  message,
  Dropdown,
  Space,
} from "antd";
//import React, { useState } from "react";
import "./Receive.css";
import { AppstoreOutlined } from "@ant-design/icons";
import { Link, useNavigate } from "react-router-dom";
//import { AudioOutlined } from '@ant-design/icons';
import { Input } from "antd";
import { Select } from "antd";
import React, { useState, useEffect, useRef, Fragment } from "react";
import PatientService from "../../service/PatientService";
import ConsultingRoomService from "../../service/ConsultingRoomService";
import DoctorService from "../../service/DoctorService";
function Receive() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");
  const [patientf, setPatientFState] = useState([]);
  const [patientT, setPatientTState] = useState([]);
  const [allConsultingRoom, setAllConsultingRoom] = useState([]);
  const [consultingRoomState, setConsultingRoomState] = useState();
  const [searchValue, setSerchValue] = useState();
  const [ErrorMessage,setErrorMessage] = useState();
  const typingTimeoutRef = useRef(null);
  const navigate = useNavigate();

  let newDate = new Date();
  let date = newDate.getDate();
  let month = newDate.getMonth() + 1;
  let year = newDate.getFullYear();
  const currentDate = date + "/" + month + "/" + year;

  useEffect(() => {
    const getAllConsultingRoom = async () => {
      const res = await ConsultingRoomService.getAllConsultingRoom(localStorage.getItem("branchId"));
      setAllConsultingRoom(res.data.data);
    };
    getAllConsultingRoom();
  }, []);

  useEffect(() => {
    const getAllPatientT = async () => {
      const res = await PatientService.getAllPatientStatusTrue(localStorage.getItem("branchId"));
      setPatientTState(res.data.data);
      console.log(res.data.data);
    };
    getAllPatientT();
  }, []);

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };

  const checkInForOldPatient = (accountId) => {
    console.log(accountId);

    navigate("/detailReceive", {
      state: { accountId: accountId, isOldPatient: true },
    });
  };

  const getPatientTByConsultingRoomAction = async (consultingRoomId) => {
    // console.log("kkkkkkkk" + consultingRoomId)
    const resdata = await DoctorService.getPatientByConsultingRoom(
      consultingRoomId, localStorage.getItem("branchId")
    );
    setPatientTState(resdata.data.data);
    // console.log(resdata.data.data)
  };

  const checkInAction = () => {
    navigate("/detailReceive", { state: { isOldPatient: false } });
  };

  const onClick = (e) => {
    console.log("click ", e);
    if (e.key == 1) {
      navigate("/receive");
    }
    if (e.key == 2) {
      navigate("/pay");
    }
  };

  const searchPatientByNameFun = async () => {
    console.log(searchValue);
    // const resT = await PatientService.getPatientByNameAndStatus(
    //   searchValue,
    //   true
    // );
    
    const resF = await PatientService.getPatientByNameAndStatus(
      searchValue,
      false
    );
    if (searchValue === undefined || searchValue === "") {
      setErrorMessage("Vui lòng điền tên để tìm kiếm");
      document.getElementById("this-dialog").showModal();
      return;
    }
    console.log(resF.data);
    if (resF.data.status === "Success") {
      if (resF.data.message === "No Data") {
        setErrorMessage(
          "Không tìm thấy hồ sơ bênh nhân nào có tên " + searchValue
        );
        document.getElementById("this-dialog").showModal();
        return;
      }
    }
    if (resF.data != null) {
      setPatientFState(resF.data.data);
    }
  };

  const getAllPatient = async ()=>{
    const res = await PatientService.getAllPatientStatusTrue(localStorage.getItem("branchId"));
      setPatientTState(res.data.data);
  }

  const ConfirmAciton = () => {
    document.getElementById("this-dialog").close();
  };

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };
  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];

  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo" />
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub1"]}
          selectedKeys={["1"]}
          mode="inline"
          items={Menuitems}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
            <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >  
                <div>
                <Avatar
                  size="default"
                  style={{ marginRight: 10 }}
                  src={window.localStorage.getItem("avatar")}
                ></Avatar>
                {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <dialog id="this-dialog">
            <p> {ErrorMessage}</p>
            <button onClick={() => ConfirmAciton()}>Đóng</button>
          </dialog>
          <div>
            <Row>
              <Col md={4}>
                {" "}
                <h1>DANH SÁCH</h1>{" "}
              </Col>
              <Col md={8}></Col>
              <Col md={10}>
                <div>
                  <Input
                    placeholder="Tên bệnh nhân"
                    style={{ width: "75%" }}
                    value={searchValue}
                    onChange={(event) => setSerchValue(event.target.value)}
                  ></Input>
                  <Button onClick={() => searchPatientByNameFun()}>Tìm</Button>
                </div>
              </Col>

              <Col md={2}>
                <Button
                  shape="round"
                  style={{
                    backgroundColor: "#39afd3",
                    color: "#e1eff0",
                  }}
                  onClick={checkInAction}
                >
                  Đăng kí
                </Button>
              </Col>
            </Row>
          </div>

          <div class="container">
          {patientf?.map((patient) => {
              return (
                <table className="tab1" key={patient.accountId}>
                  <thead>
                    <th>Mã BN</th>
                    <th>Tên Bệnh Nhân</th>
                    <th>Ngày Sinh</th>
                    <th>Số Điện Thoại</th>
                    <th>Giới Tính</th>
                    <th>Nơi ở</th>
                    <th>Action</th>
                  </thead>
                  <tbody>
                    <tr>
                      <td data-label="col-1">
                        {patient != null ? "BN" + patient.accountId : " "}
                      </td>
                      <td data-label="col-2">
                        {patient != null ? patient.fullName : " "}
                      </td>
                      <td data-label="col-3">
                        {patient != null ? patient.dob : " "}
                      </td>
                      <td data-label="col-4">
                        {patient != null ? patient.phoneNumber : " "}
                      </td>
                      <td data-label="col-5">
                        {patient != null ? patient.sex : " "}
                      </td>
                      <td data-label="col-6">
                        {patient != null
                          ? patient.address +
                            ", " +
                            patient.village +
                            ", " +
                            patient.district +
                            ", " +
                            patient.province
                          : " "}
                      </td>
                      <td data-label="col-7">
                        <Button
                          onClick={() =>
                            checkInForOldPatient(patient.accountId)
                          }
                        >
                          Tiếp Nhận
                        </Button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              );
            })}
          </div>
          <div className="rececive-div2" style={{ margin: "20px 0px 0px 0px" }}>
          <h1 style={{marginTop: "10px"}}>DANH SÁCH BỆNH NHÂN ĐANG KHÁM BỆNH</h1>
            <Row style={{ margin: "10px 0px 0px 0px" }}>
            <Col md={2}></Col>
              <Col md={2} onClick={() => getAllPatient()}><Button>Tất cả</Button></Col>
              <Col md={10}>
              <div style={{marginTop: "15px"}}>
              {" "}
                Phòng Khám:
                <Select
                  style={{
                    width: "30vh",
                    marginLeft: "10px",
                  }}
                >
                  {allConsultingRoom?.map((consultingRoom) => {
                    return (
                      <select
                        onChange={(event) =>
                          setConsultingRoomState(event.target.value)
                        }
                        value={consultingRoomState}
                        key={consultingRoom.consultingRoomId}
                      >
                        <option
                          onClick={() =>
                            getPatientTByConsultingRoomAction(
                              consultingRoom.consultingRoomId
                            )
                          }
                        >
                          {consultingRoom.roomName}
                        </option>
                      </select>
                    );
                  })}
                </Select>
              </div>
              
              </Col>
              <Col md={4}></Col>
              <Col md={5} style={{marginTop: "15px"}}>
                {" "}
                Ngày:
                <Input
                  style={{
                    width: "180px",
                    height: "38px",
                    marginLeft: "10px",
                  }}
                  // format={"DD/MM/YYYY"}
                  value={currentDate}
                />
              </Col>
            </Row>
          </div>

          {/* <Table columns={} dataSource={} />; */}
          <div class="container">
            <table className="tab1">
              <thead>
                <th>Mã BN</th>
                <th>Tên Bệnh Nhân</th>
                <th>Ngày Sinh</th>
                <th>Số Điện Thoại</th>
                <th>Giới Tính</th>
                <th>Nơi ở</th>
              </thead>
              <tbody>
                {patientT?.map((patient) => {
                  return (
                    <tr key={patient.accountId}>
                      <td data-label="col-1">
                        {patient != null ? "BN" + patient.accountId : " "}
                      </td>
                      <td data-label="col-2">{patient?.fullName}</td>
                      <td data-label="col-3">{patient?.dob}</td>
                      <td data-label="col-4">{patient?.phoneNumber}</td>
                      <td data-label="col-5">{patient?.sex}</td>
                      <td data-label="col-3">
                        {patient != null
                          ? patient.address +
                            ", " +
                            patient.village +
                            ", " +
                            patient.district +
                            ", " +
                            patient.province
                          : " "}
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const Menuitems = [
  getItem("Tiếp Đón", "sub1", <FormOutlined />, [getItem("Tiếp Đón", "1")]),
  getItem("Thanh Toán", "sub2", <DollarCircleOutlined />, [
    getItem("Thanh Toán", "2"),
  ]),
];

export default Receive;
