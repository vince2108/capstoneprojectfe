import React from "react";
import { Navigate, Outlet } from "react-router-dom";

export default function ProtectedRoutes(){
    let isLogin = localStorage.getItem("loginToken")
     console.log(isLogin)
    return(
        isLogin != true ? <Navigate to="/" />: <Outlet/>
    )
}