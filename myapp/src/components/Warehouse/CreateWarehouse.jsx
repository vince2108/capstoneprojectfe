import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  ShopOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { Layout, Menu, Row, Col, Avatar, Button,message, Dropdown } from "antd";
import React, { useState , useEffect} from "react";
import "./CreateWarehouse.css";
import StoreHouse from "../../service/StoreHouse";
import { Form, Input } from "antd";
import { Link, useLocation, useNavigate } from "react-router-dom";

function CreateWarehouse() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");
  const [storeHouseName, setStoreHouseName]= useState()
  const [storeHouseId,setStoreHouseId]= useState()
  const [oldStorehouse, setOldStorehouse]= useState()
  const [abbreviationName,setAbbreviatonName]= useState()
  const [address, setAddress]= useState()
  const [locationWarehouse, setLocationWareHouse]= useState()
  const [loading, setLoading] = useState(false);
  const location = useLocation();
  const navigate = useNavigate();
  const isAction = true

  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/staff");
    }
    if (e.key == 2) {
      navigate("/clinic");
    }
    if (e.key == 3) {
      navigate("/report");
    }
    if (e.key == 4) {
      navigate("/service");
    }
    if (e.key == 5) {
      navigate("/divison");
    }
    if (e.key == 6) {
      navigate("/listReceiptManager");
    }
    if (e.key == 7) {
      navigate("/ListTransfer")
    }
    if (e.key == 8) {
      navigate("/inventoryManager")
    }
    if (e.key == 9) {
      // navigate("/inventoryManager")
    }
    if (e.key == 10) {
      navigate("/warehouse")
    }
  };
  useEffect(()=>{
    if(location.state.isOldStorehouse=== true){
      const setDataForOldStorehouse = async()=>{
       
        const resOldData = await StoreHouse.getStoreHouseById(location.state.storehouseId)
        setOldStorehouse(resOldData.data.data)
        console.log(resOldData.data.data)
        setLoading(true)
        setDataForPage()

      }
      setDataForOldStorehouse()

    }else{
      console.log("Not Old store");
    }
  },[loading])

  const setDataForPage = ()=>{
    setStoreHouseId(oldStorehouse.storeHouseId)
    setStoreHouseName(oldStorehouse.storeHouseName)
    setAbbreviatonName(oldStorehouse.abbreviationName)
    setAddress(oldStorehouse.address)
    setLocationWareHouse(oldStorehouse.location)
  }
  const ReturnAction = () => {
    navigate(-1);
  };
  const SaveAction = async()=>{
    const res = await StoreHouse.createStoreHouse(
    
      storeHouseName,
      abbreviationName,
      address,
      localStorage.getItem("idToken"),
      localStorage.getItem("branchId"),
      locationWarehouse,
      isAction
    )
    console.log( localStorage.getItem("accountId"))
    navigate("/warehouse")
  }

  const SaveData = async()=>{
    const resUpdate = await StoreHouse.updateStorehouse(
      storeHouseId,
      storeHouseName,
      abbreviationName,
      address,
      localStorage.getItem("idToken"),
      localStorage.getItem("branchId"),
      locationWarehouse,
      isAction
      )
      console.log(resUpdate.data.data)
      navigate("/warehouse")
  }
  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };  

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];

  return (
    <Layout>
    <Sider trigger={null} collapsible collapsed={collapsed}>
      <div className="logo"> </div>
      <Menu
        theme={theme}
        onClick={onClick}
        defaultOpenKeys={["sub2"]}
        selectedKeys={["10"]}
        mode="inline"
        items={MenuItem}
      />
    </Sider>
    <Layout className="site-layout">
    <Header
        className="site-layout-background"
        style={{
          padding: 0,
        }}
      >
        <Row>
          <Col md={20}>
            {React.createElement(
              collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
              {
                className: "trigger",
                onClick: () => setCollapsed(!collapsed),
              }
            )}
          </Col>
          <Col md={4}>
            <Dropdown
              menu={{
                items,
                onClick: handleMenuClick,
              }}
              placement="bottomLeft"
            >
              <div>
                <Avatar
                  size="default"
                  style={{ marginRight: 10 }}
                  src={window.localStorage.getItem("avatar")}
                ></Avatar>
                {window.localStorage.getItem("usernameToken")}
              </div>
            </Dropdown>
          </Col>
        </Row>
      </Header>
      <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
      
          <div style={{ margin: "20px 0px 0px 0px" }}>
            <Form
            onFinish={(values) => {if (location.state.isOldBranch===true) {
              SaveData(values)
              
            } else {
              SaveAction(values)
            }}}
              labelCol={{
                span: 4,
              }}
              wrapperCol={{
                span: 14,
              }}
              layout="horizontal"
            >
                     <Form.Item>
                <Row>
                  <Col md={1}></Col>
                  <Col md={5}>
                    <Button
                      type="primary"
                      htmlType="submit"
                    >
                      Lưu
                    </Button>
                  </Col>
                  <Col md={2}>
                    <Button onClick={ReturnAction}>Hủy</Button>
                  </Col>
                </Row>
              </Form.Item>
              <Form.Item label="Tên Kho" label-size="20x"
               name="storeHouseName"

               rules={[
                {
                  required: true,
                  message: 'Vui Lòng Điền Tên Kho',
                },
              ]}
              
              hasFeedback>
                <Input value={storeHouseName}
                  onChange={(event) => setStoreHouseName(event.target.value)}></Input>
              </Form.Item>
              <Form.Item label="Tên Viết Tắt" label-size="20px"
               name="abbreviationName"

               rules={[
                {
                  required: true,
                  message: 'Vui Lòng Điền Tên Viết Tắt',
                },
              ]}
              
              hasFeedback>
                <Input value={abbreviationName}
                  onChange={(event) => setAbbreviatonName(event.target.value)}></Input>
              </Form.Item>
              <Form.Item label="Địa Điểm" label-size="20px"
               name="address"

               rules={[
                {
                  required: true,
                  message: 'Vui Lòng Điền Địa điểm',
                },
              ]}
              
              hasFeedback>
                <Input value={address}
                  onChange={(event) => setAddress(event.target.value)}></Input>
              </Form.Item>

              <Form.Item label="Địa Chỉ" label-size="20px"
               name="locationWarehouse"

               rules={[
                {
                  required: true,
                  message: 'Vui Lòng Điền Địa Chỉ',
                },
              ]}
              
              hasFeedback>
                <Input value={locationWarehouse}
                  onChange={(event) => setLocationWareHouse(event.target.value)}></Input>
              </Form.Item>
            </Form>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const MenuItem = [
  getItem("Quản lý", "sub1", <ShopOutlined />, [
    getItem("QL Nhân viên", "1"),
    getItem("QL Phòng khám", "2"),
    getItem("Báo cáo phòng khám", "3"),
    getItem("QL Dịch vụ", "4"),
    // getItem("QL Phòng ban", "5"),
  ]),
  getItem("Quản lý Kho Hàng ", "sub2", <ShopOutlined />, [
    getItem("Nhập kho", "6"),
    getItem("Điều chuyển", "7"),
    getItem("Kiểm kê", "8"),
    // getItem("Báo cáo kho", "9"),
    getItem("Quản lý kho", "10"),
  ]),
];

export default CreateWarehouse;