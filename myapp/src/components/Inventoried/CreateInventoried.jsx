import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  ShopOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { Layout, Menu, Row, Col, Avatar, Button ,message, Dropdown} from "antd";
import React, { useEffect, useState } from "react";
import "./CreateInventoried.css";
import { Form, Space, Select, Input, DatePicker } from "antd";
import StoreHouse from "../../service/StoreHouse";
import { useNavigate } from "react-router-dom";
import InventoryService from "../../service/InventoryService";

function CreateInventoried() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");
  const [selectedStoreHouse, setSelectedStoreHouse] = useState();
  const [allStoreHouse, getAllStoreHouse] = useState([]);
  const [expectedDate, setexpectedDate] = useState();
  let newDate = new Date();
  let date = newDate.getDate() + 1;
  let month = newDate.getMonth() + 1;
  let year = newDate.getFullYear();
  const currentDate = date + "/" + month + "/" + year;
  let currentDateFormYYYYMMDD = year + "-" + month + "-" + date;
  const navigate = useNavigate();
  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/staff");
    }
    if (e.key == 2) {
      navigate("/clinic");
    }
    if (e.key == 3) {
      navigate("/report");
    }
    if (e.key == 4) {
      navigate("/service");
    }
    if (e.key == 5) {
      navigate("/divison");
    }
    if (e.key == 6) {
      navigate("/listReceiptManager");
    }
    if (e.key == 7) {
      navigate("/ListTransfer")
    }
    if (e.key == 8) {
      navigate("/inventoryManager")
    }
    if (e.key == 9) {
      // navigate("/inventoryManager")
    }
    if (e.key == 10) {
      navigate("/warehouse")
    }
  };

  useEffect(() => {
    const setDataForPage = async () => {
      const res2 = await StoreHouse.getAllStoreHouse();
      getAllStoreHouse(res2.data.data);
    };
    setDataForPage();
  }, []);

  const BackActionFun = async () => {
    navigate(-1);
  };

  const SaveAction = async ()=>{
    const res = await InventoryService.createInventory(selectedStoreHouse,localStorage.getItem("idToken"),expectedDate);
    navigate("/inventoryManager",{replace: true});
  }
  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };  

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];

  return (
    <Layout>
    <Sider trigger={null} collapsible collapsed={collapsed}>
      <div className="logo"> </div>
      <Menu
        theme={theme}
        onClick={onClick}
        defaultOpenKeys={["sub2"]}
        selectedKeys={["8"]}
        mode="inline"
        items={MenuItem}
      />
    </Sider>
    <Layout className="site-layout">
    <Header
        className="site-layout-background"
        style={{
          padding: 0,
        }}
      >
        <Row>
          <Col md={20}>
            {React.createElement(
              collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
              {
                className: "trigger",
                onClick: () => setCollapsed(!collapsed),
              }
            )}
          </Col>
          <Col md={4}>
            <Dropdown
              menu={{
                items,
                onClick: handleMenuClick,
              }}
              placement="bottomLeft"
            >
              <div>
                <Avatar
                  size="default"
                  style={{ marginRight: 10 }}
                  src={window.localStorage.getItem("avatar")}
                ></Avatar>
                {window.localStorage.getItem("usernameToken")}
              </div>
            </Dropdown>
          </Col>
        </Row>
      </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <div style={{ margin: "20px 0px 0px 0px" }}>
            <Form
              labelCol={{
                span: 4,
              }}
              wrapperCol={{
                span: 14,
              }}
              layout="horizontal"
            >
              <Form.Item label="Tên Kho">
                <Select
                  className="select-divison-consultingRoom"
                  showSearch
                  placeholder="Chọn Kho Nhập"
                  optionFilterProp="children"
                  onChange={(value) => setSelectedStoreHouse(value)}
                  filterOption={(input, option) =>
                    option.props.children
                      .toLowerCase()
                      .includes(input.toLowerCase()) ||
                    option.props.value
                      .toLowerCase()
                      .includes(input.toLowerCase())
                  }
                >
                  {allStoreHouse?.map((allStoreHouse) => (
                    <Select.Option key={allStoreHouse.storeHouseId}>
                      {allStoreHouse.storeHouseName}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>

              <Form.Item
                label="Ngày Dự Kiến"
                rules={[
                  {
                    required: true,
                  },
                ]}
                style={{
                  display: "inline-block",
                  width: "calc(100% - 8px)",
                }}
                hasFeedback
              >
                <Space direction="vertical">
                  <DatePicker
                    style={{
                      width: "100%",
                      height: "38px",
                    }}
                    onChange={(value) => setexpectedDate(value)}
                    disabledDate={(d) =>
                      !d || d.isBefore(currentDateFormYYYYMMDD)
                    }
                    placeholder="Hạn chốt đặt"
                  />
                </Space>
              </Form.Item>
            </Form>

          </div>
          <div>
            <Row>
            <Col md={4}></Col>
              <Col md={2}>
                <Button onClick={() => SaveAction()}>Tạo</Button>{" "}
              </Col>
              <Col md={2}>
                <Button onClick={() => BackActionFun()}>Hủy Bỏ</Button>
              </Col>
            </Row>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const MenuItem = [
  getItem("Quản lý", "sub1", <ShopOutlined />, [
    getItem("QL Nhân viên", "1"),
    getItem("QL Phòng khám", "2"),
    getItem("Báo cáo phòng khám", "3"),
    getItem("QL Dịch vụ", "4"),
    // getItem("QL Phòng ban", "5"),
  ]),
  getItem("Quản lý Kho Hàng ", "sub2", <ShopOutlined />, [
    getItem("Nhập kho", "6"),
    getItem("Điều chuyển", "7"),
    getItem("Kiểm kê", "8"),
    // getItem("Báo cáo kho", "9"),
    getItem("Quản lý kho", "10"),
  ]),

];

export default CreateInventoried;
