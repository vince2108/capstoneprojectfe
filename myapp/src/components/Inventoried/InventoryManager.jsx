import "./Inventoried.css";
import {
  MenuFoldOutlined,
  ShopOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { Layout, Menu, Row, Col, Avatar, Button, message, Dropdown, Select } from "antd";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import InventoryService from "../../service/InventoryService";

function InventoryManager() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");
  const [allInventoryList, setAllInventoryList] = useState([]);
  const navigate = useNavigate();
  const onChange = async (value) => {
    console.log(value)
    if(value==="0"){
      const res = await InventoryService.getAllInventoryByBranch(
        localStorage.getItem("branchId")
      );
      setAllInventoryList(res.data.data);
    } else {
      const res = await InventoryService.searchInventoryByBranchAndStatus(
        localStorage.getItem("branchId"), value
      )
      console.log(res.data.data)
      setAllInventoryList(res.data.data);
    }
  };

  useEffect(() => {
    const setDataForPage = async () => {
      const res = await InventoryService.getAllInventoryByBranch(
        localStorage.getItem("branchId")
      );
      setAllInventoryList(res.data.data);
      console.log(res.data.data);
    };
    setDataForPage();
  }, []);


  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/staff");
    }
    if (e.key == 2) {
      navigate("/clinic");
    }
    if (e.key == 3) {
      navigate("/report");
    }
    if (e.key == 4) {
      navigate("/service");
    }
    if (e.key == 5) {
      navigate("/divison");
    }
    if (e.key == 6) {
      navigate("/listReceiptManager");
    }
    if (e.key == 7) {
      navigate("/ListTransfer")
    }
    if (e.key == 8) {
      navigate("/inventoryManager")
    }
    if (e.key == 9) {
      // navigate("/inventoryManager")
    }
    if (e.key == 10) {
      navigate("/warehouse")
    }
  };
  const createInventoryManager = () => {
    navigate("/createInventoried");
  };

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };  

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];

  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo"> </div>
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub2"]}
          selectedKeys={["8"]}
          mode="inline"
          items={MenuItem}
        />
      </Sider>
      <Layout className="site-layout">
      <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >
                <div>
                  <Avatar
                    size="default"
                    style={{ marginRight: 10 }}
                    src={window.localStorage.getItem("avatar")}
                  ></Avatar>
                  {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <div className="div-iv">
            <Row>
              <Col md={3}>
                <Button className="btn-tf" onClick={createInventoryManager}>
                  Tạo
                </Button>
              </Col>
              <Col md={15}></Col>
              <Col md={6}>
                Sắp xếp: &nbsp;
                <Select
                  className="select-status-listreceipt"
                  showSearch
                  defaultValue={"Tất cả"}
                  placeholder="chọn trạng thái"
                  optionFilterProp="children"
                  onChange={onChange}
                  filterOption={(input, option) =>
                    (option?.label ?? "")
                      .toLowerCase()
                      .includes(input.toLowerCase())
                  }
                  options={[
                    {
                      value: "0",
                      label: "Tất Cả",
                    },
                    {
                      value: "1",
                      label: "Sẵn Sàng",
                    },
                    {
                      value: "2",
                      label: "Hoàn thành",
                    },
                  ]}
                />
              </Col>
            </Row>
          </div>
          <div class="container">
            <table className="tab1">
              <thead>
                <th>Mã phiếu</th>
                <th>Ngày kiểm kê</th>
                <th>Tên kho</th>
                <th>Trạng thái</th>
              </thead>
              <tbody>
                {allInventoryList?.map((inventory) => {
                  return (
                    <tr key={inventory.inventoryId}>
                      <td data-label="col-1">{"KK"+inventory.inventoryId}</td>
                      <td data-label="col-2">{inventory.date}</td>
                      <td data-label="col-3">{inventory.storeHouseName}</td>
                      <td data-label="col-5">{inventory.statusName}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const MenuItem = [
  getItem("Quản lý", "sub1", <ShopOutlined />, [
    getItem("QL Nhân viên", "1"),
    getItem("QL Phòng khám", "2"),
    getItem("Báo cáo phòng khám", "3"),
    getItem("QL Dịch vụ", "4"),
    // getItem("QL Phòng ban", "5"),
  ]),
  getItem("Quản lý Kho Hàng ", "sub2", <ShopOutlined />, [
    getItem("Nhập kho", "6"),
    getItem("Điều chuyển", "7"),
    getItem("Kiểm kê", "8"),
    // getItem("Báo cáo kho", "9"),
    getItem("Quản lý kho", "10"),
  ]),

];

export default InventoryManager;
