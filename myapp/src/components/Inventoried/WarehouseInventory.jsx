import "./WarehouseInventory.css";
import {
  MenuFoldOutlined,
  ShopOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  MinusCircleOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import {
  Layout,
  Menu,
  Row,
  Col,
  Avatar,
  Select,
  Input,
  Form,
  Space,
  DatePicker,
  Button,
  Table,
  InputNumber,
  message,
  Dropdown,
} from "antd";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import InventoryService from "../../service/InventoryService";
import ProductService from "../../service/ProductService";
import { cloneDeep, isEmpty } from "lodash";
import { showError, showSuccess } from "../../utils";
import moment from "moment";

const dateFormatList = ["YYYY-MM-DD"];

function WarehouseInventory() {
  const { id } = useParams();
  const navigate = useNavigate();
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");

  const [dataSource, setDataSource] = useState([]);
  const [indexRow, setIndexRow] = useState(0);
  const [listProduct, setListProduct] = useState([]);
  const [detail, setDetail] = useState(null);
  const [isDone, setIsDone] = useState(false);
  const [dateInventory, setDateInventory] = useState(null);

  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/overview");
    }
    if (e.key == 2) {
      navigate("/listTransfer");
    }
    if (e.key == 3) {
      navigate("/inventoryWHstaff");
    }
    if (e.key == 4) {
      navigate("/listReceiptWHstaff");
    }
    if (e.key == 5) {
      navigate("/listIssueWHstaff");
    }
    if (e.key == 6) {
      navigate("/productGroups");
    }
    if (e.key == 7) {
      navigate("/unitGroup");
    }
    if (e.key == 8) {
      navigate("/unit");
    }
    if (e.key == 9) {
      navigate("/product");
    }
  };

  const onChangeDate = (date, dateString) => {
    setDateInventory(dateString);
  };

  const onChangeProduct = (row, value) => {
    const newData = cloneDeep(dataSource);
    const index = newData.findIndex((item) => item.id === row.id);
    if (index !== -1) {
      newData[index].idProduct = value;
      setDataSource(newData);
    }
  };

  const onChangeNumberRequest = (row, value) => {
    const newData = cloneDeep(dataSource);
    const index = newData.findIndex((item) => item.id === row.id);
    if (index !== -1) {
      newData[index].actualQuantity = value;
      setDataSource(newData);
    }
  };

  const onChangeNote = (row, value) => {
    const newData = cloneDeep(dataSource);
    const index = newData.findIndex((item) => item.id === row.id);
    if (index !== -1) {
      newData[index].note = value;
      setDataSource(newData);
    }
  };

  const addNewRow = (row = defaultRow) => {
    const newData = cloneDeep(dataSource);
    newData.push({
      ...row,
      id: indexRow + 1,
    });
    setDataSource(newData);
    setIndexRow(indexRow + 1);
  };

  const deleteRow = (row) => {
    const newData = cloneDeep(dataSource);
    const index = newData.findIndex((item) => item.id === row.id);
    if (index !== -1) {
      newData.splice(index, 1);
      setDataSource(newData);
    }
  };

  const fetchProduct = async () => {
    const {
      data: { data, message },
    } = await ProductService.getAllProduct();
    if (message === "MSG_SUCCESS") {
      const dataQuantity = await Promise.all(
        data.map((item) =>
          ProductService.getProductQuantity({
            idStoreHouse: detail?.storeHouseId,
            idProduct: item.productId,
          })
        )
      );
      setListProduct(
        data.map((item, index) => ({
          value: item.productId,
          label: item.productName,
          unit: item.unitName,
          quantity: dataQuantity[index]?.data?.data?.quantity || 0,
        }))
      );
    }
  };

  const fetchDetail = async () => {
    let res;
    let statusDone = false;
    const { data } = await InventoryService.getDetail(id);
    res = data;
    if (Number(res.data?.statusId) === 2) {
      statusDone = true;
      const {
        data: { data: listDataDone },
      } = await InventoryService.getDetailDone(id);
      listDataDone.forEach((item) => {
        addNewRow({
          ...item,
          idProduct: item.productID,
        });
      });
      const newData = listDataDone.map((item, idx) => ({
        ...item,
        idProduct: item.productID,
        id: idx,
      }));
      setDataSource(newData);
      setIndexRow(listDataDone.length);
    }
    res.data.inventoryDate && setDateInventory(res.data.inventoryDate)
    setDetail(res.data);
    setIsDone(statusDone);
  };

  const onSubmit = async () => {
    let hasError = false;
    let errorDuplicate = false;
    let productIds = [];
    if (dataSource.length === 0) {
      showError("Hãy thêm ít nhất 1 sản phẩm để chuyển.");
      return;
    }
    dataSource.forEach((item) => {
      if (!item.idProduct || !item.actualQuantity) hasError = true;
      if (productIds.includes(item.idProduct)) {
        errorDuplicate = true;
      }
      productIds.push(item.idProduct);
    });
    if (!dateInventory) {
      hasError = true;
    }
    if (hasError) {
      showError("Hãy nhập đủ các trường.");
      return;
    }
    if (errorDuplicate) {
      showError("Không được chọn cùng 1 sản phẩm trên nhiều dòng.");
      return;
    }
    try {
      const body = {
        idInventory: id,
        inventoryDate: dateInventory,
        idPersonInCharge: localStorage.getItem("idToken"),
        productsInventoryRequestList: dataSource.map(
          ({ idProduct, actualQuantity, note }) => {
            const product = listProduct.find(
              (item) => item.value === idProduct
            );

            return {
              idProduct,
              quantityStock: product.quantity,
              actualQuantity,
              deviant: Math.abs(product.quantity - actualQuantity),
              note,
            };
          }
        ),
      };
      const { data } = await InventoryService.finishInventory(body);
      if (data?.status && data.status === "Error") {
        throw new Error();
      }
      showSuccess();
      navigate("/inventoryWHstaff");
    } catch (error) {
      console.log(error);
      showError("Tạo thất bại!");
    }
  };

  useEffect(() => {
    if (!isEmpty(detail)) {
      fetchProduct();
    }
  }, [detail]);

  useEffect(() => {
    fetchDetail();
  }, [id]);

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];
  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo"> </div>
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub1"]}
          selectedKeys={["3"]}
          mode="inline"
          items={MenuItem}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >
                <div>
                  <Avatar
                    size="default"
                    style={{ marginRight: 10 }}
                    src={window.localStorage.getItem("avatar")}
                  ></Avatar>
                  {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          overfl
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <div className="div-wareHouseInventory-headerbody">
            <Row>
              <Col md={1}></Col>
              <Col md={3}><Button onClick={onSubmit}>Kết thúc</Button></Col>
              <Col md={3}><Button onClick={() => navigate(-1)}>Hủy Bỏ</Button></Col>
            </Row>
          </div>
          <div>
            <Row>
              <Col md={4}></Col>
              <Col md={10}>
                <h2>Mã Phiếu: {detail?.inventoryId}</h2>
              </Col>
            </Row>
          </div>
          <div style={{ margin: "10px 0px 0px 0px" }}>
            <Form
              labelCol={{
                span: 4,
              }}
              wrapperCol={{
                span: 14,
              }}
              layout="horizontal"
            >
              <Form.Item label="Tên Kho" label-size="20px">
                <Input value={detail?.storeHouseName} disabled />
              </Form.Item>
              <Form.Item label="Người Kiểm Kê" label-size="20px">
                <Input
                  value={
                    detail?.personInChargeName ||
                    localStorage.getItem("usernameToken")
                  }
                  disabled
                />
              </Form.Item>

              {/* <Form.Item label="Ngày dự kiến">
                <Space direction="vertical" size={12}>
                  <Input value={detail?.inventoryDate} disabled />
                </Space>
              </Form.Item> */}
              <Form.Item label="Ngày kiểm kê">
                <Space direction="vertical" size={12}>
                  <DatePicker value={dateInventory ? moment(dateInventory) : null} onChange={onChangeDate} format={dateFormatList} disabled={isDone}/>
                </Space>
              </Form.Item>
            </Form>
          </div>
          <Button type="primary" onClick={addNewRow}>
            Thêm 1 dòng
          </Button>
          <div className="container">
            <Table
              className="transfer-table"
              columns={renderColumn(
                listProduct,
                deleteRow,
                onChangeProduct,
                onChangeNumberRequest,
                onChangeNote,
                isDone
              )}
              dataSource={dataSource}
              pagination={false}
            />
          </div>
          <div style={{ margin: "20px 0px 0px 0px" }}>
            <Space></Space>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const MenuItem = [
  getItem("Kho Hàng ", "sub1", <ShopOutlined />, [
    getItem("Tổng quan", "1"),
    getItem("Điều chuyển", "2"),
    getItem("Kiểm kê", "3"),
    getItem("Nhập kho", "4"),
    getItem("Xuất kho", "5"),
    getItem("Nhóm sản phẩm", "6"),
    getItem("Nhóm đơn vị", "7"),
    getItem("Đơn vị", "8"),
    getItem("Sản phẩm", "9"),

  ]),
];

const defaultRow = {
  idProduct: "",
  actualQuantity: "",
  note: "",
};

const renderColumn = (
  listProduct,
  deleteRow,
  onChangeProduct,
  onChangeNumberRequest,
  onChangeNote,
  isDone
) => [
  {
    title: "",
    dataIndex: "delete",
    key: "delete",
    width: "5%",
    render: (_, row) => (
      <Button
        className="btn-del-trans"
        onClick={() => deleteRow(row)}
        type="primary"
        icon={<MinusCircleOutlined />}
        danger
        disabled={isDone}
      />
    ),
  },
  {
    title: "Sản Phẩm",
    dataIndex: "idProduct",
    key: "idProduct",
    width: " 30%",
    render: (_, row) => (
      <Select
        showSearch
        placeholder="Select Product"
        value={_}
        style={{ width: "100%" }}
        optionFilterProp="children"
        onChange={(val) => onChangeProduct(row, val)}
        filterOption={(input, option) =>
          (option?.label ?? "").toLowerCase().includes(input.toLowerCase())
        }
        options={listProduct}
        disabled={isDone}
      />
    ),
  },
  {
    title: "Số lượng",
    dataIndex: "quantityStock",
    key: "quantityStock",
    render: (_, record) =>
    _ ? _ : listProduct.find((prod) => prod.value === record.idProduct)?.quantity ||
      0,
  },
  {
    title: "Số lượng thực tế",
    dataIndex: "actualQuantity",
    key: "actualQuantity",
    render: (_, row) => (
      <InputNumber
        value={_}
        min={1}
        onChange={(val) => onChangeNumberRequest(row, val)}
        className="input-transfer"
        disabled={isDone}
      />
    ),
  },
  {
    title: "Chênh lệch",
    dataIndex: "devant",
    key: "devant",
    render: (_, record) =>
      Math.abs(
        (listProduct.find((prod) => prod.value === record.idProduct)
          ?.quantity || 0) - record?.actualQuantity
      ),
  },
  {
    title: "Lý do",
    dataIndex: "note",
    key: "note",
    render: (_, row) => (
      <Input
        value={_}
        onChange={(e) => onChangeNote(row, e.target.value)}
        className="input-transfer"
        disabled={isDone}
      />
    ),
  },
];

export default WarehouseInventory;
