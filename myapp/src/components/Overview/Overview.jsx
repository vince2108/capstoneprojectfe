import "./Overview.css";
import {
  MenuFoldOutlined,
  ShopOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { Layout, Menu, Row, Col, Avatar, Card, Button ,message, Dropdown} from "antd";
import React, { useEffect, useState } from "react";
import ReceiptService from "../../service/ReceiptService";
import { useNavigate } from "react-router-dom";
import InventoryService from "../../service/InventoryService";


function Overview() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");
  const [listReceiptNeedDo,setListReceiptNeedDo] = useState([])
  const [listInventoryNeedDo,setListInventoryNeedDo] = useState([])
  const [listExportNeedDo,setListExportNeedDo] = useState([])
  const navigate = useNavigate();
  useEffect(()=>{
    const setDataForPage = async()=>{
      const res = await ReceiptService.searchRecieveByStatusAndBranch(localStorage.getItem("branchId"),1)
      setListReceiptNeedDo(res.data.data)
      const res2 = await InventoryService.searchInventoryByBranchAndStatus(localStorage.getItem("branchId"),1)
      setListInventoryNeedDo(res2.data.data)
      const res3 = await InventoryService.getExportByBranchAndStatus(localStorage.getItem("branchId"),1)
      setListExportNeedDo(res3.data.data)
      console.log(res3.data.data)
    }
    setDataForPage();
  },[])

  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/overview");
    }
    if(e.key ==2){
      navigate("/listTransfer");
    }
    if(e.key ==3 ){
      navigate("/inventoryWHstaff");
    }
    if(e.key==4){
      navigate("/listReceiptWHstaff");
    }
    if (e.key == 5) {
      navigate("/listIssueWHstaff");
    }
    if (e.key == 6) {
      navigate("/productGroups");
    }
    if (e.key == 7) {
      navigate("/unitGroup");
    }
    if (e.key == 8) {
      navigate("/unit");
    }
    if (e.key == 9) {
      navigate("/product");
    }
  };

  const goToListReciept =()=>{
    navigate("/listReceiptWHstaff")
  }

  
  const goToListExport =()=>{
    navigate("/listIssueWHstaff")
  }

  const goToListInventory =()=>{
    navigate("/inventoryWHstaff")
  }
  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };  

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];


  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo"> </div>
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub1"]}
          selectedKeys={["1"]}
          mode="inline"
          items={MenuItem}
        />
      </Sider>
      <Layout className="site-layout">
      <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >
                <div>
                  <Avatar
                    size="default"
                    style={{ marginRight: 10 }}
                    src={window.localStorage.getItem("avatar")}
                  ></Avatar>
                  {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <h1>TỔNG QUAN</h1>
          <div className="site-card-wrapper">
            <Row gutter={16}>
              <Col span={8}>
                <Card
                  className="card-ov"
                  title="Nhập Kho"
                  bordered={false}
                >
                  {/* <div>Kho :</div> */}
                  <Button className="btn-ov" onClick={()=>goToListReciept()}>{listReceiptNeedDo === null ? "Không có đơn nào cần xử lý" : listReceiptNeedDo.length +" đơn cần xử lý"}</Button>
                </Card>
              </Col>

              <Col span={8}>
              <Card className="card-ov" title="Xuất kho" bordered={false}>
                  <Button className="btn-ov" onClick={()=>goToListExport()}>{listExportNeedDo === null ? "Không có yêu cầu nào cần xử lý" : listExportNeedDo.length +" đơn cần xử lý"}</Button>
                </Card>
              </Col>

              <Col span={8}>
                <Card className="card-ov" title="Kiểm Kê kho" bordered={false}>
                  <Button className="btn-ov" onClick={()=>goToListInventory()}>{listInventoryNeedDo === null ? "Không có đơn nào cần xử lý" : listInventoryNeedDo.length + " đơn cần xử lý"}</Button>
                </Card>
              </Col>
            </Row>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const MenuItem = [
  getItem("Kho Hàng ", "sub1", <ShopOutlined />, [
    getItem("Tổng quan", "1"),
    getItem("Điều chuyển", "2"),
    getItem("Kiểm kê", "3"),
    getItem("Nhập kho", "4"),
    getItem("Xuất kho", "5"),
    getItem("Nhóm sản phẩm", "6"),
    getItem("Nhóm đơn vị", "7"),
    getItem("Đơn vị", "8"),
    getItem("Sản phẩm", "9"),
  ]),
];

export default Overview;
