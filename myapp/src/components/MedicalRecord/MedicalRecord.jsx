import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { Layout, Menu, Row, Col, Avatar,Button, message, Dropdown } from "antd";
import React, { useState, useEffect } from "react";
import "./MedicalRecord.css";
import { AppstoreOutlined } from "@ant-design/icons";
import { Link, useNavigate } from "react-router-dom";
import { AudioOutlined } from "@ant-design/icons";
import { Input, Space } from "antd";
import { Table, Tag } from "antd";
import axios from "axios";
import PatientService from "../../service/PatientService";
// import { set } from "immer/dist/internal";

function MedicalRecord() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");
  const [patientState, setPatientFState] = useState([]);
  const navigate = useNavigate();
  const [searchValue, setSerchValue] = useState();
  const [ErrorMessage,setErrorMessage] = useState();

  const searchPatientByNameFun = async () => {
    console.log(searchValue);    
    const resF = await PatientService.getPatientByName(
      searchValue
    );
    if(searchValue===undefined || searchValue==='') {
      setErrorMessage('Vui lòng điền tên để tìm kiếm')
        document.getElementById('this-dialog').showModal();
        return;
    }
    console.log(resF.data);
    if(resF.data.status === 'Success'){
      if(resF.data.message === "No Data"){
        setErrorMessage('Không tìm thấy hồ sơ bênh nhân nào có tên ' +searchValue)
        document.getElementById('this-dialog').showModal();
        return;
      }
    }
    if (resF.data != null) {
      setPatientFState(resF.data.data);
    }
  }
  
  const ConfirmAciton = ()=>{
    document.getElementById('this-dialog').close();
  }

  useEffect(() => {
    const getAllPatientF = async () => {
      try {
        const res = await axios.get("http://localhost:8081/api/v1/patient");
        setPatientFState(res.data.data);
        console.log(setPatientFState);
      } catch (error) {
        console.log(error.message);
      }
    };
    getAllPatientF();
  }, []);

  const { Search } = Input;

  const onSearch = (value) => console.log(value);
  const suffix = (
    <AudioOutlined
      style={{
        fontSize: 16,
        color: "#1890ff",
      }}
    />
  );

  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/checkUp");
    }
    if (e.key == 2) {
      navigate("/medicalRecord");
    }
    if (e.key == 3) {
      navigate("/listIssueDoctor");
    }
  };


  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      console.log("click", e.key);
    } else if (e.key === "2") {
      logOutAction();
    }

  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];


  const checkPatientMedicalRecord =(accountId)=>{
    navigate("/detailMedicalRecord", {
      state: { selectId: accountId, canEdit: false },
    });
  }


  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo" />
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub2"]}
          selectedKeys={["2"]}
          mode="inline"
          items={Menuitems}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
            <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >  
                <div>
                <Avatar
                  size="default"
                  style={{ marginRight: 10 }}
                  src={window.localStorage.getItem("avatar")}
                ></Avatar>
               {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <dialog id="this-dialog">
            <p> {ErrorMessage}</p>
            <button onClick={() => ConfirmAciton()}>Đóng</button>
          </dialog>
          <Row>
            <Col md={10}>
                <div>
                  <Input
                    placeholder="Tên bệnh nhân"
                    style={{ width: "75%" }}
                    value={searchValue}
                    onChange={(event) => setSerchValue(event.target.value)}
                  ></Input>
                  <Button onClick={() => searchPatientByNameFun()}>
                    Tìm
                  </Button>
                </div>
              </Col>
          </Row>
          {/* <Table columns={columns1} dataSource={patientState} />; */}

          <div class="container">
          <table className="tab1">
              <thead>
                <th>Mã BN</th>
                <th>Tên Bệnh Nhân</th>
                <th>Ngày Sinh</th>
                <th>Số Điện Thoại</th>
                <th>Giới Tính</th>
                <th>Nơi ở</th>
              </thead>
              <tbody>
                {
                  patientState?.map((patient) => {
                      return (
                        <tr onDoubleClick={()=>checkPatientMedicalRecord(patient.accountId)}>
                          <td data-label="col-1">
                            {"BN" + patient?.accountId}
                          </td>
                          <td data-label="col-2">
                            {patient?.fullName}
                          </td>
                          <td data-label="col-3">
                            {patient?.dob}
                          </td>
                          <td data-label="col-4">
                            {patient?.phoneNumber}
                          </td>
                          <td data-label="col-5">
                            {patient?.sex }
                          </td>
                          <td data-label="col-3">
                            {
                              patient?.address +
                                ", " +
                                patient?.village +
                                ", " +
                                patient?.district +
                                ", " +
                                patient?.province
                              }
                          </td>
                        </tr>
                      );
                    })}
              </tbody>
            </table>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const Menuitems = [
  getItem("Khám Bệnh", "sub1", <AppstoreOutlined />, [
    getItem("Danh sách chờ", "1"),
  ]),
  getItem("Hồ sơ bệnh nhân", "sub2", <AppstoreOutlined />, [
    getItem(" Hồ Sơ bệnh  nhân", "2"),
  ]),
  getItem("Yêu cầu xuất thuốc", "sub3", <AppstoreOutlined />, [
    getItem(" Yêu cầu xuất thuốc", "3"),
  ]),

];

export default MedicalRecord;
