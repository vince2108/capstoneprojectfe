import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  EditOutlined,
  LogoutOutlined
} from "@ant-design/icons";
import { Layout, Menu, Row, Col, Avatar, message, Dropdown } from "antd";
import React, { useEffect, useState } from "react";
import "./DetailMedicalRecord.css";
import { AppstoreOutlined } from "@ant-design/icons";
import { Link, useLocation, useNavigate } from "react-router-dom";

import {
  Button,
  //Cascader,
  DatePicker,
  Form,
  Input,
  // InputNumber,
  //Radio,
  Select,
  //Switch,
  //   TreeSelect,
} from "antd";
import DoctorService from "../../service/DoctorService";
import moment from "moment";
import PatientService from "../../service/PatientService";
import MedicalRecord from "./MedicalRecord";
function DetailMedicalRecord() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");
  const { TextArea } = Input;
  const [patientInfor, setPatientInfor] = useState([]);

  const [accountId, setAccountId] = useState();
  const [fullName, setFullName] = useState();
  const [dateState, setDate] = useState();
  const [sex, setSex] = useState();
  const [job, setJob] = useState();
  const [identityCard, setIdentityCard] = useState();
  const [address, setAddress] = useState();
  const [village, setVillage] = useState();
  const [district, setDistrict] = useState();
  const [province, setProvince] = useState();
  const [phoneNumber, setPhoneNumber] = useState();
  const [email, setEmail] = useState();
  const [medicalRecordId,setMedicalRecordId] = useState();
  const [circuit, setCircuit] = useState();
  const [bloodpressure, setBloodPressure] = useState();
  const [allergies, setAllergies] = useState();
  const [note, setNote] = useState();
  const [height, setHeight] = useState();
  const [temperatue, setTemperature] = useState();
  const [weight, setWeight] = useState();
  const [pastMedicalHistory, setPastMedicalHistory] = useState();
  const [ethnicity, setEthnicity] = useState();
  const [icd10, setIcd10] = useState();
  const [icd, setIcd] = useState();
  const [advice, setAdvice] = useState();
  const [bmi, setBmi] = useState();
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();
  const location = useLocation();
  let newDate = new Date();
  let date = newDate.getDate()+1;
  let month = newDate.getMonth() + 1;
  let year = newDate.getFullYear();
  const currentDate = date + "/" + month + "/" + year;
  let currentDateFormYYYYMMDD = year + "-" + month + "-" + date;

  const saveAction = async() =>{
    const res = await PatientService.updatePatientInforAndMedicalRecord(
      accountId,fullName,dateState,address,village,district,province,sex,identityCard,phoneNumber,ethnicity,
      job,email,medicalRecordId,circuit,bloodpressure,pastMedicalHistory,allergies,note,icd10,icd,advice,
      currentDateFormYYYYMMDD,weight,weight/(height*2),height,temperatue
    )
    navigate("/generalExamination", { replace: true });
  }


  useEffect(() => {
    const setPatientInforData = async () => {
      const res = await DoctorService.getAllPatientInforByPatientId(
        location.state.selectId
      );
      setPatientInfor(res.data.data);
      console.log(res.data.data);
      setLoading(true);
      setDataForPage();
    };
    setPatientInforData();
  }, [loading]);

  const setDataForPage = () => {
    setAccountId(patientInfor.accountId);
    console.log(patientInfor.fullName);
    setFullName(patientInfor.fullName);
    setDate(patientInfor.dob);
    setAddress(patientInfor.address);
    setVillage(patientInfor.village);
    setDistrict(patientInfor.district);
    setProvince(patientInfor.province);
    console.log(patientInfor.province);
    setSex(patientInfor.sex);
    setIdentityCard(patientInfor.identityCard);
    setPhoneNumber(patientInfor.phoneNumber);
    setEthnicity(patientInfor.ethnicity);
    setJob(patientInfor.job);
    setEmail(patientInfor.email);
    setCircuit(patientInfor.circuit);
    setBloodPressure(patientInfor.bloodPressure);
    setPastMedicalHistory(patientInfor.pastMedicalHistory);
    setAllergies(patientInfor.allergies);
    setNote(patientInfor.note);
    setIcd10(patientInfor.icd10);
    setIcd(patientInfor.icd);
    setAdvice(patientInfor.advice);
    setBmi(patientInfor.bmi);
    setTemperature(patientInfor.temperature);
    setWeight(patientInfor.weight);
    setHeight(patientInfor.height);
    setMedicalRecordId(patientInfor.medicalRecordId)
  };
  const onClick = (e) => {
    console.log("click ", e);
     // setCurrent(e.key);
     if (e.key == 1) {
      navigate("/checkUp");
    }
    if (e.key == 2) {
      navigate("/medicalRecord");
    }
    if (e.key == 3) {
      navigate("/listIssueDoctor");
    }

  };
  const [componentSize, setComponentSize] = useState("default");
  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  const returnAction = () => {
    navigate(-1);
  };

  const finishAction=()=>{
    const res = DoctorService.finishService(location.state.selectId,localStorage.getItem("consultingRoom"),note)
    navigate(-1)
  }

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      console.log("click", e.key);
    } else if (e.key === "2") {
      logOutAction();
    }
  };
  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];

  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo" />
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub2"]}
          selectedKeys={["2"]}
          mode="inline"
          items={Menuitems}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
            <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >  
                <div>
                <Avatar
                  size="default"
                  style={{ marginRight: 10 }}
                  src={window.localStorage.getItem("avatar")}
                ></Avatar>
               {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <div className="detailmedicalrecord-btn-back-save">
            <Row>
              <Col md={1}></Col>
              <Col md={2}>
              <Button hidden={location.state.canEdit ? false: true} onClick={() => saveAction()}>Lưu</Button>
              </Col>
              <Col md={2}>
              <Button hidden={location.state.canEdit ? false: true} onClick={() => returnAction()}>Trở lại</Button>
              </Col>
              <Col md={2}>
                <Button hidden={location.state.canEdit ? false: true} onClick={() => finishAction()}>Kết thúc khám</Button>
              </Col>
            </Row>
          </div>
          <Row style={{ margin: "20px 0px 0px 0px" }}>
            <Col span={12}>
              <Form
                labelCol={{
                  span: 6,
                }}
                wrapperCol={{
                  span: 17,
                }}
              >
                <Form.Item
                  label="Mã BN"
                  style={{
                    display: "inline-block",
                    width: "calc(100% - 8px)",
                  }}
                >
                  <Input
                    value={accountId}
                    disabled={true}
                  ></Input>
                </Form.Item>
                <Form.Item
                  label="Tên bệnh nhân"
                  disabled = {location.state.canEdit ? false: true}
                  rules={[
                    {
                      required: true,
                    },
                  ]}
                  style={{
                    display: "inline-block",
                    width: "calc(100% - 8px)",
                  }}
                >
                  <Input
                   disabled = {location.state.canEdit ? false: true}
                    value={fullName}
                    onChange={(event) => setFullName(event.target.value)}
                  ></Input>
                </Form.Item>
                <Form.Item label="Ngày sinh" style={{ marginBottom: 0 }}>
                  <DatePicker
                     disabled = {location.state.canEdit ? false: true}
                    style={{
                      display: "inline-block",
                      width: "calc(50% - 8px)",
                    }}
                    value={moment(
                      dateState,
                      "YYYY-MM-dd"
                    )}
                    onChange={(event) =>
                      event != null
                        ? setDate(event)
                        : setDate(event.target.value)
                    }
                    disabledDate={d => !d || d.isAfter(currentDateFormYYYYMMDD)}
                  >

                  </DatePicker>
                  <Form.Item
                    label="Giới tính"
                    style={{
                      display: "inline-block",
                      width: "calc(50%)",
                    }}
                  >
                    <Input
                     disabled = {location.state.canEdit ? false: true}
                      value={sex}
                      onChange={(event) => setSex(event.target.value)}
                    ></Input>
                  </Form.Item>
                </Form.Item>
                <Form.Item label="Nghề nghiệp" style={{ marginBottom: 0 }}>
                  <Form.Item
                    style={{
                      display: "inline-block",
                      width: "calc(50% - 8px)",
                    }}
                  >
                    <Input
                       disabled = {location.state.canEdit ? false: true}
                      value={job}
                      onChange={(event) => setJob(event.target.value)}
                    ></Input>
                  </Form.Item>
                  <Form.Item
                    label="Dân tộc"
                    style={{
                      display: "inline-block",
                      width: "calc(50%)",
                    }}
                  >
                    <Input
                    disabled = {location.state.canEdit ? false: true}
                      value={ethnicity}
                      onChange={(event) => setEthnicity(event.target.value)}
                    ></Input>
                  </Form.Item>
                </Form.Item>
                <Form.Item
                  label="Số nhà"
                  style={{
                    width: "calc(100%-8px)",
                  }}
                >
                  <Input
                  disabled = {location.state.canEdit ? false: true}
                    value={address}
                    onChange={(event) => setAddress(event.target.value)}
                  ></Input>
                </Form.Item>
                <Form.Item
                  label="T/Q/P Xã"
                  style={{
                    width: "calc(100%-8px)",
                  }}
                >
                  <Input
                  disabled = {location.state.canEdit ? false: true}
                    value={village}
                    onChange={(event) => setVillage(event.target.value)}
                  ></Input>
                </Form.Item>

                <Form.Item
                  label="Q Huyện"
                  style={{
                    width: "calc(100%-8px)",
                  }}
                >
                  <Input
                  disabled = {location.state.canEdit ? false: true}
                    value={district}
                    onChange={(event) => setDistrict(event.target.value)}
                  ></Input>
                </Form.Item>
                <Form.Item
                  label="T/T Tỉnh"
                  style={{
                    width: "calc(100%-8px)",
                  }}
                >
                  <Input
                  disabled = {location.state.canEdit ? false: true}
                    value={province}
                    onChange={(event) => setProvince(event.target.value)}
                  ></Input>
                </Form.Item>
                <Form.Item
                  label="Điện Thoại"
                
                  style={{
                    width: "calc(100%-8px)",
                  }}
                >
                  <Input
                  disabled = {location.state.canEdit ? false: true}
                    value={phoneNumber}
                    onChange={(event) => setPhoneNumber(event.target.value)}
                  ></Input>
                </Form.Item>
                <Form.Item
                  label="Email"
                  style={{
                    width: "calc(100%-8px)",
                  }}
                >
                  <Input
                  disabled = {location.state.canEdit ? false: true}
                   value={email}
                  onChange={(event) => setEmail(event.target.value)}></Input>
                </Form.Item>
                <Form.Item
                  label="Note"
                  style={{
                    width: "calc(100%-8px)",
                  }}
                >
                  <TextArea
                    disabled = {location.state.canEdit ? false: true}
                    rows={4}
                    value={note}
                    onChange={(event) => setNote(event.target.value)}
                  />
                </Form.Item>
              </Form>
            </Col>
            <Col span={12}>
              <Form
                labelCol={{
                  span: 6,
                }}
                wrapperCol={{
                  span: 17,
                }}
              >
                <Form.Item
                  label="Mạch"
                  style={{
                    display: "inline-block",
                    width: "calc(100% - 8px)",
                  }}
                >
                  <Input
                    disabled = {location.state.canEdit ? false: true}
                    value={circuit}
                    onChange={(event) => setCircuit(event.target.value)}
                  ></Input>
                </Form.Item>
                <Form.Item
                  label="Huyết áp"
              
                  style={{
                    display: "inline-block",
                    width: "calc(100% - 8px)",
                  }}
                >
                  <Input
                    disabled = {location.state.canEdit ? false: true}
                    value={bloodpressure}
                    onChange={(event) => setBloodPressure(event.target.value)}
                  ></Input>
                </Form.Item>

                <Form.Item
                  label="Chiều Cao"
                  style={{
                    width: "calc(100%-25px)",
                  }}
                >
                  <Input
                  disabled = {location.state.canEdit ? false: true}
                    value={height}
                    onChange={(event) => setHeight(event.target.value)}
                  ></Input>
                </Form.Item>

                <Form.Item
                  label="Nhiệt độ"
                  style={{
                    width: "calc(100%-25px)",
                  }}
                >
                  <Input
                  disabled = {location.state.canEdit ? false: true}
                    value={temperatue}
                    onChange={(event) => setTemperature(event.target.value)}
                  ></Input>
                </Form.Item>
                <Form.Item
                  label="Cân Nặng"
                  style={{
                    width: "calc(100%-25px)",
                  }}
                >
                  <Input
                  disabled = {location.state.canEdit ? false: true}
                    value={weight}
                    onChange={(event) => setWeight(event.target.value)}
                  ></Input>
                </Form.Item>
                <Form.Item
                  label="Tiền Sử"
                  style={{
                    width: "calc(100%-8px)",
                  }}
                >
                  <TextArea
                  disabled = {location.state.canEdit ? false: true}
                    rows={4}
                    value={pastMedicalHistory}
                    onChange={(event) =>
                      setPastMedicalHistory(event.target.value)
                    }
                  />
                </Form.Item>
                <Form.Item
                  label="Dị Ứng"
                  style={{
                    width: "calc(100%-8px)",
                  }}
                >
                  <TextArea
                  disabled = {location.state.canEdit ? false: true}
                    rows={4}
                    value={allergies}
                    onChange={(event) => setAllergies(event.target.value)}
                  />
                </Form.Item>
                <Form.Item
                  label="ICD 10"
                  style={{
                    width: "calc(100%-8px)",
                  }}
                >
                  <Input
                  disabled = {location.state.canEdit ? false: true}
                    value={icd10}
                    onChange={(event) => setIcd10(event.target.value)}
                  ></Input>
                </Form.Item>
                <Form.Item
                  label="ICD kèm theo:"
                  style={{
                    width: "calc(100%-8px)",
                  }}
                >
                  <TextArea
                  disabled = {location.state.canEdit ? false: true}
                    rows={4}
                    value={icd}
                    onChange={(event) => setIcd(event.target.value)}
                  />
                </Form.Item>
                <Form.Item
                  label="Lời dặn:"
                  style={{
                    width: "calc(100%-8px)",
                  }}
                >
                  <TextArea
                  disabled = {location.state.canEdit ? false: true}
                    rows={4}
                    value={advice}
                    onChange={(event) => setAdvice(event.target.value)}
                  />
                </Form.Item>
              </Form>
            </Col>
          </Row>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const Menuitems = [
  getItem("Khám Bệnh", "sub1", <AppstoreOutlined />, [
    getItem("Danh sách chờ", "1"),
  ]),
  getItem("Hồ sơ bệnh nhân", "sub2", <AppstoreOutlined />, [
    getItem(" Hồ Sơ bệnh  nhân", "2"),
  ]),
  getItem("Yêu cầu xuất thuốc", "sub3", <AppstoreOutlined />, [
    getItem(" Yêu cầu xuất thuốc", "3"),
  ]),

];

export default DetailMedicalRecord;
