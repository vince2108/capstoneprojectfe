import {
    MenuFoldOutlined,
    MenuUnfoldOutlined,
    EditOutlined,
    LogoutOutlined,
    HomeOutlined,
    SnippetsOutlined,
    TeamOutlined
  } from "@ant-design/icons";
  import { Layout, Menu, Row, Col, Avatar, Button, message, Dropdown } from "antd";
  import React, { useState, useEffect } from "react";
  import "./CreateBase.css";
  
  
  import { Form, Input } from "antd";
  import { Link, useLocation, useNavigate } from "react-router-dom";
  import BranchService from "../../service/BranchService";
  
  function CreateBase() {
    const { Header, Sider, Content } = Layout;
    const [collapsed, setCollapsed] = useState(false);
    const [theme] = useState("dark");
    const [current, setCurrent] = useState("1");
    const [branchName, setBranchName] = useState([]);
    const [address, setAddress] = useState([]);
    const [phoneNumber, setPhoneNumber] = useState([]);
    const [branch, setBranch] = useState([]);
    const [oldBranch, setOldBranch] = useState();
    const location = useLocation();
    const navigate = useNavigate();
    const [loading, setLoading] = useState(false);
    const [BranchId, setBranchId] = useState();
  
    const onClick = (e) => {
      console.log("click ", e);
      // setCurrent(e.key);
      if (e.key == 1) {
        navigate("/base");
      }
      if (e.key == 2) {
        // navigate("/clinic");
      }
      if (e.key == 3) {
        navigate("/listManager");
      }
    };
    useEffect(()=>{
      if (location.state.isOldBranch === true) {
        const setDataForOldBranch = async () =>{
          setBranchId(location.state.branchId);
          const resBranch = await BranchService.getBranchById(location.state.branchId)
          setOldBranch(resBranch.data.data)
          console.log(resBranch.data.data); 
          setLoading(true) 
          setDataForPage()
        }
        setDataForOldBranch()
        
      } else {
        console.log("Not Old Branch")
      }

    }, [loading])

    const setDataForPage = () =>{
      setAddress(oldBranch.address)
      setBranchName(oldBranch.branchName)
      setPhoneNumber(oldBranch.phoneNumber)
    }
    const SaveAction = async (values)=>{
      const res = await BranchService.createBranch(
        values.address,
        values.phoneNumber,
        values.branchName
      )
      console.log(res.data.data)
      navigate("/base")
    }
    const ReturnAction = () => {
      navigate(-1);
    };
    const SaveData = async()=>{
      const resUpdate = await BranchService.updateBranch(location.state.branchId,
        address,
        phoneNumber,
        branchName
        )
        console.log(resUpdate.data.data)
        navigate("/base")
    }

    const logOutAction = () => {
      localStorage.clear();
      message.success("Đăng xuất thành công");
      navigate("/");
    };
  
    const handleMenuClick = (e) => {
      if (e.key === "1") {
        navigate("/updateStaff", {
          state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
        });
      } else if (e.key === "2") {
        logOutAction();
      }
    };
  
    const items = [
      {
        label: "Cập Nhập Thông Tin",
        key: "1",
        icon: <EditOutlined />,
      },
      {
        label: "Đăng Xuất",
        key: "2",
        icon: <LogoutOutlined />,
        style: { color: "red" },
      },
    ];
  
    return (
      <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo"> </div>
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub1"]}
          selectedKeys={["1"]}
          mode="inline"
          items={MenuItem}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >
                <div>
                  <Avatar
                    size="default"
                    style={{ marginRight: 10 }}
                    src={window.localStorage.getItem("avatar")}
                  ></Avatar>
                  {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
          <Content
            className="site-layout-background"
            style={{
              overflow: "scroll",
              margin: "24px 16px",
              padding: 24,
              minHeight: 280,
            }}
          >
          
            <div style={{ margin: "20px 0px 0px 0px" }}>
              <Form
              onFinish={(values) => SaveAction(values)}
                labelCol={{
                  span: 4,
                }}
                wrapperCol={{
                  span: 14,
                }}
                layout="horizontal"
              >
                <Form.Item>
                <Row>
                  <Col md={1}></Col>
                  <Col md={5}>
                    <Button
                      type="primary"
                      htmlType="submit"
                      onClick={()=>{
                        if (location.state.isOldBranch===true) {
                          SaveData()
                          
                        } else {
                          SaveAction()
                        }
                      }}
                      
                    >
                      Lưu
                    </Button>
                  </Col>
                  <Col md={2}>
                    <Button onClick={ReturnAction}>Hủy</Button>
                  </Col>
                </Row>
              </Form.Item>
                
              <Form.Item label="Tên Cơ Sở" label-size="20x"
                  name="branchName"

                  rules={[
                   {
                     required: true,
                     message: 'Vui Lòng Điền Tên Cơ Sở',
                   },
                 ]}
                 
                 hasFeedback
                >
                  <Input
                  ></Input>
                </Form.Item>
                <Form.Item label="Địa Chỉ" label-size="20px"
                     name="address"

                     rules={[
                      {
                        required: true,
                        message: 'Vui Lòng Điền Địa Chỉ',
                      },
                    ]}
                    
                    hasFeedback
                >
                  <Input
                 ></Input>
                </Form.Item>
  
                <Form.Item label="Điện Thoại" label-size="20px"
                     name="phoneNumber"

                     rules={[
                      {
                        required: true,
                        message: 'Vui Lòng Điền Số điện thoại',
                      },
                    ]}
                    
                    hasFeedback>
                  <Input
                 ></Input>
                </Form.Item>
              </Form>
            </div>
          </Content>
        </Layout>
      </Layout>
    );
  }
  function getItem(label, key, icon, children, type) {
    return {
      key,
      icon,
      children,
      label,
      type,
    };
  }
  const MenuItem = [
    getItem("Quản lý cơ sở", "sub1", <HomeOutlined />, [
      getItem("Tạo cơ sở", "1"),
    ]),
    getItem("Báo cáo", "sub2", <SnippetsOutlined />, [
      getItem("Báo cáo", "2")]),
    getItem("Quán lý nhân viên", "sub3", <TeamOutlined />, [
      getItem("Thêm quản lý", "3"),
    ]),
  ];
  
  export default CreateBase;
  