import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  LogoutOutlined,
  EditOutlined,
  SnippetsOutlined,
  HomeOutlined,
  TeamOutlined,
} from "@ant-design/icons";
import {
  Layout,
  Menu,
  Row,
  Col,
  Avatar,
  Button,
  Checkbox,
  message,
  Dropdown,
} from "antd";
import React, { useState, useEffect } from "react";
import "./Base.css";

import BranchService from "../../service/BranchService";
import { Link, useNavigate } from "react-router-dom";

function Base() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");
  const [base, setBase] = useState([]);

  const navigate = useNavigate();

  useEffect(() => {
    const getAllBase = async () => {
      const res = await BranchService.getAllBranch();
      setBase(res.data.data);
      console.log(res.data.data);
    };
    getAllBase();
  }, []);

  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/base");
    }
    if (e.key == 2) {
      // navigate("/clinic");
    }
    if (e.key == 3) {
      navigate("/listManager");
    }
  };

  const onChange = (e) => {
    console.log(`checked = ${e.target.checked}`);
  };

  const createNewBranch = () => {
    navigate("/createBase", {
      state: { isOldBranch: false },
    });
  };
  const updateBranch = (branchId) => {
    console.log(branchId);
    navigate("/updateBase", {
      state: { branchId: branchId, isOldBranch: true },
    });
  };

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];

  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo"> </div>
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub1"]}
          selectedKeys={["1"]}
          mode="inline"
          items={MenuItem}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >
                <div>
                  <Avatar
                    size="default"
                    style={{ marginRight: 10 }}
                    src={window.localStorage.getItem("avatar")}
                  ></Avatar>
                  {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <div className="div1-wh">
            <Row>
              <Col md={2}>
                <Button
                  style={{ margin: "0px 0px 0px 10px" }}
                  onClick={createNewBranch}
                >
                  Tạo
                </Button>
              </Col>
              <Col md={2}>
                <Button style={{ margin: "0px 0px 0px 10px" }}>Xóa</Button>
              </Col>
            </Row>
          </div>
          <div class="container">
            <table className="tab1">
              <thead>
                <th>Cơ sở</th>
                <th>Địa chỉ</th>
                <th>Số Điện Thoại</th>
                <th></th>
              </thead>
              <tbody>
                {base.map((base) => {
                  return (
                    <tr>
                      <td data-label="col-1">
                        <Checkbox onChange={onChange}>
                          {base != null ? base.branchName : ""}
                        </Checkbox>
                      </td>
                      <td data-label="col-2">
                        {base != null ? base.address : ""}
                      </td>
                      <td data-label="col-3">
                        {base != null ? base.phoneNumber : ""}
                      </td>
                      <td data-label="col-4">
                        <Button onClick={() => updateBranch(base.branchId)}>
                          Update
                        </Button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const MenuItem = [
  getItem("Quản lý cơ sở", "sub1", <HomeOutlined />, [
    getItem("Tạo cơ sở", "1"),
  ]),
  getItem("Báo cáo", "sub2", <SnippetsOutlined />, [
    getItem("Báo cáo", "2")]),
  getItem("Quán lý nhân viên", "sub3", <TeamOutlined />, [
    getItem("Thêm quản lý", "3"),
  ]),
];

export default Base;
