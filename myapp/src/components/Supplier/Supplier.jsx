import "./Supplier.css";
import {
  MenuFoldOutlined,
  ShopOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Layout, Menu, Row, Col, Avatar, Button, Checkbox } from "antd";
import React, { useState, useEffect } from "react";
import SupplierService from "../../service/SupplierService";

function Supplier() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");
  const [suppliers, setSuppliers]=useState([])

  const onClick = (e) => {
    console.log("click ", e);
    setCurrent(e.key);
  };

  const onChange = (e) => {
    console.log(`checked = ${e.target.checked}`);
  };
  useEffect(() => {
    const getAllSuppliers = async () => {
      const res = await SupplierService.getAllSupplier();
      setSuppliers(res.data.data);
    };
    getAllSuppliers();
  }, []);

  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo"> </div>
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub1"]}
          selectedKeys={[current]}
          mode="inline"
          items={items}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={21}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={3}>
              <div>
                <Avatar size="default" icon={<UserOutlined />}></Avatar> Long
              </div>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow:"scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <div className="div-supplier-headercontent">
            <Row>
                <Col md={1}></Col>
              <Col md={2}>
                <Button className="btn-tf">Tạo</Button>
              </Col>
              <Col md={2}>
                <Button className="btn-tf">Xóa</Button>
              </Col>
            </Row>
          </div>
          <div class="div-supplier-bodycontent">
          <table className="tab-supplier-bodycontent">
              <thead>
                <th></th>
                <th>nhà Cung Cấp</th>
                <th>Tên viết tắt</th>
                <th>Số Điện thoại</th>
              </thead>
              <tbody>
                 {suppliers.map((suppliers)=>{
                  return(
                    <tr>
                  <td data-label="col-1"><Checkbox onChange={onChange}></Checkbox></td>
                  <td data-label="col-2">{suppliers != null ? suppliers.supplierName : ""}</td>
                  <td data-label="col-3">{suppliers != null ? suppliers.abbreviationName : ""}</td>
                  <td data-label="col-4">{suppliers != null ? suppliers.phoneNumber : ""}</td>
                 </tr>
                  )
                 })}

                 
              </tbody>
            </table>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const items = [
  getItem("Kho Hàng ", "kh", <ShopOutlined />, [
    getItem("Tổng quan", "1"),
    getItem("Điều chuyển", "2"),
    getItem("Kiểm kê", "3"),
    getItem("Quản lý kho", "4"),
    getItem("Nhóm sản phẩm", "5"),
    getItem("Nhóm đơn vị", "6"),
    getItem("Đơn vị", "7"),
    getItem("Nhập kho", "8"),
  ]),
];

export default Supplier;
