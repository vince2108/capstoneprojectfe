import "./Bill.css";
import { Row, Col, Button } from "antd";
import React ,{ useEffect, useRef } from "react";
import { useReactToPrint } from "react-to-print";
import { useLocation, useNavigate } from "react-router-dom";
import PatientService from "../../service/PatientService";
import { useState } from "react";
import BranchService from "../../service/BranchService";
import PaymentService from "../../service/PaymentService";

class ComponentToPrint extends React.Component {
  render (){
    return (
      <div className="bill">
        <div>
          <Row>
            <Col md={1}></Col>
            <Col md={10} className="bill-clinic-address">
              <h2 className="h2-bill">
                Phóng Khám {this.props.branchInfor.branchName}
              </h2>
              <h4 className="h4-bill">
                Địa Chỉ: {this.props.branchInfor.address} SĐT:{" "}
                {this.props.branchInfor.phoneNumber}
              </h4>
            </Col>
            <Col md={6}></Col>
            <Col md={7} className="bill-idinvoice">
              {" "}
              <h4 className="h4-bill">Số phiếu: PT25325</h4>
            </Col>
          </Row>
        </div>
        <div className="bill-title">
          <h3 className="h3-bill">PHIẾU THU</h3>
        </div>
        <div className="bill-content">
          <p>Họ và Tên: {this.props.userInfor.fullName}</p>
          <p>
            Giới Tính: {this.props.userInfor.sex} &emsp;&emsp; Số ĐT:{" "}
            {this.props.userInfor.phoneNumber}
          </p>
          <table className="tab1">
              <thead>
                <th>Dịch vụ </th>
                <th>Đơn giá</th>
                <th>Giá trị gia tăng</th>
                <th>Thành tiền</th>
              </thead>
              <tbody>
                {
                  this.props.servicedList?.map((service) => {
                      return (
                        <tr>
                          <td data-label="col-1">
                              {service.serviceName}
                          </td>
                          <td data-label="col-4">{service.price}</td>
                          <td data-label="col-5">{service.taxPercent}%</td>
                          <td data-label="col-5">{service.totalCost}</td>
                        </tr>
                      );
                    })}
              </tbody>
            </table>
          <p>Tổng tiền: {this.props.sumPrice}</p>

        </div>
        <div className="bill-signature">
          <Row>
            <Col md={12}></Col>
            <Col md={12}>
            <p className="h4-bill">{this.props.currentDate}</p>
            <h4 className="h2-bill">NGƯỜI THU TIỀN</h4>
            <br />
            <p className="h2-bill">{this.props.cashierName}</p>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const Bill = () => {
  const componentRef = useRef();
  const location = useLocation();
  const navigate = useNavigate();
  const [userInfor, setUserInfor] = useState([]);
  const [branchInfor, setBranchInfor] = useState([]);
  const [servicedList, setServiceList] = useState([]);
  const sumPrice = location.state.sumPrice;
  let newDate = new Date();
  let date = newDate.getDate();
  let month = newDate.getMonth() + 1;
  let year = newDate.getFullYear();
  const currentDate = "Ngày " + date + " Tháng " + month + " Năm " + year;
  const cashierName = localStorage.getItem("usernameToken");
  useEffect(() => {
    const getDataForBill = async () => {
      const res = await PatientService.getPatientById(location.state.accountId);
      setUserInfor(res.data.data);
      const res2 = await BranchService.getBranchById(
        localStorage.getItem("branchId")
      );
      setBranchInfor(res2.data.data);
      const res3 = await PaymentService.getReportServiceById(location.state.serviceList);
      setServiceList(res3.data.data);
    };
    getDataForBill();
  }, []);
  const handlePrint =  useReactToPrint({
    content: () => componentRef.current,
  });
  const getPayingAction = async ()=>{
    console.log(location.state.serviceList)
    const res = await PaymentService.patientPayingService(location.state.serviceList);
    handlePrint();
    navigate("/createPay", {
      replace: true,
      state: { accountId: location.state.accountId },
    });
  }
  const backAction = () => {
    navigate(-1);
  };
  return (
    <div>
      <ComponentToPrint
        ref={componentRef}
        userInfor={userInfor}
        branchInfor={branchInfor}
        servicedList={servicedList}
        cashierName={cashierName}
        sumPrice={sumPrice}
        currentDate={currentDate}

      />
      <Button onClick={() => { getPayingAction();}}>In hóa đơn</Button>&emsp;
      <Button onClick={backAction}>Trở lại</Button>
    </div>
  );
};
export default Bill;
