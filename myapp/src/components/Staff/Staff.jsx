import "./Staff.css";
import {
  MenuFoldOutlined,
  ShopOutlined,
  MenuUnfoldOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { Layout, Menu, Row, Col, Avatar, Checkbox, Button, Dropdown, message } from "antd";
import React, { useState, useEffect } from "react";
import { Input, Space } from "antd";
import StaffService from "../../service/StaffService";
import { Link, useNavigate } from "react-router-dom";

function Staff() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");

  const [staffF, setStaffFState] = useState([]);
  const [loading,setLoading] = useState(false);

  const navigate = useNavigate();

  useEffect(() => {
    const getAllStaffF = async () => {
      const res = await StaffService.getAllStaff();
      setStaffFState(res.data.data);
      setLoading(true)
    };
    getAllStaffF();
  }, [loading]);

  const checkInForStaff = (accountId) => {
    console.log(accountId);
    navigate("/updateStaff", {
      state: { accountId: accountId, isOldStaff: true },
    });
  };

  const deleteStaffAction = async(accountId) => {
    const res =await StaffService.deleteStaff(accountId)
    setLoading();
  };

  

  const createNewStaff = () => {
    navigate("/createStaff", {
      state: { isOldStaff: false },
    });
  };


  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/staff");
    }
    if (e.key == 2) {
      navigate("/clinic");
    }
    if (e.key == 3) {
      navigate("/report");
    }
    if (e.key == 4) {
      navigate("/service");
    }
    if (e.key == 5) {
      navigate("/divison");
    }
    if (e.key == 6) {
      navigate("/listReceiptManager");
    }
    if (e.key == 7) {
      navigate("/ListTransfer")
    }
    if (e.key == 8) {
      navigate("/inventoryManager")
    }
    if (e.key == 9) {
      // navigate("/inventoryManager")
    }
    if (e.key == 10) {
      navigate("/warehouse")
    }
  };

  // search
  const { Search } = Input;
  const onSearch = (value) => {};

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];
  
  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo"> </div>
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub1"]}
          selectedKeys={[current]}
          mode="inline"
          items={MenuItem}
        />
      </Sider>
      <Layout className="site-layout">
      <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >
                <div>
                  <Avatar
                    size="default"
                    style={{ marginRight: 10 }}
                    src={window.localStorage.getItem("avatar")}
                  ></Avatar>
                  {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <div className="div-iv">
            <Row>
              <Col md={5}>
                <h2>DANH SÁCH</h2>
              </Col>
              <Col md={8}></Col>
              <Col md={9}>
                <Space direction="vertical">
                {/* <Search
                    placeholder="input search text"
                    allowClear
                    enterButton="Search"
                    size="large"
                    onSearch={onSearch}
                  /> */}
                </Space>
              </Col>
              <Col md={2}>
                <Button onClick={createNewStaff}>Thêm mới</Button>
              </Col>
            </Row>
          </div>
          <div className="container">
            <table className="tab1">
              <thead>
                <th>Tên bác sĩ</th>
                <th>Số điện thoại</th>
                <th>Giới tính</th>
                <th>Email</th>
                <th></th>
              </thead>
              <tbody>
                {staffF?.map((staff) => {
                  return (
                    <tr key={staff.accountId}>
                      <td data-label="col-1">
                        {staff != null ? staff.fullName : ""}
                      </td>
                      <td data-label="col-2">
                        {staff != null ? staff.phoneNumber : ""}
                      </td>
                      <td data-label="col-3">
                        {staff != null ? staff.sex : ""}
                      </td>
                      <td data-label="col-4">
                        {staff != null ? staff.email : ""}
                      </td>
                      <td data-label="col-5">
                        <Button
                          onClick={() => checkInForStaff(staff.accountId)}
                        >
                          Update
                        </Button>{" "}
                        &nbsp;
                        <Button
                          onClick={() => deleteStaffAction(staff.accountId)}>Xóa</Button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  }; 
}
const MenuItem = [
  getItem("Quản lý", "sub1", <ShopOutlined />, [
    getItem("QL Nhân viên", "1"),
    getItem("QL Phòng khám", "2"),
    getItem("Báo cáo phòng khám", "3"),
    getItem("QL Dịch vụ", "4"),
    // getItem("QL Phòng ban", "5"),
  ]),
  getItem("Quản lý Kho Hàng ", "sub2", <ShopOutlined />, [
    getItem("Nhập kho", "6"),
    getItem("Điều chuyển", "7"),
    getItem("Kiểm kê", "8"),
    // getItem("Báo cáo kho", "9"),
    getItem("Quản lý kho", "10"),
  ]),

];

export default Staff;