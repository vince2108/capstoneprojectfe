import "./CreateStaff.css";
import {
  MenuFoldOutlined,
  ShopOutlined,
  MenuUnfoldOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import {
  Layout,
  Menu,
  Row,
  Col,
  Avatar,
  Button,
  Input,
  Form,
  Select,
  message,
  Dropdown,
} from "antd";
import { DatePicker, Space } from "antd";
import React, { useState, useEffect } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import StaffService from "../../service/StaffService";
import BranchService from "../../service/BranchService";
import RoleService from "../../service/RoleService";
import moment from "moment";
import ConsultingRoomService from "../../service/ConsultingRoomService";
const dateFormatList = ["DD/MM/YYYY", "DD/MM/YY"];

function CreateStaff() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");
  const [fullName, setFullName] = useState();
  const [dateState, setDate] = useState();
  const [dob, setDob] = useState();
  const [sex, setSex] = useState();
  const [job, setJob] = useState();
  const [identityCard, setIdentityCard] = useState();
  const [address, setAddress] = useState();
  const [village, setVillage] = useState();
  const [district, setDistrict] = useState();
  const [province, setProvince] = useState();
  const [phoneNumber, setPhoneNumber] = useState();
  const [ethnicity, setEthnicity] = useState();
  const [email, setEmail] = useState();
  const [userName, setUserName] = useState();
  const [passWord, setPassWord] = useState();
  const [confirmPassWord, setConfirmPassWord] = useState();
  const [newStaff, setNewStaff] = useState();
  const [branchS, setBranchSelect] = useState([]);
  const [branchIdS, setBranchSelectId] = useState([]);
  const [roleS, setRoleSelect] = useState([]);
  const [roleId, setRoleSelectId] = useState([]);
  const [conslutingRoomS, setConslutingRoomSelect]= useState([]);
  const [conslutingRoomId, setConslutingRoomSelectId]= useState([]);
  const [position, setPosition] = useState();


  const [loading, setLoading] = useState(false);
  const location = useLocation();
  const navigate = useNavigate();
  let newDate = new Date();
  let date = newDate.getDate() + 1;
  let month = newDate.getMonth() + 1;
  let year = newDate.getFullYear();
  const currentDate = date + "/" + month + "/" + year;
  let currentDateFormYYYYMMDD = year + "-" + month + "-" + date;

  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/staff");
    }
    if (e.key == 2) {
      navigate("/clinic");
    }
    if (e.key == 3) {
      navigate("/report");
    }
    if (e.key == 4) {
      navigate("/service");
    }
    if (e.key == 5) {
      navigate("/divison");
    }
    if (e.key == 6) {
      navigate("/listReceiptManager");
    }
    if (e.key == 7) {
      navigate("/ListTransfer")
    }
    if (e.key == 8) {
      navigate("/inventoryManager")
    }
    if (e.key == 9) {
      // navigate("/inventoryManager")
    }
    if (e.key == 10) {
      navigate("/warehouse")
    }
  };
  const handleBranchChange = (key) => {
    console.log(key);
    setBranchSelectId(key);
   
    const getAllConsultingRoomSelect = async () =>{
      const res3 = await ConsultingRoomService.getAllConsultingRoom(key);
      setConslutingRoomSelect(res3.data.data)
    }
    getAllConsultingRoomSelect();
  };
  const handleRoleChange = (key) => {
    console.log(key);
    setRoleSelectId(key);
  };
  const handleConslutingRoomChange = (value) => {
    console.log(value);
    setConslutingRoomSelectId(value);
  };
  // const handlePositionChange = (value) => {
  //   console.log(value);
  //  <td data-label="col-3">{consultingRoom.branchId === branch.branchId ? branch.branchName :"" }</td>
  // };


  useEffect(() => {
    const getAllBranchSelect = async () => {
      const res1 = await BranchService.getAllBranch();
      setBranchSelect(res1.data.data);
      // setBranchSelectId(branchId);
    };
    getAllBranchSelect();
    const getAllRoleSelect = async () => {
      const res2 = await RoleService.getAllRole();
      setRoleSelect(res2.data.data);
      // setRoleSelectId(roleId);
    };
    getAllRoleSelect();
  

    // setDataForNewStaff();
  }, []);
  const setDataForNewStaff = () => {
    console.log(newStaff);

    setFullName(newStaff.fullName);
    setDate(newStaff.dob);
    setDob(newStaff.dob);
    setSex(newStaff.sex);
    setJob(newStaff.job);
    setIdentityCard(newStaff.identityCard);
    setAddress(newStaff.address);
    setVillage(newStaff.village);
    setDistrict(newStaff.district);
    setProvince(newStaff.province);
    setPhoneNumber(newStaff.phoneNumber);
    setEthnicity(newStaff.ethnicity);
    setPosition(newStaff.position)
    console.log(setNewStaff);
  };

  const SaveAction = async () => {
    // console.log(dateState);
    let dateStateFrom = location.state.isNewStaff
      ? moment(dateState, "yyyy-MM-dd")
      : dateState.format("yyyy-MM-DD");
    console.log(dateState);
    console.log(dateStateFrom);
    const [year, month, day] = dateStateFrom.split("-");

    let dateFrom = day + "-" + month + "-" + year;
    console.log(dateFrom);

    const resdata = await StaffService.createStaff(
      fullName,
      dateStateFrom,
      address,
      village,
      district,
      province,
      sex,
      identityCard,
      phoneNumber,
      ethnicity,
      job,
      email,
      userName,
      passWord,
      confirmPassWord,
      branchIdS,
      roleId,
      conslutingRoomId,
      position
    );

    console.log(position);
    navigate("/staff");
  };
  const ReturnAction = () => {
    navigate(-1);
  };

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];
  
  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo"> </div>
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub1"]}
          selectedKeys={[current]}
          mode="inline"
          items={MenuItem}
        />
      </Sider>
      <Layout className="site-layout">
      <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >
                <div>
                  <Avatar
                    size="default"
                    style={{ marginRight: 10 }}
                    src={window.localStorage.getItem("avatar")}
                  ></Avatar>
                  {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <h2>Thêm Thông Tin Nhân Viên</h2>
          <div className="ct-cs" style={{ margin: "15px 0px 0px 0px" }}>
            <Form
              labelCol={{
                span: 4,
              }}
              wrapperCol={{
                span: 14,
              }}
            >
              <Form.Item label="Họ và Tên">
                <Input
                  value={fullName}
                  onChange={(event) => setFullName(event.target.value)}
                />
              </Form.Item>

              <Form.Item label="Giới tính" style={{ marginBottom: 0 }}>
                <Row>
                  <Col md={12}>
                    <Form.Item
                      rules={[
                        {
                          required: true,
                        },
                      ]}
                      style={{
                        display: "inline-block",
                        width: "calc(100% - 20px)",
                      }}
                    >
                      <Input
                        value={sex}
                        onChange={(event) => setSex(event.target.value)}
                      />
                    </Form.Item>
                  </Col>
                  <Col md={12}>
                    <Form.Item
                      label="Ngày sinh"
                      rules={[
                        {
                          required: true,
                        },
                      ]}
                      style={{
                        display: "inline-block",
                        width: "calc(100% - 10px)",
                        margin: "0 8px",
                      }}
                    >
                      <Space direction="vertical">
                        <DatePicker
                          onChange={(event) =>
                            event != null
                              ? setDate(event)
                              : setDate(event.target.value)
                          }
                          disabledDate={(d) =>
                            !d || d.isAfter(currentDateFormYYYYMMDD)
                          }
                        />
                      </Space>
                    </Form.Item>
                  </Col>
                </Row>
              </Form.Item>

              <Form.Item label="Nghề nghiệp" style={{ marginBottom: 0 }}>
                <Form.Item
                  rules={[
                    {
                      required: true,
                    },
                  ]}
                  style={{
                    display: "inline-block",
                    width: "calc(50% - 20px)",
                  }}
                >
                  <Input
                    value={job}
                    onChange={(event) => setJob(event.target.value)}
                  ></Input>
                </Form.Item>

                <Form.Item
                  label="Dân tộc"
                  rules={[
                    {
                      required: true,
                    },
                  ]}
                  style={{
                    display: "inline-block",
                    width: "calc(50%)",
                    margin: "0px 0px 0px 20px",
                  }}
                >
                  <Input
                    value={ethnicity}
                    onChange={(event) => setEthnicity(event.target.value)}
                  ></Input>
                </Form.Item>
              </Form.Item>

              <Form.Item label="Số nhà">
                <Input
                  value={address}
                  onChange={(event) => setAddress(event.target.value)}
                />
              </Form.Item>
              <Form.Item label="Xã">
                <Input
                  value={village}
                  onChange={(event) => setVillage(event.target.value)}
                />
              </Form.Item>
              <Form.Item label="Q/Huyện">
                <Input
                  value={district}
                  onChange={(event) => setDistrict(event.target.value)}
                ></Input>
              </Form.Item>
              <Form.Item label="T/T Tỉnh">
                <Input
                  value={province}
                  onChange={(event) => setProvince(event.target.value)}
                />
              </Form.Item>
              <Form.Item label="Điện thoại">
                <Input
                  value={phoneNumber}
                  onChange={(event) => setPhoneNumber(event.target.value)}
                />
              </Form.Item>
              <Form.Item label="Email">
                <Input
                  value={email}
                  onChange={(event) => setEmail(event.target.value)}
                />
              </Form.Item>
              <Form.Item label="CCCD">
                <Input
                  value={identityCard}
                  onChange={(event) => setIdentityCard(event.target.value)}
                />
              </Form.Item>
              <Form.Item label="Cơ sở">
              <Select onChange={handleBranchChange}>
                  {branchS.map((branch) => {
                    return (
                      <Select.Option
                        value={branch.branchId}
                        key={branch.branchName}
                      >
                        {branch.branchName}
                      </Select.Option>
                    );
                  })}
                </Select>
              </Form.Item>
              <Form.Item label="Loại Người Dùng">
                <Select onChange={handleRoleChange}>
                <Select.Option value={3}>Bác Sĩ</Select.Option>
                  <Select.Option value={4}>Tiếp Tân</Select.Option>
                  <Select.Option value={5}>Nhân Viên Kho</Select.Option>
                </Select>
                
              </Form.Item>
              <Form.Item label="Phòng Ban">
                <Select onChange={handleConslutingRoomChange}>
                  {conslutingRoomS.map((conslutingRoom) => {
                    return (
                      <Select.Option value={conslutingRoom?.consultingRoomId} key={conslutingRoom.roomName}>
                        {conslutingRoom.roomName}
                      </Select.Option>
                    );
                  })}
                </Select>
                
              </Form.Item>
              <Form.Item label="Chức vụ">
               
              <Input
                  value={position}
                  
                  onChange={(event) => setPosition(event.target.value)}
                />
              </Form.Item>
            </Form>
          </div>
          <div style={{ margin: "20px 0px 0px 0px" }}>
            <Form
              labelCol={{
                span: 4,
              }}
              wrapperCol={{
                span: 14,
              }}
            >
              <Form.Item label="Tài Khoản">
                <Input
                  value={userName}
                  onChange={(event) => setUserName(event.target.value)}
                />
              </Form.Item>
              <Form.Item label="Mật  Khẩu">
                <Input
                  value={passWord}
                  onChange={(event) => setPassWord(event.target.value)}
                />
              </Form.Item>
              <Form.Item label="Xác nhận Mật  Khẩu">
                <Input
                  value={confirmPassWord}
                  onChange={(event) => setConfirmPassWord(event.target.value)}
                />
              </Form.Item>
            </Form>
          </div>
          <div>
            <Row>
              <Col md={8}></Col>
              <Col md={4}>
                <Button onClick={SaveAction}>Lưu</Button>
              </Col>
              <Col md={4}>
                <Button onClick={ReturnAction}>Hủy Bỏ</Button>
              </Col>
            </Row>
          </div>

   
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const MenuItem = [
  getItem("Quản lý", "sub1", <ShopOutlined />, [
    getItem("QL Nhân viên", "1"),
    getItem("QL Phòng khám", "2"),
    getItem("Báo cáo phòng khám", "3"),
    getItem("QL Dịch vụ", "4"),
    // getItem("QL Phòng ban", "5"),
  ]),
  getItem("Quản lý Kho Hàng ", "sub2", <ShopOutlined />, [
    getItem("Nhập kho", "6"),
    getItem("Điều chuyển", "7"),
    getItem("Kiểm kê", "8"),
    // getItem("Báo cáo kho", "9"),
    getItem("Quản lý kho", "10"),
  ]),

];


export default CreateStaff;