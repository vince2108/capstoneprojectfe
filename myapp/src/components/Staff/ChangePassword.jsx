import "./ChangePassword.css";
import { Row, Col, Button, Input, Form } from "antd";

import React, { useState } from "react";
import {useNavigate } from "react-router-dom";
import StaffService from "../../service/StaffService";

function ChangePassword() {
  const [userName, setUserName] = useState();
  const [oldPassword, setOldPassword] = useState();
  const [newPassword, setNewPassword] = useState();
  const [confirmPassword, setConfirmPassword] = useState();

  const navigate = useNavigate();

  const SaveAction = async () => {
    const res = await StaffService.changePassword(
      userName,
      oldPassword,
      newPassword,
      confirmPassword
    );
    console.log(res.data.data);
    navigate("/");
  };

  const BackAction = () => {
    navigate("/");
  };

  return (
    <div className="forgotPassword-body">
    <div className="forgotPassword-form">
      <label className="forgorpassword-title">Cập Nhật Mật Khẩu</label>

      <Form
        labelCol={{
          span: 5,
        }}
        wrapperCol={{
          span: 15,
        }}
      >
        <Form.Item label="Tên Tài Khoản ">
          <Input
            value={userName}
            onChange={(event) => setUserName(event.target.value)}
          />
        </Form.Item>
        <Form.Item label="Mật khẩu cũ">
          <Input
            value={oldPassword}
            onChange={(event) => setOldPassword(event.target.value)}
          />
        </Form.Item>
        <Form.Item label="Mật  Khẩu mới">
          <Input
            value={newPassword}
            onChange={(event) => setNewPassword(event.target.value)}
          />
        </Form.Item>
        <Form.Item label="Xác nhận Mật  Khẩu">
          <Input
            value={confirmPassword}
            onChange={(event) => setConfirmPassword(event.target.value)}
          />
        </Form.Item>
      </Form>

      <div>
        <Row>
          <Col md={8}></Col>
          <Col md={4}>
            <Button onClick={SaveAction}>Lưu</Button>
          </Col>
          <Col md={4}>
            <Button onClick={BackAction}>Hủy Bỏ</Button>
          </Col>
        </Row>
      </div>
      </div>
    </div>
  );
}


export default ChangePassword;
