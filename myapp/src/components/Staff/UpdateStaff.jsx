import "./UpdateStaff.css";
import {
  MenuFoldOutlined,
  ShopOutlined,
  MenuUnfoldOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import {
  Layout,
  Menu,
  Row,
  Col,
  Avatar,
  Button,
  Input,
  Form,
  Select,
  Image,
  Upload,
  Dropdown,
  message,
} from "antd";
import ImgCrop from "antd-img-crop";
import { DatePicker, Space } from "antd";
import React, { useState, Fragment, useEffect } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import StaffService from "../../service/StaffService";
import BranchService from "../../service/BranchService";
import RoleService from "../../service/RoleService";
import ConsultingRoomService from "../../service/ConsultingRoomService";
import moment from "moment";
import axios from "axios";
import FileService from "../../service/FileService";

function UpdateStaff() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");
  const [accountId, setAccountId] = useState();
  const [fullName, setFullName] = useState();
  const [dateState, setDate] = useState();
  const [dob, setDob] = useState();
  const [sex, setSex] = useState();
  const [job, setJob] = useState();
  const [identityCard, setIdentityCard] = useState();
  const [address, setAddress] = useState();
  const [village, setVillage] = useState();
  const [district, setDistrict] = useState();
  const [province, setProvince] = useState();
  const [phoneNumber, setPhoneNumber] = useState();
  const [ethnicity, setEthnicity] = useState();
  const [email, setEmail] = useState();
  const [imageStaff, setImageStaff] = useState();

  const [oldStaff, setOldStaff] = useState();

  const [branchS, setBranchSelect] = useState([]);
  const [branchId, setBranchSelectId] = useState([]);
  const [roleS, setRoleSelect] = useState([]);
  const [roleId, setRoleSelectId] = useState([]);
  const [conslutingRoomS, setConslutingRoomSelect] = useState([]);
  const [conslutingRoomId, setConslutingRoomSelectId] = useState([]);
  const [possition, setPosition] = useState();

  const [loading, setLoading] = useState(false);
  const location = useLocation();
  const navigate = useNavigate();

  const [fileList, setFileList] = useState([]);

  const [fileList2, setFileList2] = useState("");

  const test = (e) => {
    console.log(e.target.files[0]);
    setFileList2(e.target.files[0]);
  };

  const onChange = ({ fileList: newFileList }) => {
    setFileList(newFileList);
  };
  const onPreview = async (file) => {
    let src = file.url;
    if (!src) {
      src = await new Promise((resolve) => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);
        reader.onload = () => resolve(reader.result);
      });
    }
    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow?.document.write(image.outerHTML);
  };

  const SaveAvatarAction = async () => {
    let formData = new FormData();
    let formData2 = new FormData();
    formData.append("file", fileList[0].originFileObj);
    formData2.append("file", setFileList2);
    console.log(formData);
    console.log(formData2);
    const res = await FileService.upLoadFileStaff(formData);
    console.log(res.data.data);
    const res2 = await StaffService.updateStaffImage(
      location.state.accountId,
      res.data.data
    );
  };

  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/staff");
    }
    if (e.key == 2) {
      navigate("/clinic");
    }
    if (e.key == 3) {
      navigate("/report");
    }
    if (e.key == 4) {
      navigate("/service");
    }
    if (e.key == 5) {
      navigate("/divison");
    }
    if (e.key == 6) {
      navigate("/listReceiptManager");
    }
    if (e.key == 7) {
      navigate("/ListTransfer")
    }
    if (e.key == 8) {
      navigate("/inventoryManager")
    }
    if (e.key == 9) {
      // navigate("/inventoryManager")
    }
    if (e.key == 10) {
      navigate("/warehouse")
    }
  };


  const handleBranchChange = (value) => {
    console.log(value);
    setBranchSelectId(value);
    const getAllConsultingRoomSelect = async () => {
      const res3 = await ConsultingRoomService.getAllConsultingRoom(value);
      setConslutingRoomSelect(res3.data.data);
    };
    getAllConsultingRoomSelect();
  };
  const handleRoleChange = (value) => {
    console.log(value);
    setRoleSelectId(value);
  };
  const handleConslutingRoomChange = (value) => {
    console.log(value);
    setConslutingRoomSelectId(value);
  };

  useEffect(() => {
    if (location.state.isOldStaff === true) {
      const setDataOfOldStaff = async () => {
        // console.log(location)
        // console.log(location.state)
        // console.log(location.state.accountId)
        setAccountId(location.state.accountId);

        // console.log(location)
        // console.log(location.state.accountId)
        // console.log(location.state.isOldPatient)
        const res = await StaffService.getStaffById(location.state.accountId);

        setOldStaff(res.data.data);
        console.log(res.data.data);

        setLoading(true);
        setDataForPage();
      };
      setDataOfOldStaff();
      const getAllBranchSelect = async () => {
        const res1 = await BranchService.getAllBranch();
        setBranchSelect(res1.data.data);
        // setBranchSelectId(branchId);
      };
      getAllBranchSelect();
      const getAllRoleSelect = async () => {
        const res2 = await RoleService.getAllRole();
        setRoleSelect(res2.data.data);
        // setRoleSelectId(roleId);
      };
      getAllRoleSelect();
    } else {
      console.log("Not Old Patient");
    }
  }, [loading]);

  const setDataForPage = () => {
    console.log(oldStaff);

    console.log(setOldStaff);
    setAccountId(oldStaff.accountId);
    console.log(oldStaff.accountId);
    setFullName(oldStaff.fullName);
    setDate(oldStaff.dob);
    setDob(oldStaff.dob);
    setSex(oldStaff.sex);
    setJob(oldStaff.job);
    setIdentityCard(oldStaff.identityCard);
    setAddress(oldStaff.address);
    setVillage(oldStaff.village);
    setDistrict(oldStaff.district);
    setProvince(oldStaff.province);
    setPhoneNumber(oldStaff.phoneNumber);
    setEthnicity(oldStaff.ethnicity);
    setEmail(oldStaff.email);
    setPosition(oldStaff.possition);
  };

  const SaveAction = async () => {
    console.log(dateState);
    let dateStateFrom = location.state.isOldStaff
      ? moment(dateState, "yyyy-MM-dd")
      : dateState.format("yyyy-MM-DD");
    const [year, month, day] = dateState.split("-");
    let dateFrom = day + "-" + month + "-" + year;
    console.log(dateFrom);
    console.log(accountId);

    console.log(dob);
    const res = await StaffService.updateStaff(

      accountId,

      fullName,
      dateFrom,
      address,
      village,
      district,
      province,
      sex,
      identityCard,
      phoneNumber,
      ethnicity,
      job,
      email,
      branchId,
      roleId,
      conslutingRoomId,
      possition
    );
    console.log(res.data.data);
    navigate(-1);
  };
  const ReturnAction = () => {
    navigate(-1);
  };
  const changePass = () => {
    navigate("/changePassword");
  }

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];



  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo"> </div>
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub1"]}
          selectedKeys={[current]}
          mode="inline"
          items={MenuItem}
        />
      </Sider>
      <Layout className="site-layout">
      <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >
                <div>
                  <Avatar
                    size="default"
                    style={{ marginRight: 10 }}
                    src={window.localStorage.getItem("avatar")}
                  ></Avatar>
                  {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <h2>Thông Tin Bác Sĩ</h2>
          <div className="csst-cs" style={{ margin: "15px 0px 0px 0px" }}>
            <Form
              labelCol={{
                span: 4,
              }}
              wrapperCol={{
                span: 14,
              }}
            >
              <Row>
                <Col md={18}>
                  <Form.Item label="Họ và Tên">
                    <Input
                      value={fullName}
                      onChange={(event) => setFullName(event.target.value)}
                    />
                  </Form.Item>

                  <Form.Item label="Giới tính" style={{ marginBottom: 0 }}>
                    <Row>
                      <Col md={12}>
                        <Form.Item
                          style={{
                            display: "inline-block",
                          }}
                        >
                          <Input
                            style={{
                              width: "30vh",
                            }}
                            value={sex}
                            onChange={(event) => setSex(event.target.value)}
                          />
                        </Form.Item>
                      </Col>
                      <Col md={12}>
                      <Form.Item label="Ngày sinh" name="month">
                    <Space direction="vertical">
                      <DatePicker
                        style={{
                          width: "195px",
                          height: "38px",
                        }}
                        value={moment(dateState, "YYYY-MM-dd")}
                        onChange={(event) =>
                          event != null
                            ? setDate(event)
                            : setDate(event.target.value)
                        }
                      />
                    </Space>
                  </Form.Item>
                      </Col>
                    </Row>
                  </Form.Item>

                  <Form.Item label="Nghề nghiệp" style={{ marginBottom: 0 }}>
                    <Row>
                      <Col md={12}>
                        <Form.Item
                          style={{
                            display: "inline-block",
                            width: "30vh",
                          }}
                        >
                          <Input
                            value={job}
                            onChange={(event) => setJob(event.target.value)}
                          ></Input>
                        </Form.Item>
                      </Col>
                      <Col md={12}>
                        <Form.Item
                          label="Dân tộc"
                          style={{
                            display: "inline-block",
                            width: "35vh",
                            margin: "0 8px",
                          }}
                        >
                          <Input
                            value={ethnicity}
                            onChange={(event) =>
                              setEthnicity(event.target.value)
                            }
                          ></Input>
                        </Form.Item>
                      </Col>
                    </Row>
                  </Form.Item>

                  <Form.Item label="Số nhà">
                    <Input
                      value={address}
                      onChange={(event) => setAddress(event.target.value)}
                    />
                  </Form.Item>
                  <Form.Item label="Xã">
                    <Input
                      value={village}
                      onChange={(event) => setVillage(event.target.value)}
                    />
                  </Form.Item>
                  <Form.Item label="Q/Huyện">
                    <Input
                      value={district}
                      onChange={(event) => setDistrict(event.target.value)}
                    ></Input>
                  </Form.Item>
                  <Form.Item label="T/T Tỉnh">
                    <Input
                      value={province}
                      onChange={(event) => setProvince(event.target.value)}
                    />
                  </Form.Item>
                  <Form.Item label="Điện thoại">
                    <Input
                      value={phoneNumber}
                      onChange={(event) => setPhoneNumber(event.target.value)}
                    />
                  </Form.Item>
                  <Form.Item label="Email">
                    <Input
                      value={email}
                      onChange={(event) => setEmail(event.target.value)}
                    />
                  </Form.Item>
                  <Form.Item label="CCCD">
                    <Input
                      value={identityCard}
                      onChange={(event) => setIdentityCard(event.target.value)}
                    />
                  </Form.Item>
                </Col>
                <Col md={6}>
                  <div className="image-cs">
                    <ImgCrop rotate>
                      <Upload
                        listType="picture-card"
                        fileList={fileList}
                        onChange={(event) => onChange(event)}
                        onPreview={onPreview}
                        accept=".png,.jpg,.jpeg"
                        beforeUpload={() => true}
                      >
                        {fileList.length < 1 && "+ Upload"}
                      </Upload>
                    </ImgCrop>

                    <Button onClick={SaveAvatarAction}>
                      Cập nhật ảnh đại diện
                    </Button>
                  </div>
                  
                </Col>
              </Row>
            </Form>
            <div className="fawsss">
              <Row>
                <Col md={4}></Col>
                <Col md={4}>
                  <Button onClick={SaveAction}>Lưu</Button>
                </Col>
                <Col md={4}>
                  <Button onClick={ReturnAction}>Hủy Bỏ</Button>
                </Col>
                <Col md={4}>
                  <Button onClick={changePass}>đổi mật khẩu</Button>
                </Col>
              </Row>
            </div>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const MenuItem = [
  getItem("Quản lý", "sub1", <ShopOutlined />, [
    getItem("QL Nhân viên", "1"),
    getItem("QL Phòng khám", "2"),
    getItem("Báo cáo phòng khám", "3"),
    getItem("QL Dịch vụ", "4"),
    getItem("QL Phòng ban", "5"),
  ]),
  getItem("Quản lý Kho Hàng ", "sub2", <ShopOutlined />, [
    getItem("Nhập kho", "6"),
    getItem("Điều chuyển", "7"),
    getItem("Kiểm kê", "8"),
    // getItem("Báo cáo kho", "9"),
    getItem("Quản lý kho", "10"),
  ]),

];

export default UpdateStaff;
