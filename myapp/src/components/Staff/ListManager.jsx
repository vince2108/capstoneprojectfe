import "./Staff.css";
import {
  MenuFoldOutlined,
  ShopOutlined,
  MenuUnfoldOutlined,
  EditOutlined,
  LogoutOutlined,
  HomeOutlined,
  SnippetsOutlined,
  TeamOutlined,
} from "@ant-design/icons";
import { Layout, Menu, Row, Col, Avatar, Checkbox, Button, Dropdown, message } from "antd";
import React, { useState, useEffect } from "react";
import { Input, Space } from "antd";
import StaffService from "../../service/StaffService";
import { Link, useNavigate } from "react-router-dom";

function ListManager() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");

  const [staffF, setStaffFState] = useState([]);

  const navigate = useNavigate();

  useEffect(() => {
    const getAllStaffF = async () => {
      const res = await StaffService.getStaffFollowRole(6);
      setStaffFState(res.data.data);
      console.log(res.data.data)
    };
    getAllStaffF();
    
  }, []);

  const checkInForStaff = (accountId) => {
    console.log(accountId);
    navigate("/updateStaff", {
      state: { accountId: accountId, isOldStaff: true },
    });
  };
  const createNewManager = () => {
    navigate("/createManager", {
      state: { isOldStaff: false },
    });
  };


  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/base");
    }
    if (e.key == 2) {
      // navigate("/clinic");
    }
    if (e.key == 3) {
      navigate("/listManager");
    }
  };

  // search
  const { Search } = Input;
  const onSearch = (value) => {};

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];
  
  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo"> </div>
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub3"]}
          selectedKeys={["3"]}
          mode="inline"
          items={MenuItem}
        />
      </Sider>
      <Layout className="site-layout">
      <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >
                <div>
                  <Avatar
                    size="default"
                    style={{ marginRight: 10 }}
                    src={window.localStorage.getItem("avatar")}
                  ></Avatar>
                  {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <div className="div-iv">
            <Row>
              <Col md={5}>
                <h2>DANH SÁCH</h2>
              </Col>
              <Col md={8}></Col>
              <Col md={9}>
                <Space direction="vertical">
                {/* <Search
                    placeholder="input search text"
                    allowClear
                    enterButton="Search"
                    size="large"
                    onSearch={onSearch}
                  /> */}
                </Space>
              </Col>
              <Col md={2}>
                <Button onClick={createNewManager}>Thêm mới</Button>
              </Col>
            </Row>
          </div>
          <div className="container">
            <table className="tab1">
              <thead>
                <th>Tên bác sĩ</th>
                <th>Số điện thoại</th>
                <th>Giới tính</th>
                <th>Email</th>
                <th>Cơ Sở</th>
                <th></th>
              </thead>
              <tbody>
                {staffF?.map((staff) => {
                  return (
                    <tr key={staff.accountId}>
                      <td data-label="col-1">
                        {staff != null ? staff.fullName : ""}
                      </td>
                      <td data-label="col-2">
                        {staff != null ? staff.phoneNumber : ""}
                      </td>
                      <td data-label="col-3">
                        {staff != null ? staff.sex : ""}
                      </td>
                      <td data-label="col-4">
                        {staff != null ? staff.email : ""}
                      </td>
                      <td data-label="col-4">
                        {staff != null ? staff.branchName : ""}
                      </td>
                      <td data-label="col-5">
                        <Button
                          onClick={() => checkInForStaff(staff.accountId)}
                        >
                          Update
                        </Button>{" "}
                        &nbsp;
                        <Button>Xóa</Button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  }; 
}
const MenuItem = [
    getItem("Quản lý cơ sở", "sub1", <HomeOutlined />, [
      getItem("Tạo cơ sở", "1"),
    ]),
    getItem("Báo cáo", "sub2", <SnippetsOutlined />, [
      getItem("Báo cáo", "2")]),
    getItem("Quán lý nhân viên", "sub3", <TeamOutlined />, [
      getItem("Thêm quản lý", "3"),
    ]),
  ];

export default ListManager;