import "./CreatePay.css";
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  FormOutlined,
  DollarCircleOutlined,
  EditOutlined,
  LogoutOutlined
} from "@ant-design/icons";
import { Layout, Menu, Row, Col, Avatar, Checkbox, Button, message,Dropdown } from "antd";
import React, { useEffect, useState } from "react";
import PatientService from "../../service/PatientService";
import { Link, useLocation, useNavigate } from "react-router-dom";
import PaymentService from "../../service/PaymentService";


function CreatePay() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");
  const [patientInforState, setPatientInforState] = useState([]);
  const [serviceInforState, setServiceInforState] = useState([]);
  const [serviceList, setServiceList] = useState([]);
  const [sumPriceState, setSumPriceState] = useState(0);
  const [loading, setLoading] = useState(false);
  const [errorMessage, setErrosMessage] = useState();
  const [reportListId,setReportListId] = useState([]);
  const [indexSelected,setSelectedIndex] = useState();
  const [serviceIdSelect,setServiceIdSelect] = useState([]);
  const [objectListSelect,setObjectListSelect] = useState([])
    
  const location = useLocation();
  const navigate = useNavigate();


  const onChange = (event, price, serviceId,reportId,serviceId2,index) => {
    console.log(`checked = ${event.target.checked}`);
    console.log(serviceId);
    console.log(reportId)
    console.log(index)
    console.log(serviceId2)
    setSelectedIndex(index)
    const newObjectService = {
      idReportService: serviceId,
      idService: serviceId2,
    };
    event.target.checked
      ? setSumPriceState(sumPriceState + price)
      : setSumPriceState(sumPriceState - price);
    if (event.target.checked) {
      setServiceList([...serviceList, serviceId]);
      setReportListId([...reportListId,reportId])
      setServiceIdSelect([...serviceIdSelect,serviceId2])
      setObjectListSelect([...objectListSelect,newObjectService])
      if(sumPriceState===0){
        document.getElementById('login-error').style.display = "block";
      }
    }
    if (!event.target.checked) {
      const id = Object.values(setServiceList).filter((id) => id === serviceId);
      const id2 = Object.values(setReportListId).filter((id) => id === reportId);
      const id3 = Object.values(setServiceIdSelect).filter((id) => id === serviceId2);
      const id4 = Object.values(setReportListId).filter((id) => id === serviceId);
      setServiceList(id);
      setReportListId(id2)
      setServiceIdSelect(id3)
      setObjectListSelect(id4)
      if(sumPriceState!==0){
        document.getElementById('login-error').style.display = "none";
      }
    }
  };

  const getPayingAction = async () => {
    console.log(serviceList);
    console.log(reportListId)
    // const res = await PaymentService.patientPayingService(serviceList);
    navigate('/bill',{state :{accountId:location.state.accountId,serviceList:serviceList,sumPrice:sumPriceState,reportListId:reportListId,indexSelected:indexSelected,serviceId:serviceIdSelect,objectListSelect:objectListSelect}})
    // setSumPriceState(0);
    // setLoading();

    // navigate(-1)
    // navigate('/bill',{state :{accountId:location.state.accountId,servicedList:serviceList,sumPrice:sumPriceState}})
  };
  const vnPayTest = () =>{
    document.getElementById("background").showModal();
  }

  const hideThisDialog = () => {
    document.getElementById("this-dialog").close();
    console.log("close");
  };

  const getFinishAction = async () => {
    const res = await PaymentService.patientFinishService(
      location.state.accountId
    );
    if (res.data.status === "Error") {
      setErrosMessage(res.data.message);
      document.getElementById("this-dialog").showModal();
    } else {
      navigate("/pay", { replace: true });
    }
  };
  useEffect(() => {
    const setDataPatientInforState = async () => {
      console.log(location.state.accountId);
      const res = await PatientService.getPatientById(location.state.accountId);
      setPatientInforState(res.data.data);
      console.log(res.data.data);
    };
    setDataPatientInforState();
  }, []);

  useEffect(() => {
    const setDataServiceInforState = async () => {
      console.log(location.state.accountId);
      const res = await PaymentService.getPatientUseService(
        location.state.accountId
      );
      console.log(res.data.data);
      setServiceInforState(res.data.data);
      setLoading(true);
    };
    setDataServiceInforState();
  }, [loading]);

  
  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/receive");
    }
    if (e.key == 2) {
      navigate("/pay");
    }
  };

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      console.log("click", e.key);
    } else if (e.key === "2") {
      logOutAction();
    }
  };
  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];

  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo"> </div>
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub2"]}
          selectedKeys={["2"]}
          mode="inline"
          items={MenuItem}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
          <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
            <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >  
                <div>
                <Avatar
                  size="default"
                  style={{ marginRight: 10 }}
                  src={window.localStorage.getItem("avatar")}
                ></Avatar>
                {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <dialog id="this-dialog">
            <p> {errorMessage}</p>
            <button onClick={() => hideThisDialog()}>Đóng</button>
          </dialog>

          <dialog class="bcgrnd animate" id="background">
            <div class="modal-content">
              <div class="modal-body">
                <form method="dialog">
                  <img
                    src="https://res.cloudinary.com/atapas/image/upload/v1602138198/sample.jpg"
                    width="100%"
                    // alt="flower"
                  />
                  <br />
                  <button>Close</button>
                </form>
              </div>
            </div>
          </dialog>
          <h2>Thông tin bệnh nhân</h2>
          <div className="div-iv">
            <Row>
              <Col md={1}></Col>
              <Col md={10}>
                <h4> Mã: {patientInforState.accountId}</h4>
                <h4> Họ và Tên: {patientInforState.fullName}</h4>
                <h4> Giới tính: {patientInforState.sex}</h4>
                <h4> Ngày Sinh: {patientInforState.dob}</h4>
                <h4>
                  {" "}
                  Địa chỉ:{" "}
                  {patientInforState.address +
                    ", " +
                    patientInforState.village +
                    ", " +
                    patientInforState.district +
                    ", " +
                    patientInforState.province}
                </h4>
                <h3 style={{
                  color: "red"
                }}> Trạng thái:{patientInforState.status?"Đang Khám":"Đã Khám Xong"}</h3>
              </Col>
              <Col md={14}></Col>
            </Row>
          </div>
          <div class="container">
            <table className="tab1">
              <thead>
                <th>Dịch vụ </th>
                <th>Đơn giá</th>
                <th>Giá trị gia tăng</th>
                <th>Thành tiền</th>
                <th>Trạng thái</th>
              </thead>
              <tbody>
                {serviceInforState?.map((service,index) => {
                      return (
                        <tr>
                          <td data-label="col-1">
                            <Checkbox
                              disabled={service.isPay ? true : false}
                              onChange={(event) =>
                                onChange(
                                  event,
                                  service.totalCost,
                                  service.reportServiceId,
                                  service.reportId,
                                  service.serviceId,
                                  index
                                )
                              }
                            >
                              {service.serviceName}
                            </Checkbox>
                          </td>
                          <td data-label="col-4">{service.price}</td>
                          <td data-label="col-5">{service.taxPercent}%</td>
                          <td data-label="col-5">{service.totalCost}</td>
                          <td data-label="col-5">
                            {service.isPay
                              ? "Đã Thanh Toán"
                              : "Chưa Thanh Toán"}
                          </td>
                        </tr>
                      );
                    })}
              </tbody>
            </table>
          </div>
          <div style={{ margin: "20px 0px 0px 0px" }}>
            <Row>
              <Col md={2}></Col>
              <Col md={3}>
                <Button 
                 style={{
                    display:"none",
                  }}
                onClick={() => getPayingAction()} id="login-error">Thu phí</Button>
              </Col>
              <Col md={12}>
                <Button onClick={() => getFinishAction()}>Kết thúc</Button>
              </Col>
              <Col style={{ margin: "20px 0px 0px 0px" }} md={3}>
                Tổng: {sumPriceState === null ? "0" : sumPriceState}
              </Col>
{/* 
              <Button onClick={() => vnPayTest()}>test VNP</Button> */}
              
            </Row>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const MenuItem = [
  getItem("Tiếp Đón", "sub1", <FormOutlined />, [getItem("Tiếp Đón", "1")]),
  getItem("Thanh Toán", "sub2", <DollarCircleOutlined />, [getItem("Thanh Toán", "2")]),
];

export default CreatePay;
