import "./Pay.css";
import {
  MenuFoldOutlined,
  ShopOutlined,
  SendOutlined,
  FormOutlined,
  MenuUnfoldOutlined,
  DollarCircleOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { Layout, Menu, Row, Col, Avatar, Button, message, Dropdown } from "antd";
import React, { useState, useEffect } from "react";
import { Input, Space } from "antd";
import { useNavigate } from "react-router-dom";
import PaymentService from "../../service/PaymentService";
import PatientService from "../../service/PatientService";

function Pay() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [current, setCurrent] = useState("1");
  const [theme] = useState("dark");
  const [allPatientPaidState, setAllPatientPaidState] = useState([]);
  const navigate = useNavigate();
  const [searchValue, setSerchValue] = useState();
  const [ErrorMessage,setErrorMessage] = useState();
  let newDate = new Date();
  let date = newDate.getDate();
  let month = newDate.getMonth() + 1;
  let year = newDate.getFullYear();
  const currentDate = date + "/" + month + "/" + year;
  let currentDateFormYYYYMMDD = year + "-" + month + "-" + date;

  useEffect(() => {
    const getAllPatientPaid = async () => {
      const res = await PaymentService.getAllPatientPaid(localStorage.getItem("branchId"));
      console.log(res.data.data);
      setAllPatientPaidState(res.data.data);
    };
    getAllPatientPaid();
  }, []);

  const searchPatientByNameFun = async () => {
    console.log(searchValue);
    if (searchValue === undefined || searchValue === "") {
      setErrorMessage("Vui lòng điền tên để tìm kiếm");
      document.getElementById("this-dialog").showModal();
      return;
    }

    const res = await PaymentService.getPatientPaidByNameAndCurrentDate(
      searchValue,
      localStorage.getItem("branchId")
    );
    if(res.data.status ==='Success'){
      if(res.data.message === "No Data"){
        setErrorMessage('Không tìm thấy bênh nhân nào có tên ' +searchValue)
        document.getElementById('this-dialog').showModal();
        return;
      }
    }

    setAllPatientPaidState(res.data.data);
  };

  const ConfirmAciton = ()=>{
    document.getElementById('this-dialog').close();
  }

  const CreatePaiedFun = (accountId) => {
    navigate("/createPay", { state: { accountId: accountId } });
  };

  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key === "1") {
      navigate("/receive");
    }
    if (e.key === "2") {
      navigate("/pay");
    }
  };

  // search
  const { Search } = Input;
  // const onSearch = (value) => console.log(value);

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };
  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];


  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo"> </div>
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub2"]}
          selectedKeys={["2"]}
          mode="inline"
          items={MenuItem}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >
                <div>
                  <Avatar
                    size="default"
                    style={{ marginRight: 10 }}
                    src={window.localStorage.getItem("avatar")}
                  ></Avatar>
                  {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <dialog id="this-dialog">
            <p> {ErrorMessage}</p>
            <button onClick={() => ConfirmAciton()}>Đóng</button>
          </dialog>
          <div className="div-iv">
            <Row>
              <Col md={2}></Col>
              <Col md={22}>
                <Row>
                  <Col md={18}>
                    <h2>Danh sách chờ thu</h2>
                  </Col>
                  <Col md={4}>
                    <p>Ngày: {currentDate}</p>
                  </Col>
                </Row>
                <Input
                  placeholder="input search text"
                  style={{ width: "50vh" }}
                  value={searchValue}
                  onChange={(event) => setSerchValue(event.target.value)}
                ></Input>
                <Button onClick={() => searchPatientByNameFun()}>search</Button>
              </Col>
            </Row>
          </div>

          <div class="container">
            <table className="tab1">
              <thead>
                <th>Mã số</th>
                <th>Họ và Tên</th>
                <th>Giới tính</th>
                <th>Ngày sinh</th>
                <th></th>
              </thead>
              <tbody>
                {allPatientPaidState === null
                  ? null
                  : allPatientPaidState.map((patientPaid) => {
                      return (
                        <tr>
                          {/* setCount(count+1) */}
                          <td data-label="col-2">{patientPaid.accountId}</td>
                          <td data-label="col-3">{patientPaid.fullName}</td>
                          <td data-label="col-4">{patientPaid.sex}</td>
                          <td data-label="col-5">{patientPaid.dob}</td>
                          <td>
                            <Button className="btn-asasss"
                              onClick={() =>
                                CreatePaiedFun(patientPaid.accountId)
                              }
                              
                            >
                              Thanh toán
                            </Button>
                          </td>
                        </tr>
                      );
                    })}
              </tbody>
            </table>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const MenuItem = [
  getItem("Tiếp Đón", "sub1", <FormOutlined />, [getItem("Tiếp Đón", "1")]),
  getItem("Thanh Toán", "sub2", <DollarCircleOutlined />, [
    getItem("Thanh Toán", "2"),
  ]),
];

export default Pay;
