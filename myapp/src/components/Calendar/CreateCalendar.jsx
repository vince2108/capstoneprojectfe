import {
    MenuFoldOutlined,
    MenuUnfoldOutlined,
    UserOutlined,
    ShopOutlined,
  } from "@ant-design/icons";
  import { Layout, Menu, Row, Col, Avatar } from "antd";
  import React, { useState } from "react";
  import "./CreateCalendar.css";
  import { Badge, Calendar } from 'antd';
  
  function CreateCalendar() {
    const { Header, Sider, Content } = Layout;
    const [collapsed, setCollapsed] = useState(false);
    const [theme] = useState("dark");
    const [current, setCurrent] = useState("1");
  
    const onClick = (e) => {
      console.log("click ", e);
      setCurrent(e.key);
    };

    //lịch
const getListData = (value) => {
  let listData;
  switch (value.date()) {
    case 8:
      listData = [
        {
          type: 'warning',
          content: 'This is warning event.',
        },
        {
          type: 'success',
          content: 'This is usual event.',
        },
      ];
      break;
    case 10:
      listData = [
        {
          type: 'warning',
          content: 'This is warning event.',
        },
        {
          type: 'success',
          content: 'This is usual event.',
        },
        {
          type: 'error',
          content: 'This is error event.',
        },
      ];
      break;
    case 15:
      listData = [
        {
          type: 'warning',
          content: 'This is warning event',
        },
        {
          type: 'success',
          content: 'This is very long usual event。。....',
        },
        {
          type: 'error',
          content: 'This is error event 1.',
        },
        {
          type: 'error',
          content: 'This is error event 2.',
        },
        {
          type: 'error',
          content: 'This is error event 3.',
        },
        {
          type: 'error',
          content: 'This is error event 4.',
        },
      ];
      break;
    default:
  }
  return listData || [];
};
const getMonthData = (value) => {
  if (value.month() === 8) {
    return 1394;
  }
};
    //lịch
    //lịch
    const monthCellRender = (value) => {
        const num = getMonthData(value);
        return num ? (
          <div className="notes-month">
            <section>{num}</section>
            <span>Backlog number</span>
          </div>
        ) : null;
      };
      const dateCellRender = (value) => {
        const listData = getListData(value);
        return (
          <ul className="events">
            {listData.map((item) => (
              <li key={item.content}>
                <Badge status={item.type} text={item.content} />
              </li>
            ))}
          </ul>
        );
      };
    //lịch
    return (
      <Layout>
        <Sider trigger={null} collapsible collapsed={collapsed}>
          <div className="logo" />
          <Menu
            theme={theme}
            onClick={onClick}
            defaultOpenKeys={["sub1"]}
            selectedKeys={[current]}
            mode="inline"
            items={items}
          />
        </Sider>
        <Layout className="site-layout">
          <Header
            className="site-layout-background"
            style={{
              padding: 0,
            }}
          >
            <Row>
              <Col md={18}>
                {React.createElement(
                  collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                  {
                    className: "trigger",
                    onClick: () => setCollapsed(!collapsed),
                  }
                )}
              </Col>
              <Col md={2}></Col>
              <Col md={4}>
                <div>
                  <Avatar
                    size="default"
                    icon={<UserOutlined />}
                    style={{ marginRight: 10 }}
                  ></Avatar>
                  Nguyen Van A
                </div>
              </Col>
            </Row>
          </Header>
          <Content
            className="site-layout-background"
            style={{
              overflow: "scroll",
              margin: "24px 16px",
              padding: 24,
              minHeight: 280,
            }}
          >
            <div className="div1-wh">
              <h2>Phân Bố Lịch Làm Việc</h2>
            </div>
            <div style={{margin:"10px 0px 0px 0px"}}>
            <Calendar dateCellRender={dateCellRender} monthCellRender={monthCellRender} />;
            </div>
          </Content>
        </Layout>
      </Layout>
    );
  }
  function getItem(label, key, icon, children, type) {
    return {
      key,
      icon,
      children,
      label,
      type,
    };
  }
  const items = [
    getItem("Kho Hàng ", "kh", <ShopOutlined />, [
      getItem("Tổng quan", "1"),
      getItem("Điều chuyển", "2"),
      getItem("Kiểm kê", "3"),
      getItem("Quản lý kho", "4"),
      getItem("Nhóm sản phẩm", "5"),
      getItem("Nhóm đơn vị", "6"),
      getItem("Đơn vị", "7"),
      getItem("Nhập kho", "8"),
    ]),
  ];
  
  export default CreateCalendar;
  