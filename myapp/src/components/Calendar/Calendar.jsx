import {
    MenuFoldOutlined,
    MenuUnfoldOutlined,
    UserOutlined,
    ShopOutlined,
  } from "@ant-design/icons";
  import { Layout, Menu, Row, Col, Avatar, Button } from "antd";
  import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
  import "./Calendar.css";
  
  function Calendar() {
    const { Header, Sider, Content } = Layout;
    const [collapsed, setCollapsed] = useState(false);
    const [theme] = useState("dark");
    const [current, setCurrent] = useState("1");
    const navigate = useNavigate();
    const onClick = (e) => {
      console.log("click ", e);
      setCurrent(e.key);
    };

    const viewCalenderAction=()=>{
      navigate('/createCalendar')
    }
    return (
      <Layout>
        <Sider trigger={null} collapsible collapsed={collapsed}>
          <div className="logo" />
          <Menu
            theme={theme}
            onClick={onClick}
            defaultOpenKeys={["sub1"]}
            selectedKeys={[current]}
            mode="inline"
            items={items}
          />
        </Sider>
        <Layout className="site-layout">
          <Header
            className="site-layout-background"
            style={{
              padding: 0,
            }}
          >
            <Row>
              <Col md={18}>
                {React.createElement(
                  collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                  {
                    className: "trigger",
                    onClick: () => setCollapsed(!collapsed),
                  }
                )}
              </Col>
              <Col md={2}></Col>
              <Col md={4}>
                <div>
                  <Avatar
                    size="default"
                    icon={<UserOutlined />}
                    style={{ marginRight: 10 }}
                  ></Avatar>
                  Nguyen Van A
                </div>
              </Col>
            </Row>
          </Header>
          <Content
            className="site-layout-background"
            style={{
              overflow: "scroll",
              margin: "24px 16px",
              padding: 24,
              minHeight: 280,
            }}
          >
            <div className="div1-wh">
              <h2>Phân Bố Lịch Làm Việc</h2>
            </div>
            <div style={{margin:"10px 0px 0px 0px"}}>
                <h3>Phòng Ban:</h3>
                <Row>
                    <Col md={8}>
                        <Button onClick={()=>viewCalenderAction()}>Phòng Khám Nội</Button>
                    </Col>
                </Row>
            </div>
          </Content>
        </Layout>
      </Layout>
    );
  }
  function getItem(label, key, icon, children, type) {
    return {
      key,
      icon,
      children,
      label,
      type,
    };
  }
  const items = [
    getItem("Kho Hàng ", "kh", <ShopOutlined />, [
      getItem("Tổng quan", "1"),
      getItem("Điều chuyển", "2"),
      getItem("Kiểm kê", "3"),
      getItem("Quản lý kho", "4"),
      getItem("Nhóm sản phẩm", "5"),
      getItem("Nhóm đơn vị", "6"),
      getItem("Đơn vị", "7"),
      getItem("Nhập kho", "8"),
    ]),
  ];
  
  export default Calendar;
  