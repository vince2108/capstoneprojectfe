import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  ShopOutlined,
  EditOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { Layout, Menu, Row, Col, Avatar, Button, Select, message, Dropdown } from "antd";
import React, { useState,useEffect } from "react";
import "./CreateClinic.css";
import BranchService from "../../service/BranchService";
import ConsultingRoomService from "../../service/ConsultingRoomService";
import { Link, useLocation, useNavigate } from "react-router-dom";

import { Form, Input } from "antd";

function CreateClinic() {
  const { Header, Sider, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const [theme] = useState("dark");
  const [current, setCurrent] = useState("1");
  const [branchS, setBranchSelect] = useState([]);
  const [branchId, setBranchSelectId] = useState([]);
  const [conslutingRoomS, setConslutingRoomSelect]= useState([]);
  const [consultingRoomId, setConsultingRoomId] = useState();
  const navigate = useNavigate();
  const [roomName, setRoomName] = useState();
  const [briefName, setBriefName] = useState();
  const isAction = true;
  const location = useLocation();
  const [oldClinic, setOldClinic]= useState();
  const [loading, setLoading] = useState(false);
  const [form] = Form.useForm();
  const onClick = (e) => {
    console.log("click ", e);
    // setCurrent(e.key);
    if (e.key == 1) {
      navigate("/staff");
    }
    if (e.key == 2) {
      navigate("/clinic");
    }
    if (e.key == 3) {
      navigate("/report");
    }
    if (e.key == 4) {
      navigate("/service");
    }
    if (e.key == 5) {
      navigate("/divison");
    }
    if (e.key == 6) {
      navigate("/listReceiptManager");
    }
    if (e.key == 7) {
      navigate("/ListTransfer")
    }
    if (e.key == 8) {
      navigate("/inventoryManager")
    }
    if (e.key == 9) {
      // navigate("/inventoryManager")
    }
    if (e.key == 10) {
      navigate("/warehouse")
    }
  };
  const { Option } = Select;

const formItemLayout = {
labelCol: {
  xs: { span: 24 },
  sm: { span: 6 },
},
wrapperCol: {
  xs: { span: 24 },
  sm: { span: 14 },
},
};
  useEffect(() => {
   
      const getAllBranchSelect = async () => {
        const res1 = await BranchService.getAllBranch();
        setBranchSelect(res1.data.data);
        // setBranchSelectId(branchId);
      };
      getAllBranchSelect();

      const getAllConsultingRoomSelect = async () =>{
        const res3 = await ConsultingRoomService.getAllConsultingRoom();
        setConslutingRoomSelect(res3.data.data)
      }
      getAllConsultingRoomSelect();
  
    
 


   
  }, [loading]);



  const handleBranchChange = (key) => {
    console.log(key);
    setBranchSelectId(key);
  };

  const SaveAction = async (values) => {
    
   
    const resdata = await ConsultingRoomService.createConsultingRoom(
      values.roomName,
      values.briefName,
      values.branchId,
      isAction 
    );

    navigate("/clinic");
  };
  const ReturnAction = () => {
    navigate(-1);
  };

  const logOutAction = () => {
    localStorage.clear();
    message.success("Đăng xuất thành công");
    navigate("/");
  };

  const handleMenuClick = (e) => {
    if (e.key === "1") {
      navigate("/updateStaff", {
        state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
      });
    } else if (e.key === "2") {
      logOutAction();
    }
  };

  const items = [
    {
      label: "Cập Nhập Thông Tin",
      key: "1",
      icon: <EditOutlined />,
    },
    {
      label: "Đăng Xuất",
      key: "2",
      icon: <LogoutOutlined />,
      style: { color: "red" },
    },
  ];


  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo" />
        <Menu
          theme={theme}
          onClick={onClick}
          defaultOpenKeys={["sub1"]}
          selectedKeys={["2"]}
          mode="inline"
          items={MenuItem}
        />
      </Sider>
      <Layout className="site-layout">
      <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >
                <div>
                  <Avatar
                    size="default"
                    style={{ marginRight: 10 }}
                    src={window.localStorage.getItem("avatar")}
                  ></Avatar>
                  {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            overflow: "scroll",
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
         
          <div style={{ margin: "20px 0px 0px 0px" }}>
            <Form
             onFinish={(values) => SaveAction(values)}
             form={form}
               labelCol={{
                 span: 4,
               }}
               wrapperCol={{
                 span: 16,
               }}
               
            >
               <Form.Item>
                <Row>
                  <Col md={1}></Col>
                  <Col md={5}>
                    <Button
                      type="primary"
                      htmlType="submit"
                    >
                      Lưu
                    </Button>
                  </Col>
                  <Col md={2}>
                    <Button onClick={ReturnAction}>Hủy</Button>
                  </Col>
                </Row>
              </Form.Item>
   


              <Form.Item 
              label="Tên Phòng Khám" 
              label-size="20x"
              name="roomName"

                 rules={[
                  {
                    required: true,
                    message: 'Vui Lòng Điền Tên Phòng Khám',
                  },
                ]}
                
                hasFeedback>
                <Input
                 value={
                  roomName 
                 }
                 
                 onChange={(event) => setRoomName(event.target.value)}></Input>
              </Form.Item>
              <Form.Item label="tên Viết Tắt" label-size="20px"
              name="briefName"
              rules={[
                {
                  required: true,
                  message: 'Vui Lòng Điền Tên Viết Tắt',
                },
              ]}
              
              hasFeedback>
                <Input
                 ></Input>
              </Form.Item>

              <Form.Item label="Cơ sở"
              name="branchId"
              rules={[
                {
                  required: true,
                  message: 'Vui Lòng Chọn Cơ sở',
                },
              ]}
              
              hasFeedback>
              <Select onChange={handleBranchChange}>
                {branchS.map((branch) => {
                  return (
                    <Select.Option
                      value={branch.branchId}
                      key={branch.branchName}
                    >
                      {branch.branchName}
                    </Select.Option>
                  );
                })}
              </Select>
            </Form.Item>
            </Form>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const MenuItem = [
  getItem("Quản lý", "sub1", <ShopOutlined />, [
    getItem("QL Nhân viên", "1"),
    getItem("QL Phòng khám", "2"),
    getItem("Báo cáo phòng khám", "3"),
    getItem("QL Dịch vụ", "4"),
    // getItem("QL Phòng ban", "5"),
  ]),
  getItem("Quản lý Kho Hàng ", "sub2", <ShopOutlined />, [
    getItem("Nhập kho", "6"),
    getItem("Điều chuyển", "7"),
    getItem("Kiểm kê", "8"),
    // getItem("Báo cáo kho", "9"),
    getItem("Quản lý kho", "10"),
  ]),

];

export default CreateClinic;
