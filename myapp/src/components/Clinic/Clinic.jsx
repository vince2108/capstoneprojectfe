import {
    MenuFoldOutlined,
    MenuUnfoldOutlined,
    UserOutlined,
    ShopOutlined,
    EditOutlined,
    LogoutOutlined,
  } from "@ant-design/icons";
  import { Layout, Menu, Row, Col, Avatar, Button, Dropdown, message } from "antd";
  import React, { useState } from "react";
  import "./Clinic.css";
  import ConsultingRoomService from "../../service/ConsultingRoomService";
  import {useNavigate } from "react-router-dom";
  import BranchService from "../../service/BranchService";
import { useEffect } from "react";
  
  function Clinic() {
    const { Header, Sider, Content } = Layout;
    const [collapsed, setCollapsed] = useState(false);
    const [theme] = useState("dark");
    const [current, setCurrent] = useState("1");

    const [consultingRoom, setConsultingRoom] = useState([]);
    const [branch, setBranch] = useState([]);
    const navigate = useNavigate();
  
    const onClick = (e) => {
      console.log("click ", e);
      // setCurrent(e.key);
      if (e.key == 1) {
        navigate("/staff");
      }
      if (e.key == 2) {
        navigate("/clinic");
      }
      if (e.key == 3) {
        navigate("/report");
      }
      if (e.key == 4) {
        navigate("/service");
      }
      if (e.key == 5) {
        navigate("/divison");
      }
      if (e.key == 6) {
        navigate("/listReceiptManager");
      }
      if (e.key == 7) {
        navigate("/ListTransfer")
      }
      if (e.key == 8) {
        navigate("/inventoryManager")
      }
      if (e.key == 9) {
        // navigate("/inventoryManager")
      }
      if (e.key == 10) {
        navigate("/warehouse")
      }
    };
    const onChange = (e) => {
      console.log(`checked = ${e.target.checked}`);
    };
    const handleChange = (key) => {
      console.log(key);
      
    };
    const createClinic = ()=>{
      console.log();
      navigate("/createClinic",{
      state: {  isOldClinic: false }})
    }
    const updateClinic = (consultingRoomId) => {
      console.log(consultingRoomId);
      navigate("/updateClinic", {
        state: { consultingRoomId: consultingRoomId, isOldClinic: true },
      });
    };

    useEffect(()=>{
      const getAllConsultingRoom = async() =>{
        const res = await ConsultingRoomService.getAllConsultingRoom(localStorage.getItem("branchId"));
        setConsultingRoom(res.data.data);
        console.log(res.data.data);
      }
      getAllConsultingRoom();
      const getAllBranch = async() =>{
        const res2 = await BranchService.getAllBranch();
        setBranch(res2.data.data);
      }
      getAllBranch();
    },[]);
  
    const logOutAction = () => {
      localStorage.clear();
      message.success("Đăng xuất thành công");
      navigate("/");
    };
  
    const handleMenuClick = (e) => {
      if (e.key === "1") {
        navigate("/updateStaff", {
          state: { accountId: localStorage.getItem("idToken"), isOldStaff: true },
        });
      } else if (e.key === "2") {
        logOutAction();
      }
    };
  
    const items = [
      {
        label: "Cập Nhập Thông Tin",
        key: "1",
        icon: <EditOutlined />,
      },
      {
        label: "Đăng Xuất",
        key: "2",
        icon: <LogoutOutlined />,
        style: { color: "red" },
      },
    ];
    return (
      <Layout>
        <Sider trigger={null} collapsible collapsed={collapsed}>
          <div className="logo" />
          <Menu
            theme={theme}
            onClick={onClick}
            defaultOpenKeys={["sub1"]}
            selectedKeys={["2"]}
            mode="inline"
            items={MenuItem}
          />
        </Sider>
        <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          <Row>
            <Col md={20}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </Col>
            <Col md={4}>
              <Dropdown
                menu={{
                  items,
                  onClick: handleMenuClick,
                }}
                placement="bottomLeft"
              >
                <div>
                  <Avatar
                    size="default"
                    style={{ marginRight: 10 }}
                    src={window.localStorage.getItem("avatar")}
                  ></Avatar>
                  {window.localStorage.getItem("usernameToken")}
                </div>
              </Dropdown>
            </Col>
          </Row>
        </Header>
          <Content
            className="site-layout-background"
            style={{
              overflow: "scroll",
              margin: "24px 16px",
              padding: 24,
              minHeight: 280,
            }}
          >
            <div className="div1-wh">
              <Row>
                
                <Col md={2}>
                  <Button style={{margin:"0px 0px 0px 10px"}} onClick={createClinic}
                  >Tạo</Button>
                </Col>
                <Col md={2}>
                  <Button style={{margin:"0px 0px 0px 10px"}}>Xóa</Button>
                </Col>
              </Row>
            </div>
            <div class="container">
              <table className="tab1">
                <thead>
                  <th>Phòng Khám</th>                  
                  <th>Tên viết Tắt</th>
                  <th>Cơ sở</th>
                  <th>Action</th>
                </thead>
                <tbody>
                {consultingRoom?.map((consultingRoom)=>{
                  return(
                    <tr key={consultingRoom.consultingRoomId}>
                    <td data-label="col-1" >
                      {consultingRoom != null ? consultingRoom.roomName : ""}
                      </td>
                    <td data-label="col-2">{consultingRoom != null ? consultingRoom.abbreviationName : ""}</td>
                   
                        <td data-label="col-3">{ consultingRoom != null ? consultingRoom.branchName :"" }</td>
                   
                        <td data-label="col-4"> <Button  onClick={()=> updateClinic(consultingRoom.consultingRoomId)}
                  >Update</Button></td>
                  </tr>
                  )
                 })

                 }
  
                 
  
                </tbody>
              </table>
            </div>
          </Content>
        </Layout>
      </Layout>
    );
  }
  function getItem(label, key, icon, children, type) {
    return {
      key,
      icon,
      children,
      label,
      type,
    };
  }
  const MenuItem = [
    getItem("Quản lý", "sub1", <ShopOutlined />, [
      getItem("QL Nhân viên", "1"),
      getItem("QL Phòng khám", "2"),
      getItem("Báo cáo phòng khám", "3"),
      getItem("QL Dịch vụ", "4"),
      // getItem("QL Phòng ban", "5"),
    ]),
    getItem("Quản lý Kho Hàng ", "sub2", <ShopOutlined />, [
      getItem("Nhập kho", "6"),
      getItem("Điều chuyển", "7"),
      getItem("Kiểm kê", "8"),
      // getItem("Báo cáo kho", "9"),
      getItem("Quản lý kho", "10"),
    ]),
  
  ];
  
  
  export default Clinic;
  