import "./App.css";
//import HomePage from "./Components/Home/HomePage";
import {
  BrowserRouter as Router,
  Link,
  Navigate,
  Route,
  Routes,
} from "react-router-dom";
import Login from "./components/Login/Login";
import Register from "./components/Register/Register";
import RequireAuth from "./hooks/RequireAuth";
//import { useState } from "react";
//import DashboardPage from "./Components/Dashboard.jsx/DashboardPage";
import MedicalRecord from "./components/MedicalRecord/MedicalRecord";
import NotFound from "./components/NotFound/NotFound";
import DetailMedicalRecord from "./components/MedicalRecord/DetailMedicalRecord";
import Receive from "./components/Receive/Receive";
import DetailReceive from "./components/Receive/DetailReceive";
import Pay from "./components/Pay/Pay";
import CreatePay from "./components/Pay/CreatePay";
import CheckUp from "./components/CheckUp/CheckUp";
import DetailCheckUp from "./components/CheckUp/DetailCheckUp";
import Layout from "./components/Layout";
import Unauthorized from "./components/NotFound/Unauthorized";
import UpdateStaff from "./components/Staff/UpdateStaff";
import Staff from "./components/Staff/Staff";
import CreateStaff from "./components/Staff/CreateStaff";
import CreateUnit from "./components/UnitGroup/CreateUnit";
import Unit from "./components/UnitGroup/Unit";
import UnitGroup from "./components/UnitGroup/UnitGroup";
import Base from "./components/Base/Base";
import CreateBase from "./components/Base/CreateBase";
import Clinic from "./components/Clinic/Clinic";
import CreateClinic from "./components/Clinic/CreateClinic";
import UpdateClinic from "./components/Clinic/UpdateClinic";
import Warehouse from "./components/Warehouse/Warehouse";
import CreateWarehouse from "./components/Warehouse/CreateWarehouse";
import ProductGroups from "./components/ProductGroups/ProductGroups";
import CreateProductGroups from "./components/ProductGroups/CreateProductGroups";
import Calendar from "./components/Calendar/Calendar";
import CreateCalendar from "./components/Calendar/CreateCalendar";
import MedicalExamination from "./components/MedicalExamination/MedicalExamination";
import InventoryManager from "./components/Inventoried/InventoryManager";
import InventoryWHstaff from "./components/Inventoried/InventoryWHstaff";
import CreateInventoried from "./components/Inventoried/CreateInventoried";
import WarehouseInventory from "./components/Inventoried/WarehouseInventory";
import Product from "./components/Product/Product";
import CreateProduct from "./components/Product/CreateProduct";
import ReceiptWHstaff from "./components/Receipt/ReceiptWHstaff";
import CreateReceiptManager from "./components/Receipt/CreateReceiptManager";
import GeneralExamination from "./components/MedicalExamination/GeneralExamination";
import Bill from "./components/Bill/Bill";
import Service from "./components/Service/Service";

import Supplier from "./components/Supplier/Supplier";
import ListTransfer from "./components/Transfer/ListTransfer";
import TransferIssue from "./components/Transfer/TransferIssue";
import TransferReceipt from "./components/Transfer/TransferReceipt";
import CreateService from "./components/Service/CreateService";
import ChangePassword from "./components/Staff/ChangePassword";
import ProtectedRoutes from "./components/ProtectedRoutes";
import ListReceiptWHstaff from "./components/Receipt/ListReceiptWHstaff";
import ListReceiptManager from "./components/Receipt/ListReceiptManager";
import ForgotPassword from "./components/ForgotPassword/ForgotPassword";
import ListIssueWHstaff from "./components/Issue/ListIssueWHstaff";
import IssueWHstaff from "./components/Issue/IssueWHstaff";
import CreateIssueDoctor from "./components/Issue/CreateIssueDoctor";
import Overview from "./components/Overview/Overview";
import ForgotPassword2 from "./components/ForgotPassword/ForgotPassword2";
import CreateTransfer from "./components/Transfer/CreateTransfer";
import Report from "./components/Report/Report";
import ListIssueDoctor from "./components/Issue/ListIssueDoctor";
import CreateManager from "./components/Staff/CreateManager";
import ListManager from "./components/Staff/ListManager";

const ROLES = {
  User: "1",
  CEO: "2",
  Doctor: "3",
  Cashier: "4",
  WarehouseStaff: "5",
  Manager: "6",
};

function App() {
  return (
    <Routes>
      {/* <Route path="/" element={localStorage.getItem("loginToken") ? <MedicalRecord /> : <Login />} /> */}
      {/* <Route path="/" element={<Layout />}> */}
      <Route path="/" element={<Login />} />
      <Route path="/forgotPassword" element={<ForgotPassword />} />
      <Route path="/forgotPassword2" element={<ForgotPassword2 />} />
      <Route path="unauthorized" element={<Unauthorized />} />
      <Route path="/createTransfer" element={<CreateTransfer />} />
      <Route path="/changePassword" element={<ChangePassword />} />
      <Route path='/listTransfer' element={<ListTransfer />} />
      <Route path='/transferReceipt/:id' element={<TransferReceipt />} />
      <Route path='/transferIssue/:id' element={<TransferIssue />} />
      <Route path='/product' element={<Product />} />
      
      {/* <Route path="/receive" element={<Receive />} /> */}
      <Route element={<RequireAuth allowedRoles={[ROLES.CEO]} />}>
        <Route path="/base" element={<Base />} />
        <Route path="/createBase" element={<CreateBase />} />
        <Route path="/createManager" element={<CreateManager/>} />
        <Route path="/listManager" element={<ListManager/>} />
      </Route>

      <Route
        element={
          <RequireAuth allowedRoles={[ROLES.Manager, ROLES.WarehouseStaff]} />
        }
      >
        <Route path="/unit" element={<Unit />} />
        <Route path="/unitGroup" element={<UnitGroup />} />
        <Route path="/createUnit" element={<CreateUnit />} />
        <Route path="/productGroups" element={<ProductGroups />} />
        <Route path="/createProductGroups" element={<CreateProductGroups />} />
      </Route>

      <Route
        element={
          <RequireAuth allowedRoles={[ROLES.Manager, ROLES.WarehouseStaff,ROLES.Doctor,ROLES.Cashier]} />
        }
      >
        <Route path="/updateStaff" element={<UpdateStaff />} />
      </Route>

      <Route element={<RequireAuth allowedRoles={[ROLES.Manager]} />}>
        <Route path="/staff" element={<Staff />} />
        <Route path="/createStaff" element={<CreateStaff />} />
        <Route path="/service" element={<Service />} />
        <Route path="/createService" element={<CreateService />} />

        <Route path="/createBase" element={<CreateBase />} />
        <Route path="/updateBase" element={<CreateBase />} />
        <Route path="/clinic" element={<Clinic />} />
        <Route path="/createClinic" element={<CreateClinic />} />
        <Route path="/updateClinic" element={<UpdateClinic />} />
        <Route path="/warehouse" element={<Warehouse />} />
        <Route path="/createWarehouse" element={<CreateWarehouse />} />
        <Route path="/updateWarehouse" element={<CreateWarehouse />} />
        <Route path="/listReceiptManager" element={<ListReceiptManager />} />
        <Route
          path="/createReceiptManager"
          element={<CreateReceiptManager />}
        />
        <Route path="/createProduct" element={<CreateProduct />} />
        <Route path="/inventoryManager" element={<InventoryManager />} />
        <Route path="/createInventoried" element={<CreateInventoried />} />
        <Route path="/report" element={<Report />} />
      </Route>

      <Route element={<RequireAuth allowedRoles={[ROLES.Doctor]} />}>
        <Route path="/medicalRecord" element={<MedicalRecord />} />
        <Route path="/detailMedicalRecord" element={<DetailMedicalRecord />} />
        <Route path="/checkUp" element={<CheckUp />} />
        <Route path="/detailCheckUp" element={<DetailCheckUp />} />
        <Route path="/medicalExamination" element={<MedicalExamination />} />
        <Route path="/generalExamination" element={<GeneralExamination />} />
        <Route path="/createIssueDoctor" element={<CreateIssueDoctor />} />
        <Route path="/listIssueDoctor" element={<ListIssueDoctor/>} />
      </Route>

      <Route element={<RequireAuth allowedRoles={[ROLES.Cashier]} />}>
        <Route path="/receive" element={<Receive />} />
        <Route path="/detailReceive" element={<DetailReceive />} />
        <Route path="/pay" element={<Pay />} />
        <Route path="/createPay" element={<CreatePay />} />
        <Route path="/bill" element={<Bill />} />
      </Route>

      <Route element={<RequireAuth allowedRoles={[ROLES.WarehouseStaff]} />}>
        <Route path="/listReceiptWHstaff" element={<ListReceiptWHstaff />} />
        <Route path="/product" element={<Product />} />
        <Route path="/receiptWHstaff" element={<ReceiptWHstaff />} />
        <Route path="/listIssueWHstaff" element={<ListIssueWHstaff />} />
        <Route path="/issueWHstaff/:id" element={<IssueWHstaff />} />
        <Route path="/overview" element={<Overview />} />
        <Route path="/inventoryWHstaff" element={<InventoryWHstaff />} />
        <Route
          path="/warehouseInventory/:id"
          element={<WarehouseInventory />}
        />
      </Route>

      <Route path="*" element={<NotFound />} />
      {/* </Route> */}
    </Routes>
  );
}

export default App;
