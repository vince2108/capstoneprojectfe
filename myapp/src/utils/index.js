import { notification } from "antd";

export const showSuccess = (description) => {
    notification.success({
      message: 'Success!',
      description,
    });
  }

export const showError = (description) => {
notification.error({
    message: 'Oops!',
    description,
});
}