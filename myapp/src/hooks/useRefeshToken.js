import axios from '../components/Api/axios';
import useAuth from './useAuth';
// import useAuth from './useAuth';

const useRefreshToken = () => {
    const { setAuth } = useAuth();

    const refresh = async () => {
        console.log(setAuth)
        const response = await axios.get('/refresh', {
            withCredentials: true
        });
        setAuth(prev => {
            console.log(JSON.stringify(prev));
            console.log(prev);
            console.log(response.data.accessToken);
            return { ...prev,  roles: response.data.roles, }
        });
        return setAuth?.username;
    }
    return refresh;
};

export default useRefreshToken;