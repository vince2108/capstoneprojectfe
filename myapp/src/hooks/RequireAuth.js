import { useLocation, Navigate, Outlet } from "react-router-dom";
import useAuth from "../hooks/useAuth";

const RequireAuth = ({ allowedRoles }) => {
    // const { auth } = useAuth();
    const location = useLocation();
    // console.log(allowedRoles)
    // const role = localStorage.getItem("RoleToken")
    // console.log("1"+role+"1")
    // if(role==='4'){
    //     console.log("bằng nhau")
    // }
    // console.log(allowedRoles.includes(role))
    // console.log(auth.roles)
    // console.log(auth?.username)
    return (

        // allowedRoles?.includes(localStorage.getItem("RoleToken"))
        //     ? <Outlet />
        //     : localStorage.getItem("loginToken")
        //         ? <Navigate to="/unauthorized" state={{ from: location }} replace />
        //         : <Navigate to="/login" state={{ from: location }} replace />
        
        localStorage.getItem("loginToken") ? 
        allowedRoles?.includes(localStorage.getItem("RoleToken")) ? <Outlet /> : <Navigate to="/unauthorized" state={{ from: location }} replace />
         : <Navigate to="/" state={{ from: location }} replace />
    );
}

export default RequireAuth;